// DOM elementos
var $isAux = $("#isAux");

// Data global
var _data = {
    smmlv: 0,
    dia_semana: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
    tipo_documento: {
        "CC": "Cedula ciudadanía",
        "CE": "Cedula extranjería",
        "RUT": "RUT",
        "NIT": "Cedula ciudadanía",
    },
    elementos: [
        "salario_minimo", 
        "name_employer", 
        "type_doct_employer", 
        "name_employee", 
        "number_employer",
        "phone_employer",
        "email_employer",
        "type_doct_employee", 
        "number_employee",
        // "despido",
        // "email_employee", 
        "city", 
        "cargo", 
        "tipContract", 
        "terminacionContrato", 
        "jornadaLaboral", 
        "dateInicio", 
        "dateFin", 
        "dateLiquidacion", 
        "salario", 
        "salarioXd", 
        "diasLaborados",
        "vacacionesDais",
        "pgCesantias",
        "pgPrima",
        "aux", 
        "dateSalari",
        "salaryPrevious",
        "cambioSalario",
    ]
};

// ===========================================================
// Confirma que las vacaciones sea mayor a 0
// ===========================================================
function vacaciones(vacaciones) {
    return vacaciones >= 0;
}

// ===========================================================
// 
// ===========================================================
function checkAuxTransport() {
    
    var daysWeek = $("input[name='diasLaborados']:checked").length;
    var salaryDaily = $("#salarioXd").val();
    var salary = (daysWeek * 4.33) * salaryDaily;
    var $auxInput = $("input[name='aux']");
    

    if ((2 * _data.smmlv) < salary) {
        $isAux.show();
        $('input[name="aux"]').filter('[value="0"]').attr('checked', true);
        $isAux.hide();
    } else {
        $auxInput.filter('[value="0"]').attr('checked', false);
        $auxInput.filter('[value="1"]').attr('checked', false);
        $isAux.show();
    }
}

// ===========================================================
// Confirma si el elemento type_doct viene vacio
// ===========================================================
function typeDoc() {
    if ($("#type_doct_employer").val() != '' ) {
        $('#type_doct_employer-label').remove();
        $('#type_doct_employer').removeClass('alert alert-danger');
    }
    if ($("#type_doct_employee").val() != '' ) {
        $('#type_doct_employee-label').remove();
        $('#type_doct_employee').removeClass('alert alert-danger');
    }
}

// ===========================================================
// Confirma que sea un correo válido
// ===========================================================
function validar_email(email) {
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

// ===========================================================
// Valida que sea un número de celular válido
// ===========================================================
function validar_phone(phone) {
    return (phone.length <= 10 && phone[0] == 3 && phone.length >= 10)
}

// ===========================================================
// Confirma que los días trabajados sean válidos
// ===========================================================
function porDias() {
    var daysWeek = $("input[name='diasLaborados']:checked").length;
    $(`#diasLaborados`).removeClass('alert alert-danger');
    $(`#diasLaborados-label`).remove();
    
    var valor = $(this).val();
    if ( daysWeek > 5) {
        swal("¡Obligatorio!", "Persona por días no puede laborar más de 6 días a la semana.", "error");
        $("input[name='diasLaborados']:checked").filter(`[value=${valor}]`).prop("checked", false);
    }
}

// ===========================================================
// inicializa el objeto _data
// ===========================================================
function initData() {
    _data.smmlv = $('#salario_minimo').val();
}

// ===========================================================
// Obtiene los dátos registrados previamente
// ===========================================================
function precargarDatos(leadId) {
    var leadId = localStorage.getItem('leadData');
    // #debug#
    console.log(leadId);

    $.ajax({
        type: 'POST',
        url: '/obtener-lead',
        data: {
            idLead: leadId
        },
        dataType: 'json',
    })
    .done(function (data) {
        $('#name_employer').val(data.nombre);
        $('#phone_employer').val(data.celular);
        $('#email_employer').val(data.email);
    });
}

// ===========================================================
// Toma los datos del formulario para mostrarlos en una modal de resumen
// ===========================================================
function renderDataModal( data ) {
    //Pintar datos ingresado en modal
    
    $('#Dname').text(data.name_employer);

    var type_doct_employer = _data.tipo_documento[data.type_doct_employer];
    $('#DtypeDocument').text( type_doct_employer );
    $('#DnumberEm').text(data.number_employer);
    $('#DemailEm').text(data.email_employer);
    $('#buyerEmail').val(data.email_employer);
    $('#extra1').val(data.id);
    $('#DphonEm').text(data.phone_employer);

    $('#DnameEmplo').text(data.name_employee);
    $('#DtypeDocEmplo').text( _data.tipo_documento[ data.type_doct_employee ] );
    $('#DnumberEmplo').text(data.number_employee);
    $('#DemailEmplo').text(data.email_employee);

    $('#Dcargo').text(data.cargo);
    $('#DtypeContract').text(data.tipContract == 'TF' ? 'Término fijo' : 'Término Indefinido');
    $('#DcausaTer').text(data.terminacionContrato);
    data.jornadaLaboral == 30 ? $('#Djornada').text('Tiempo Completo') : $('#Djornada').text('Por días');
    $('#DdateIni').text(data.dateInicio);
    $('#DdateEnd').text(data.dateFin);
    $('#DdateLiqui').text(data.dateLiquidacion);
    $('#Dsalary').text(data.salario);
    $('#DdSalary').text(data.salarioXd);
    data.aux == 0 ? $('#Daux').text('No') : $('#Daux').text('Si');
    $('#Dvacaciones').text(data.vacacionesDais);
    $('#Dprima').text(data.pgPrima);
    $('#Dces').text(data.pgCesantias);
    
    var dias = [];
    data.diasLaborados.forEach( function( key, idx ) {
        dias.push( _data.dia_semana[key] );
    } );

    $('#DdaysXD').text( dias.join(', ') );
}

// ===========================================================
// Postea los datos al back
// ===========================================================
function peticionAjax(data) {

    $.ajax({
        url: "/calcularDias",
        method: 'post',
        async: false,
        dataType: 'json',
        data: data
    })
    .done(function (info) {

        if (info.error) {
            $('#btnGuardar').attr('disabled', false);
            swal("¡Error!", "Liquidación para fechas actuales", "error");
            return false;
        }

        var jornadaTrabajo = $('input[name=jornadaTrabajo]:checked').val() == 'XD' ? _data.smmlv / 30 : _data.smmlv;

        if (info.smlv) {
            $('#btnGuardar').attr('disabled', false);
            swal("¡Error!", `Verificar salario, debe ser mayor a $${jornadaTrabajo} y no debe tener ni puntos(.) ni comas(,)`, "error");
            return false;
        }

        $('#checkout').show();
        $('#btnGuardar').attr('data-id', info.id);
        $('#btnGuardar').removeAttr('disabled', false);

        renderDataModal(data);

        //Pintar datos ingresado en modal

    })
    .fail(function (e) {
        $('#btnGuardar').attr('disabled', false);
        swal("¡Error!", "Algo salió mal", "error");
    });


} // FIN peticionAjax

// ===========================================================
// INICIO DE LA APP
// ===========================================================
$(document).ready(function(){

    initData();
    precargarDatos();
    limpiarValidacion();
    inicioFechas();
    // #debug#
    // console.info('_data', _data );

});

// ===========================================================
// Valida que el telefono sea un número valido
// ===========================================================
$("#phone_employer").keyup(function () {
    $('#phone_employer-label').remove();
    $('#phone_employer').addClass('alert alert-danger');
    $('#phone_employer').parent().append('<p id="phone_employer-label" class="alert alert-danger" for="formBasic_lastName2Employer"><i class=" fas fa-exclamation-triangle"></i>Por favor ingresa número de teléfono valido</p>');
    if (validar_phone($('#phone_employer').val())) {
        $('#phone_employer-label').remove();
        $('#phone_employer').removeClass('alert alert-danger');
    }
});

// ===========================================================
// 
// ===========================================================
$("#salario").keyup(function () {

    let salary = $("#salario").val() * 1;

    if ((2 * _data.smmlv) < salary) {
        $isAux.show();
        $("input[name='aux']").filter('[value="0"]').attr('checked', true);
        $isAux.hide();
    } else {
        $("input[name='aux']").filter('[value="0"]').attr('checked', false);
        $("input[name='aux']").filter('[value="1"]').attr('checked', false);
        $isAux.show();
    }
})


$("#salarioXd").keyup(checkAuxTransport);
// $("#diasLaborados1").click(checkAuxTransport);
$("input[name='diasLaborados']").click(porDias);


$('.selectLiquidation').change(function () {
    var value = $('#'+$(this).attr('id') + ' option:selected').text();
    $($(this)).closest('.selectDivLiquidation').find('.label-select').text(value);

});


$('#cambioSalarioCheck').change(function () {
    var valor = $('#cambioSalarioCheck input[name=cambioSalario]:checked').val();
    // #debug#
    // console.info('valor', valor );
    if ( valor == 0 ) {
        $('.cambioSalario').hide();
    } else {
        $('.cambioSalario').show();
    }

});

// ===========================================================
//causa terminación contrato
// ===========================================================
$('#terminacionContrato').change(function() {
    var terminacionContrato = $("#terminacionContrato").val();
    $('#causa').hide();
    var mensajeSwal = {};

    switch (terminacionContrato) {
        case "Con justa causa":
            mensajeSwal = {
                title: "¡Atención!",
                text: "Ten en cuenta que el código sustantivo del trabajo establece cuales son las causas que se pueden considerar justas para dar por terminado un contrato de trabajo. Además, es necesario seguir el debido proceso y tener soportes de este.",
                type: "warning",
                confirmButtonText: 'Entendido'
            };
            $('#causa').show();
            break;
        case "Mutuo acuerdo":
            mensajeSwal = {
                title: "¡Atención!",
                text: "Recuerda que al terminar un contrato por esta causa se deberá firmar acta en el que las dos partes estén de acuerdo con las condiciones de finalización. Legalmente se recomienda que exista una compensación monetaria por parte del empleador, sin embargo, no existe un monto específico de esta.",
                type: "warning",
                confirmButtonText: 'Entendido'
            };
            break;
    } // FIN switch

    if (mensajeSwal.hasOwnProperty('title')) {
        swal(mensajeSwal);
    }

    if (terminacionContrato != '') {
        $('#cuasaTerminacionContrato-label').remove();
        $('#cuasaTerminacionContrato').removeClass('alert alert-danger');
    }
});

// ===========================================================
// Muestra/Oculta el tipo de contrato definido
// ===========================================================
$('#tipContract').change(function (event) {
    $(".optionTf").hide();
    $("#tipFijo").show();
    $("#tipLiquid").show();
    
    $('#dateInicio').val('');
    $('#dateFin').val('');
    $('#dateLiquidacion').val('');
    $('#dateFin').datepicker('setEndDate', null );
    $('#dateLiquidacion').datepicker('setEndDate', null );

    if($(this).val() == 'TF'){
        $('#tipDefinido').show();
        $("#tc2").show();
        $(".optionTf").show();
    }else{
        $(".optionTf").hide();
        $('#tipDefinido').hide();
        $("#tc2").hide();
    }
});

// ===========================================================
// 
// ===========================================================
$('#form_registro').submit( function( e ) {
    e.preventDefault();
    // #debug#
    // console.info('ingreso a form_registro');
    var forma_data = {};
    var forma = $(this).serializeArray();
    var diasLaborados = [];
    // var elementos = [];

    // #debug#
    // console.info('datos form', forma );

    forma.forEach( function( key, idx ) {
        // elementos.push( key.name );
        switch (key.name) {
            case "diasLaborados":
                diasLaborados.push( key.value );
                break;
        
            default:
                forma_data[key.name] = key.value;
                break;
        }
    } );

    forma_data['diasLaborados'] = diasLaborados;
    if (forma_data.terminacionContrato == "Con justa causa" ) {
        forma_data.terminacionContrato = forma_data.despido;
        delete forma_data.despido;
    }

    forma_data.id = $('#btnGuardar').attr('data-id');

    // #debug#
    console.info('forma_data', forma_data);

    // var validacion = validateForm();
    var validacion = validar( forma_data );
    // #debug#
    console.info('validacion', validacion );
    if ( validacion == 0 ) {
        // forma_data.pgPrima = 'NO';
        // forma_data.pgCesantias = 'NO';
        peticionAjax( forma_data );
    }


} );

function renderValidacionFechas( texto, clase, idx ) {
    return `
        <p>
            ${texto}
        </p>
        <div class="form-check form-check-inline">
            <input class="form-check-input ${clase}" type="radio" name="${clase}" value="Si">
            <label class="form-check-label" for="${idx}Si">Si</label>
        </div>
        <div class="form-check form-check-inline">
            <input class="form-check-input ${clase}" type="radio" name="${clase}" value="No">
            <label class="form-check-label" for="${idx}No">No</label>
        </div>
        <input type="hidden" id="${clase}">
    `;

} // FIN renderValidacionFechas

//Tipo jornada
function jornada( jornada ) {
    $('#jornadaTrabajo-label').remove();
    $('#jornadaTrabajo').removeClass('alert alert-danger');
    if (jornada == "XD") {
        $("#xd").hide();
        $("#tc").show();
        $("#tc1").show();
        $("#diasLaborados").show();
        $("#jornadaLaboral").show();
        $("#salarioDiaDiv").show();
        $("#salarioDiaDiv").addClass("salario");
        $("#salariosMensual").removeClass("salario");
        $("#salariosMensual").hide();
        $('#salario').val('');
        $('#salarioXd').number(true, 0);
    } else {
        $("#xd").show();
        $("#tc").hide();
        $("#tc1").hide();
        $("#diasLaborados").hide();
        $("#salariosMensual").show();
        $("#salariosMensual").addClass("salario");
        $("#salarioDiaDiv").removeClass("salario");
        $("#salarioDiaDiv").hide();
        $("#jornadaLaboral").hide();
        $('#salarioXd').val('');
        $('#salario').number(true, 0);
    }
}
//Tipo jornada


// Salario minímo
function smlv(tx) {
    if(tx == 'xd'){
        $("#salarioXd").val( Math.ceil(_data.smmlv / 30) );
        $('#salarioXd').removeClass('alert alert-danger');
        $('#salarioXd-label').remove();
    }else {
        $("#salario").val( _data.smmlv );
        $('#salario').removeClass('alert alert-danger');
        $('#salario-label').remove();
    }
}
// Salario minímo

function limpiarValidacion() {

    var _radiobuttons = [
        'jornadaLaboral',
        'aux',
        'pgPrima',
        'pgCesantias',
        'cambioSalario',
    ];

    _data.elementos.forEach( function( key ) {
        if ( _radiobuttons.includes(key) ) {
            $(`input[name=${key}]`).change(function() {
                if ($(`input[name=${key}]:checked`).val() != '') {
                    $(`#${key}-label`).remove();
                    $(`#${key}`).removeClass('alert alert-danger');
                }
            });
        }else {
            $(`#${key}`).change(function() {
                if ($(`#${key}`).val() != '') {
                    $(`#${key}-label`).remove();
                    $(`#${key}`).removeClass('alert alert-danger');
                }
            });
        }

    } );

}

// ===========================================================
// Inicializa los eventos de los calendarios
// ===========================================================
function inicioFechas() {
    $('#dateInicio').datepicker({
        language: 'es',
        startDate: '01/01/2017',
        endDate: new Date(),
        format: 'dd-mm-yyyy',
        autoclose: true
    }).on('changeDate', function (e) {
        $('#dateInicio').removeClass('alert alert-danger');
        $('#dateInicio-label').remove();
        console.info('dateInicio', $(this).val());
        $('#dateFin').datepicker('setStartDate', $(this).val());
        $('#dateLiquidacion').datepicker('setStartDate', $(this).val());
        $('#dateFin').val('');
        $('#dateLiquidacion').val('');
    });

    $('#dateFin').datepicker({
        language: 'es',
        format: 'dd-mm-yyyy',
        autoclose: true
    }).on('changeDate', function (e) {
        $('#dateFin').removeClass('alert alert-danger');
        $('#dateFin-label').remove();
        $('#dateLiquidacion').datepicker('setEndDate', $(this).val());
        $('#dateLiquidacion').val('');
    });

    $('#dateLiquidacion').datepicker({
        language: 'es',
        format: 'dd-mm-yyyy',
        autoclose: true
    }).on('changeDate', function (e) {
        if ($("#dateLiquidacion").val() != '' && $("#dateInicio").val() != '' ) {
            $('#dateLiquidacion-label').remove();
            $('#dateLiquidacion').removeClass('alert alert-danger');

            var fin = $("#dateLiquidacion").val().split("-");
            var init = $("#dateInicio").val().split("-");
            var mes_fin = parseInt( fin[1] );
            var mes_init = parseInt( init[1] );
            var anio_fin = parseInt( fin[2] );
            var anio_init = parseInt( init[2] );

            // #debug#
            console.info('fin', fin, 'init', init );

            // cesantias
            if ( anio_init < anio_fin ) {
                
                var texto = `¿Pagaste cesantias del año ${(anio_fin - 1)} en enero del ${(anio_fin)}?`;
                
                $('#cesantiasPregunta p').html(texto);
                $('#cesantiasPregunta').show();
                $('#pgCesantias-label').remove();

                // FALSE
                if ( !$(".pgCesantias").is(":checked")) {
                    $('#pgCesantias').addClass('alert alert-danger');
                    $('#pgCesantias').parent().append(`
                        <p id="pgCesantias-label" class="alert alert-danger" for="formBasic_lastName2Employer">
                            <i class=" fas fa-exclamation-triangle"></i>Este campo es requerido.
                        </p>
                    `);

                    validacion = 1;

                    // swal("¡Obligatorio!", "Estos campos son requeridos para continuar", "error");
                }
                // TRUE
                if ($(".pgCesantias").is(":checked")) {
                    $('#pgCesantias-label').remove();
                    $('#pgCesantias').removeClass('alert alert-danger');
                }
            } else {
                $("input[name='pgCesantias']").filter('[value="No"]').attr('checked', true);
                $("#cesantiasPregunta").hide();
            }

            // Prima
            var texto = '';
            if (mes_fin == 12) {
                texto = `¿Pagaste la prima del último periodo (30 diciembre ${anio_fin} )?`;

            } else if (mes_fin > 5 && mes_fin < 12) {
                texto = `<p>¿Pagaste la prima del último periodo (30 junio ${anio_fin} )?</p>`;

            } else if (mes_fin < 6 && anio_init < anio_fin ) {
                texto = `¿Pagaste la prima del último periodo (30 diciembre ${(anio_fin - 1)} )?`;
            }else {
                $("input[name='pgPrima']").filter('[value="No"]').attr('checked', true);
                $("#pagoPrima").hide();
            }
            $("#pagoPrima p").html(texto);
            if( texto.length > 0 ){
                $("#pagoPrima").show();
            }
        }
    });
}

// ===========================================================
// Valida que los datos del formulario esten completos
// ===========================================================
function validar( data ) {

    var validacion = 0;
    var listado_validaciones = [];
    var anio_fin = parseInt(data.dateLiquidacion.split('-')[2]);
    var anio_inicio = parseInt(data.dateInicio.split('-')[2]);
    // #debug#
    console.info('ingreso a validar', data );

    // Validaciones jornada Trabajo
    if (data.hasOwnProperty('jornadaLaboral') ) {
        if ( data.jornadaLaboral == '30' ) {
            listado_validaciones = ['salarioXd', 'diasLaborados'];
        } else {
            listado_validaciones = ['salario'];
        }
    }

    // #debug#
    // console.info(`${anio_inicio} == ${anio_fin}`, anio_inicio == anio_fin );

    if ( anio_inicio == anio_fin ) {
        listado_validaciones = listado_validaciones.concat(['pgCesantias']);
    }

    // Validaciones tipo Contrato
    if ( data.hasOwnProperty('tipContract') ) {
        
        if ( data.tipContract == "TF" ) {
            listado_validaciones = listado_validaciones.concat([]);
        } else if ( data.tipContract == "TI" ) {
            listado_validaciones = listado_validaciones.concat(['dateFin']);
        }
    }

    // Validaciones Cambio de salario Ultimo año
    if ( data.hasOwnProperty('cambioSalario') ) {
        if ( data.cambioSalario == 0 ) {
            listado_validaciones = listado_validaciones.concat(['dateSalari','salaryPrevious']);
        }
    }

    // Confirma con cada uno de los campos que existan valores adecuados
    _data.elementos.forEach( function(key) {
        
        if (!data.hasOwnProperty(key) || ( data.hasOwnProperty(key) && data[key].length == 0 ) ) {

            if ( listado_validaciones.includes(key) ) {
                console.log("keys que no se validan", key);
                return;
            }
            // #debug#
            // console.info('no key |', key, `#${key}` );
            var idx = `#${key}`;
            var idx_label = `#${key}-label`;
            $(idx_label).remove();
            $(idx).addClass('alert alert-danger');
            $(`.error_${key}`).append(`
                <p id="${key}-label" class="alert alert-danger" for="formBasic_lastName2Employer">
                    <i class=" fas fa-exclamation-triangle"></i>
                    Por favor ingresa un dato válido
                </p>
            `);
            validacion ++;

        }

    } );

    return validacion;

}

$(document).on('click','.hideElement', function (e) {
    $('#checkout').hide();
});