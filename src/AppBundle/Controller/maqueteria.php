<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Liquidation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class maqueteria extends Controller
{

    /**
     * @Route("/preview-doc")
     */
    public function testDocummentAction(){
        $sql = $this->getDoctrine()->getRepository(Liquidation::class)->getLastRegister();

        if(!empty($sql)){
            return $this->render('documents/notificacion-termino-contrato.html.twig',
                array('reg' => $sql));
        }
    }
}
