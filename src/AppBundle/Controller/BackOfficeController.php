<?php

namespace AppBundle\Controller;

use AppBundle\Entity\register;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\service\DocumentLiquidation;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\service\Mailer;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\File\File;
use DateTime;

class BackOfficeController extends Controller
{
    /**
     * @Route("/admin/dashboard", name="dashboardAdmin")
     */
    public function indexAction(){

        $em = $this->getDoctrine()->getManager();

        $registers = $em->getRepository('AppBundle:register')->findAll();

        return $this->render('backOffice/reports/index.html.twig',array(
            'registros'   => $registers
        ));

    }

    /**
     * @Route("/admin/leads", name="reporteLeads")
     */
    public function reporteLeadsAction(){

        $em = $this->getDoctrine()->getManager();

        $leads = $em->getRepository('AppBundle:Leads')->findAll();

        return $this->render('backOffice/reports/leads.html.twig',array(
            'leads'   => $leads
        ));

    }

    /**
     * @Route("/admin/showDetails/{id}", name="showDetails")
     */
    public function showDetailsAction($id){

        if($id != ''){
            $em = $this->getDoctrine()->getManager();
            $register = $em->getRepository('AppBundle:register')->findOneBy(array(
                'id'    => $id
            ));
            return $this->render('backOffice/reports/showDetails.html.twig',array(
                'registro'   => $register
            ));
        } else {
            return $this->redirectToRoute('dashboardAdmin');
        }

    }

    /**
     * @Route("/sendEmailFromAdmin", name="sendEmailFromAdmin")
     */
    public function sendEmailFromAdminAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idRegister = $request->get('idRegister',null);
        $email = $request->get('email',null);

        $register = $em->getRepository('AppBundle:register')->findOneBy(array(
            'id'    => $idRegister
        ));

        $docs = $em->getRepository('AppBundle:Liquidation')->getRegister(intval($register->getId()));

        if(!empty($docs)){

            $paths = $this->get(DocumentLiquidation::class)->Liquidation($docs);
            $emails = $this->get(Mailer::class)->sendEmail($paths,$register,$email);

        }

        return new Response(json_encode(true));

    }

    /**
     * @Route("/downloadDocumentsFromAdmin", name="downloadDocumentsFromAdmin")
     */
    public function downloadDocumentsFromAdminAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $idRegister = $request->get('idRegister',null);

        $register = $em->getRepository('AppBundle:register')->findOneBy(array(
            'id'    => $idRegister
        ));

        $docs = $em->getRepository('AppBundle:Liquidation')->getRegister(intval($register->getId()));

        if(!empty($docs)){

            $paths = $this->get(DocumentLiquidation::class)->Liquidation($docs);
            if(COUNT($paths) > 0){
                foreach ($paths as $path){
                    $response = new BinaryFileResponse($path);
                    $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT);
                }
            }

        }

        return $response;

    }

    /**
     * @Route("/downloadDocumentsAdmin/{id}", name="downloadDocumentsAdmin")
     */
    public function downloadDocumentsAdminAction($id)
    {

        $files = array();
        $em = $this->getDoctrine()->getManager();

        $register = $em->getRepository('AppBundle:register')->findOneBy(array(
            'id'    => $id
        ));

        $docs = $em->getRepository('AppBundle:Liquidation')->getRegister(intval($register->getId()));

        if(!empty($docs)){

            $paths = $this->get(DocumentLiquidation::class)->Liquidation($docs);
            if(COUNT($paths) > 0){
                foreach ($paths as $path){
                    array_push($files, $path);
                }
            }

        }

        $zip = new \ZipArchive();
        $zipName = $register->getEmailEmployer().time().".zip";
        $zip->open($zipName,  \ZipArchive::CREATE);
        foreach ($files as $f) {
            $zip->addFromString(basename($f),  file_get_contents($f));
        }
        $zip->close();

        $response = new Response(file_get_contents($zipName));
        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Content-Disposition', 'attachment;filename="' . $zipName . '"');
        $response->headers->set('Content-length', filesize($zipName));
        return $response;

    }

}
