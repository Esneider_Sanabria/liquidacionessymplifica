<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Leads;
use AppBundle\Entity\register;
use AppBundle\service\Mailer;
use AppBundle\service\Utilities;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\service\DocumentLiquidation;
use DateTime;
use AppBundle\service\Compensation;

class DefaultController extends Controller
{

    private $days360;
    private $timeCommitment;
    private $aux;
    public $idRegister;
    public $dateEnd;

    /* vistas base */
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $value = $em->getRepository('AppBundle:config')->findOneBy(array(
            'name'  => 'amount'
        ));
        $taxes = $em->getRepository('AppBundle:config')->findOneBy(array(
            'name'  => 'tax'
        ));
        $amount = intval($value->getValueLiquidation()) + intval($taxes->getValueLiquidation());
        return $this->render('dashboard/inicio.html.twig',array(
            'amount'    => $amount
        ));
    }
    /**
     * @Route("/info", name="pre-form")
     */
    public function previaAction(Request $request)
    {
        return $this->render('dashboard/previa.html.twig');
    }

    /**
     * @Route("/crear-lead", name="crearLead")
     */
    public function crearLeadAction(Request $request)
    {
        try{
            $answer = $request->request->all();
            $em = $this->getDoctrine()->getManager();

            $exist = $em->getRepository('AppBundle:Leads')->findOneBy(array(
                'nombre'    => $answer['name_employer']
            ));

            if(is_object($exist)){
                $data = array(
                    'flag'  => true,
                    'id'    => $exist->getId()
                );
            } else {

                $lead = new Leads();
                $lead->setNombre($answer['name_employer']);
                $lead->setCelular($answer['celular']);
                $lead->setEmail($answer['email']);
                $lead->setFechaCreacion(new \DateTime());
                $em->persist($lead);
                $em->flush();
                $data = array(
                    'flag'  => true,
                    'id'    => $lead->getId()
                );

            }

            return new Response(json_encode($data));
        } catch (\Exception $e){
            return new Exception($e);
        }
    }



    /**
     * @Route("/obtener-lead", name="obtenerLead")
     */
    public function obtenerLeadAction(Request $request)
    {
            $id = $request->get('idLead',null);
            $em = $this->getDoctrine()->getManager();
            $lead = $em->getRepository('AppBundle:Leads')->findOneBy(array(
                'id'    => $id
            ));
            if(is_object($lead)){
                $data = array(
                    'nombre'    => $lead->getNombre(),
                    'celular'    => $lead->getCelular(),
                    'email'    => $lead->getEmail(),
                );
                return new JsonResponse($data);
            }
            return new Response(json_encode(false));
    }

    /**
     * @Route("/formulario", name="form")
     */
    public function formularioAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $value = $em->getRepository('AppBundle:config')->findOneBy(array(
            'name'  => 'amount'
        ));
        $taxes = $em->getRepository('AppBundle:config')->findOneBy(array(
            'name'  => 'tax'
        ));
        $causaDespido = $em->getRepository('AppBundle:causasDespido')->findAll();
        $reference = uniqid();
        $apiKey = 'p95v5cEiJGGneXeU7UGd0Gs6uW';
        $merchantId = 554611;
        $currency = 'COP';

        $valWithoutTax = $value->getValueLiquidation();
        $tax = $taxes->getValueLiquidation();
        //$valWithoutTax = 8404;
        //$tax = 1596;
        $amount = intval($valWithoutTax) + intval($tax);

        $signature = md5($apiKey.'~'.$merchantId.'~'.$reference.'~'.$amount.'~'.$currency);

        return $this->render('dashboard/form.html.twig', array(
            'smlv'          => $this->smlv(),
            'causa'         => $causaDespido,
            'signature'     => $signature,
            'reference'     => $reference,
            'apiKey'        => $apiKey,
            'merchantId'    => $merchantId,
            'currency'      => $currency,
            'valWithoutTax' => $valWithoutTax,
            'tax'           => $tax,
            'amount'        => $amount,
        ));
    }
    /**
     * @Route("/aprobado", name="aprobado")
     */
    public function aprobadoAction(Request $request)
    {
        $flashbag = $this->get('session')->getFlashBag();
        $idRegister = $flashbag->get("idRegister");

        if(COUNT($idRegister) > 0){
            $em = $this->getDoctrine()->getManager();

            $register = $em->getRepository('AppBundle:register')->findOneBy(array(
                'id'    => intval($idRegister[0])
            ));

            return $this->render('dashboard/aprobado.html.twig',array(
                'register'  => $register,
            ));
        } else {
           return $this->redirectToRoute('homepage');
        }
    }

    /**
     * @Route("/no-aprobado", name="noAprobado")
     */
    public function noAprobadoAction(Request $request)
    {

        $flashbag = $this->get('session')->getFlashBag();
        $idRegister = $flashbag->get("idRegister");

        if(COUNT($idRegister) > 0){
            $em = $this->getDoctrine()->getManager();

            $register = $em->getRepository('AppBundle:register')->findOneBy(array(
                'id'    => intval($idRegister[0])
            ));

            $state = $em->getRepository('AppBundle:RegisterStatus')->findOneBy(array(
                'id'    => 2
            ));

            $register->setStatus($state);
            $em->persist($register);
            $em->flush();

            return $this->render('dashboard/noAprobado.html.twig');
        } else {
            return $this->redirectToRoute('homepage');
        }
    }

    /**
     * @Route("/payu/confirmacion", name="payuConfirmacion")
     */
    public function payuConfirmacionAction(Request $request)
    {
        $state_pol = $request->get('state_pol');
        $extra1 = $request->get('extra1');
        $em = $this->getDoctrine()->getManager();

        $register = $em->getRepository('AppBundle:register')->findOneBy(array(
            'id'    => intval($extra1)
        ));


        if(is_object($register)){

            switch ($state_pol){
                case 4:
                    $state = $em->getRepository('AppBundle:RegisterStatus')->findOneBy(array(
                        'id'    => 6
                    ));

                    $docs = $em->getRepository('AppBundle:Liquidation')->getRegister(intval($register->getId()));

                    if(!empty($docs)){

                        $paths = $this->get(DocumentLiquidation::class)->Liquidation($docs);
                        $emails = $this->get(Mailer::class)->sendEmail($paths,$register);

                    }
                    error_log('Se enviaron los emails');

                    $lead = $em->getRepository('AppBundle:Leads')->findOneBy(array(
                        'email'    => $register->getEmailEmployer()
                    ));

                    if(is_object($lead)){
                        $em->remove($lead);
                        $em->flush();
                    }

                    break;
                case 6:
                    $state = $em->getRepository('AppBundle:RegisterStatus')->findOneBy(array(
                        'id'    => 2
                    ));
                    error_log('La TX se rechazo');
                    break;
                case 5:
                    $state = $em->getRepository('AppBundle:RegisterStatus')->findOneBy(array(
                        'id'    => 2
                    ));
                    error_log('La TX se rechazo');
                    break;
            }

            $register->setStatus($state);
            $em->persist($register);
            $em->flush();

            return new Response(json_encode(true));

        }

        error_log('La TX no tiene extra1 :-(');

        return new Response(json_encode(false));
    }

    /**
     * @Route("/payu/respuesta", name="payuRespuesta")
     */
    public function payuRespuestaAction(Request $request)
    {
        return $this->render('dashboard/noAprobado.html.twig');
    }

    /**
     * @Route("/espera", name="enEspera")
     */
    public function enEsperaAction(Request $request)
    {

        $flashbag = $this->get('session')->getFlashBag();
        $idRegister = $flashbag->get("idRegister");

        if(COUNT($idRegister) > 0){
            $em = $this->getDoctrine()->getManager();

            $register = $em->getRepository('AppBundle:register')->findOneBy(array(
                'id'    => intval($idRegister[0])
            ));

            $state = $em->getRepository('AppBundle:RegisterStatus')->findOneBy(array(
                'id'    => 3
            ));

            $register->setStatus($state);
            $em->persist($register);
            $em->flush();

            return $this->render('dashboard/enEspera.html.twig',array(
                'register'  => $register
            ));
        } else {
            return $this->redirectToRoute('homepage');
        }
    }
    /**
     * @Route("/liquidacionn", name="liquidacion")
     */
    public function liquidacionAction(Request $request)
    {
        return $this->render('documents/liquidacion.html.twig');
    }
    /* vistas base */

    /**
     * @Route("/dataRegister", name="dataRegister")
     */
    public function dataRegisterAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $sql = $em->getRepository('AppBundle:Liquidation')->getLastRegister();

        if(!empty($sql)){

            $paths = $this->get(DocumentLiquidation::class)->Liquidation($sql);
            $emails = $this->get(Mailer::class)->sendEmail($paths,$sql);

            return new Response('true');

        }
    }

    /**
     * @param Request $request
     * @Route("/liquidacion", name="registrarLiquidacion")
     * @return JsonResponse
     *
     */
    public function registrarLiquidacionAction(Request $request){

        $answer = $request->request->all();

        return $this->render('documents/liquidacion.html.twig');
    }


    public function days360($dateInit,$dateEnd){

        $period = array();
        $period['dateInit'] =  new \DateTime($dateInit);
        $period['dateEnd']  =  new \DateTime($dateEnd);

        return   $this->calculateUnitsPrestacionesSociales($period,'TC');
    }


    public function daysTrabajados($dateInit,$dateEnd,$diasLaborados){

        $period = array();
        $period['dateInit'] =  new \DateTime($dateInit);
        $period['dateEnd']  =  new \DateTime($dateEnd);

        return   $this->calculateUnitsPrestacionesSociales($period,'XD',$diasLaborados);
    }

    private function primaXD($dates, $salario, $daysWorks, $interna, $pgPrima)
    {
        //se extrae el año de inicio y fin de trabajo
        $init = new \DateTime($dates['init']);
        $endDate = new \DateTime($dates['end']);
        $year = $endDate->format('Y') - $init->format('Y');

        //$pagoPrima = $answer['pgPrima'];

        $period = $this->semesterDays($init, $endDate, $salario, $daysWorks, $interna);

        // liquidacion mayor a junio y ya pago prima ene-jun
        if ( ($year == 0 || $year != 0) && ($endDate->format('m') >= 6) && $pgPrima == "Si"){

            $period = [
                "daysWorksCurrent" => 0,
                "primeraPrima" => 0,
                "daysPrimerSemestre" => 0,
                "daysSegundoSemestre" => $period['daysSegundoSemestre'],
                "segundaPrima" => $period['segundaPrima'],
                "daysWorksPrevious" => $period['daysWorksPrevious']
            ];
        }
        // liquidacion mayor a junio y ya pago prima ene-jun

        // liquidacion menor a julio y ya pago prima jul-dic
        if ( ($year == 0 || $year != 0) && ($endDate->format('m') <= 7) && $pgPrima == "Si"){

            $period = [
                "daysWorksCurrent" => $period['daysWorksCurrent'],
                "primeraPrima" => $period['primeraPrima'],
                "daysPrimerSemestre" => $period['daysPrimerSemestre'],
                "daysSegundoSemestre" => $period['daysSegundoSemestre'],
                "segundaPrima" => $period['segundaPrima'],
                "daysWorksPrevious" => $period['daysWorksPrevious']
            ];

        }
        // liquidacion menor a julio y ya pago prima jul-dic

        return array(
            "daysPrimerSemestre" => $period['daysWorksCurrent'],
            "primeraPrima" =>  $period['primeraPrima'],
            "daysSegundoSemestre" => $period['daysWorksPrevious'],
            "segundaPrima" =>  $period['segundaPrima']
        );
    }

    private function primaTC($salario, $dates, $interna, $pgPrima)
    {

        //se extrae el año de inicio y fin de trabajo
        $init = new \DateTime($dates['init']);
        $endDate = new \DateTime($dates['end']);
        $year = $endDate->format('Y') - $init->format('Y');
        //$pagoPrima = $answer['pgPrima'];

        $period = $this->semesterDays($init, $endDate, $salario, null, $interna);

        // liquidacion mayor a junio y ya pago prima ene-jun
        if ( ($year == 0 || $year != 0) && ($endDate->format('m') >= 6) && $pgPrima == "Si"){

            $period = [
                "daysPrimerSemestre" => 0,
                "primeraPrima" => 0,
                "daysSegundoSemestre" => $period['daysSegundoSemestre'],
                "segundaPrima" => $period['segundaPrima']
            ];
        }
        // liquidacion mayor a junio y ya pago prima ene-jun

        // liquidacion menor a julio y ya pago prima jul-dic
        if ( ($year == 0 || $year != 0) && ($endDate->format('m') <= 7) && $pgPrima == "Si"){

            $period = [
                "daysPrimerSemestre" => $period['daysPrimerSemestre'],
                "primeraPrima" => $period['primeraPrima'],
                "daysSegundoSemestre" => 0,
                "segundaPrima" => 0
            ];

        }
        // liquidacion menor a julio y ya pago prima jul-dic

        return $period;

    }

    private function calcularLiquidacionTC($dates, $salario, $vacacionesRegister, $salaryPrevious, $interna, $pgCesantias, $pgPrima){


        $tc = 30;
        $dias    = $this->days360;
        /*
        $aux     = $this->aux;
        $vacacionesRegister = $answer['vacacionesDais'];
        */
        $valorCesantias = $this->cesantiasEinteres($salario, "TC", $dates, $salaryPrevious, null, $interna, $pgCesantias);
        $prima = $this->primaTC($salario, $dates, $interna, $pgPrima);

        $diasVacaciones = (($dias/360)*15);
        $valorVacaciones = (($salario/$tc)*($diasVacaciones - $vacacionesRegister));

        //$indemnizacion = $this->calcularIndemnizacion($answer);
        $indemnizacion = 0;

        return array(
                    'daysPrimerSemestre'       => $prima['daysPrimerSemestre'],
                    'primeraPrima'       => $prima['primeraPrima'],
                    'daysSegundoSemestre'       => $prima['daysSegundoSemestre'],
                    'segundaPrima'       => $prima['segundaPrima'],
                    'diasYearActual'          => $valorCesantias['diasYearActual'],
                    'cesantiasActual'          => $valorCesantias['valorCesantiasActual'],
                    'interesesCesantias' => $valorCesantias['valorInteres'],
                    'diasYearAnterior'          => $valorCesantias['diasYearAnterior'],
                    'cesantiasAnterior'          => $valorCesantias['valorCesantiasAnterior'],
                    'interesesCesantiasAnterior' => $valorCesantias['valorInteresAnterior'],
                    'vacaciones'         => $valorVacaciones,
                    'diasVacaciones'     => $diasVacaciones,
                    'indemnizacion'      => $indemnizacion,
                    'units'              => $indemnizacion

         );
    }

    public function  calcularLiquidacionXD($answer, $dates, $interna){

        $aux       = $this->aux;
        $dias      = $this->days360;
        $salario = $answer['salarioXd'];
        $vacacionesRegister  =  $answer['vacacionesDais'];
        $this->dateEnd       =  new \DateTime($answer['dateLiquidacion']);
        $dateEnd       = $answer['dateLiquidacion'];
        $diasLaborados = $this->daysTrabajados($answer['dateInicio'],$dateEnd,$answer['diasLaborados']);
        $base   = (( $salario + ($aux / 30)) * $diasLaborados) / ($dias / 30);

        $valorCesantias = $this->cesantiasEinteres($salario, "XD", $dates,null, $answer['diasLaborados'], $interna, $answer['pgCesantias']);

        $prima = $this->primaXD($dates, $salario, $answer['diasLaborados'], $interna, $answer['pgPrima']);

        $diasVacaciones  = (($diasLaborados/360)*15);
        $valorVacaciones = ($salario * ($diasVacaciones - $vacacionesRegister));

        $indemnizacion = 0;

        return array(
            'base'               => $base,
            'daysPrimerSemestre'       => $prima['daysPrimerSemestre'],
            'primeraPrima'       => $prima['primeraPrima'],
            'daysSegundoSemestre'       => $prima['daysSegundoSemestre'],
            'segundaPrima'       => $prima['segundaPrima'],
            'diasYearActual'          => $valorCesantias['diasYearActual'],
            'cesantiasActual'          => $valorCesantias['valorCesantiasActual'],
            'interesesCesantias' => $valorCesantias['valorInteres'],
            'diasYearAnterior'          => $valorCesantias['diasYearAnterior'],
            'cesantiasAnterior'          => $valorCesantias['valorCesantiasAnterior'],
            'interesesCesantiasAnterior' => $valorCesantias['valorInteresAnterior'],
            'interesesCesantias' => $valorCesantias['valorInteres'],
            'vacaciones'         => $valorVacaciones,
            'diasVacaciones'     => $diasVacaciones,
            'indemnizacion'      => 0,
            'units'              => 0,
            'daysLaborados'      => $diasLaborados
        );

    }

    /**
     * @param Request $request
     * @Route("/calcularDias", name="calcularDias")
     * @return JsonResponse
     */
    public function calcularDiasAction(Request $request){

        $em = $this->getDoctrine()->getManager();

        $answer = $request->request->all();

        $year = explode("-", $answer['dateLiquidacion']);

        if( $year[2] > (new \DateTime())->format('Y') ){
            return new JsonResponse(array(
                'error'          => true
            ));
        }

        if( $answer['jornadaLaboral'] == "XD" && $answer['salarioXd'] < ($this->smlv()->getSalario()/30) ){
            return new JsonResponse(array(
                'smlv'          => true
            ));
        }

        if( $answer['jornadaLaboral'] != "XD" && $answer['salario'] < ($this->smlv()->getSalario()) ){
            return new JsonResponse(array(
                'smlv'          => true
            ));
        }

        $this->aux = 0;
        if($answer['aux'] == 0){
            // si el salario no pasa de 2 minimos se aplica aux
            $this->aux = ($answer['salario']) > (($this->smlv()->getSalario())*2) ? 0 : $this->smlv()->getAux();
        }

        $this->timeCommitment    = ($answer['jornadaLaboral'] == 30 )? 'TC' : 'XD';
        //$dateEnd       = $answer['dateLiquidacion'];
        $id = ($answer['id']);

        $dates = array(
            'init'  => $answer['dateInicio'],
            'end'  => $answer['dateLiquidacion'],
        );
        $this->days360 = $this->days360($dates['init'],$dates['end']);
        $calcularLiquidacion = ($this->timeCommitment == 'TC') ? $this->calcularLiquidacionTC($dates, $answer['salario'], $answer['vacacionesDais'], $answer['salaryPrevious'], $answer['aux'], $answer['pgCesantias'], $answer['pgPrima']) : $this->calcularLiquidacionXD($answer,$dates, $answer['aux']);

        try {
            // insertar table register
            if(empty($id)){
                $register = new register();
            }else{
                $register = $this->getDoctrine()->getRepository(register::class)->find($id);
            }
            $register->setNameEmployer($answer['name_employer']);
            $register->setTypeDocEmployer($answer['type_doct_employer']);
            $register->setNumberEmployer($answer['number_employer']);
            $register->setPhoneEmployer($answer['phone_employer']);
            $register->setEmailEmployer($answer['email_employer']);


            $register->setNameEmployee($answer['name_employee']);
            $register->setTypeDocEmployee($answer['type_doct_employee']);
            $register->setNumberDocEmployee($answer['number_employee']);
            $register->setEmailEmployee($answer['email_employee']);
            $register->setCiudad($answer['city']);

            $register->setSalary($answer['salario']);
            $dias = null;
            if($answer['jornadaLaboral'] == 'XD'){
                $register->setSalary($answer['salarioXd']);
                $daysXD = $answer['diasLaborados'];

                $dia0 = null;
                $dia1 = null;
                $dia2 = null;
                $dia3 = null;
                $dia4 = null;
                $dia5 = null;
                $dia6 = null;
                $coma = ",";
                for ($i = 0; $i < count($daysXD); $i++){

                    //$coma = count($daysXD)-1 == $i ? substr($coma, 0, -1) : $coma;

                    if($daysXD[$i] == 0){
                        $dia0 = "Domingo".$coma;
                    }else if( $daysXD[$i] == 1 ){
                        $dia1 = "Lunes".$coma;
                    }else if( $daysXD[$i] == 2 ){
                        $dia2 = "Martes".$coma;
                    }else if( $daysXD[$i] == 3 ){
                        $dia3 = "Miercoles".$coma;
                    }else if( $daysXD[$i] == 4 ){
                        $dia4 = "Jueves".$coma;
                    }else if( $daysXD[$i] == 5 ){
                        $dia5 = "Viernes".$coma;
                    }else{
                        $dia6 = "Sabado".$coma;
                    }
                }

                $dias = $dia0 . $dia1 . $dia2 . $dia3 . $dia4 . $dia5 . $dia6;
                $dias = substr($dias, 0, -1);
            }

            $register->setDiasSemana($dias);

            $register->setDateEnd(NULL);
            if($answer['tipContract'] == 'TF'){
                $register->setDateEnd(new DateTime($answer['dateFin']));
            }

            $register->setJornadaLaboral($this->timeCommitment);

            $register->setTransSubsidy($answer['aux']);
            $register->setTypeContract($answer['tipContract']);
            $register->setDateLiquidation(new DateTime($answer['dateLiquidacion']));
            $register->setCargo($answer['cargo']);
            $register->setPaymentPrima($answer['pgPrima']);
            $register->setPaymentCesantias($answer['pgCesantias']);
            $register->setDateInit(new DateTime($answer['dateInicio']));
            $register->setTermContract($answer['terminacionContrato']);
            $register->setVacaciones($answer['vacacionesDais']);

            $register->setDaysPrimerSemestre($calcularLiquidacion['daysPrimerSemestre']);
            $register->setValorPrimeraPrima($calcularLiquidacion['primeraPrima']);
            $register->setDaysSegundoSemestre($calcularLiquidacion['daysSegundoSemestre']);
            $register->setValorSegundaPrima($calcularLiquidacion['segundaPrima']);
            $register->setDaysCesantiasActual($calcularLiquidacion['diasYearActual']);
            $register->setValorCesantiasActual($calcularLiquidacion['cesantiasActual']);
            $register->setDaysCesantiasAnterior($calcularLiquidacion['diasYearAnterior']);
            $register->setValorCesantiasAnterior($calcularLiquidacion['cesantiasAnterior']);
            $register->setValorIntCesantiasActual($calcularLiquidacion['interesesCesantias']);
            $register->setValorIntCesantiasAnterior($calcularLiquidacion['interesesCesantiasAnterior']);
            $register->setValorVacaciones($calcularLiquidacion['vacaciones']);
            $register->setDiasVacaciones($calcularLiquidacion['diasVacaciones']);
            $register->setDaysIndemnizacion($calcularLiquidacion['units']);
            $register->setValorIndemnizacion($calcularLiquidacion['indemnizacion']);
            $register->setDaysLaborados($this->days360);

            if($this->timeCommitment == 'XD'){
                $register->setBase($calcularLiquidacion['base']);
                $register->setDaysLaborados($calcularLiquidacion['daysLaborados']);
            }else{
                $register->setBase($answer['salario'] + $this->aux);
            }


            $status = $em->getRepository('AppBundle:RegisterStatus')->findOneBy(array(
                'id'    => 1
            ));

            $register->setStatus($status);
            // Indicamos que queremos guardar este registro.
            $em->persist($register);
            // Ejecuta las querys de las operaciones indicadas.
            $em->flush();

            $id = $register->getId();
            $this->idRegister = $register->getId();

            $this->addFlash("idRegister", $id);

        } catch (\Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }

        return new JsonResponse(array(
            'id'          => $id
            ));

        // insertar table register


    }

    private function calcularIndemnizacion($answer){
        $jornada =  $answer["jornadaLaboral"];
        $compesationService = new Compensation($this->get(Utilities::class));
        $indemnizacion = $compesationService->calculate(
            $answer['terminacionContrato'],
            [
            'start_date' => new \Datetime($answer['dateInicio']),
            'end_date' => new \Datetime($answer['dateFin']),
            'date_end' => new \Datetime($answer['dateLiquidacion']),
        ], $answer['tipContract'],
            $this->timeCommitment  == 'TC'
                ? $answer["salario"]
                : $answer["salarioXd"],
            $jornada, $this->timeCommitment, isset($answer['diasLaborados']) ? $answer['diasLaborados'] : 30);

        return array(
            "indemnizacion"     => $indemnizacion['value'],
            "units"             => $indemnizacion['units']
        );
    }

    public function calculateUnitsPrestacionesSociales($period,$typeContract,$arrayDays = false)
    {


        $units = 0;
        $dateReferenced = new \DateTime($period['dateInit']->format('Y') . '-' . $period['dateInit']->format('m') . '-' . $period['dateInit']->format('d'));
        $dateReferencedEnd = new \DateTime($period['dateInit']->format('Y') . '-' . $period['dateInit']->format('m') . '-30');

        if ($period['dateEnd']->format('d') > 30){
            $period['dateEnd'] = new \DateTime($period['dateEnd']->format('Y-m-30'));
        }
        if ($dateReferencedEnd > $period['dateEnd']) {
            $dateReferencedEnd = new \DateTime($period['dateEnd']->format('Y-m-d'));
        }

        do {
            $unitsMonth = $this->unitsMonth($dateReferenced, $dateReferencedEnd,$typeContract,$arrayDays);

            if ($dateReferenced->format('m') == '02' && $typeContract == 'TC' && $unitsMonth > 27) {
                $unitsMonth += 30 - $unitsMonth;
            }

            $month = $dateReferenced->format('m') + 1;
            $year = $dateReferenced->format('Y');

            if ($month > 12) {
                $year++;
                $month = '01';
            }
            $dayEnd = $month == 2 ? 28 : 30;

            $dateReferencedEnd = new \DateTime($year . '-' . $month . '-' . $dayEnd);
            $dateReferenced = new \DateTime($year . '-' . $month . '-01');

            if ($dateReferencedEnd->format('m') == $period['dateEnd']->format('m') && $dateReferencedEnd->format('Y') == $period['dateEnd']->format('Y')) {
                $dateReferencedEnd = clone $period['dateEnd'];
            }
            $units += $unitsMonth;


        } while ($dateReferencedEnd <= $period['dateEnd']);

        return $units;
    }

    //Se calculan los dias contrato trabajados en el perido
    private function unitsMonth($startDate, $endDate,$typeContract,$arrayDays = false)
    {
        $units = 0;
        if ($typeContract == 'XD') {
            $units = $this->daysWorkedXD($startDate, $endDate,$arrayDays);
        } else {
            $units = $this->daysWorkedTC($startDate, $endDate);
        }
        return $units;
    }

    public function daysWorkedXD($dateRStart, $dateREnd,$arrayDays)
    {

        $wkd = array();

        /** @var WeekWorkableDays $wwd */
        foreach ($arrayDays as $wwd) {
            $wkd[$wwd] = true;
        }

        $dateREnd->modify('+1 day');


        if(($this->dateEnd->format('Y') == $dateREnd->format('Y') and $this->dateEnd->format('m') == $dateREnd->format('m')) and $this->dateEnd->format('d') != 31 ){
                   $dateREnd   = new \DateTime($dateREnd->format('Y').'-'.$dateREnd->format('m').'-'.$dateREnd->format('d').' 00:00:00.000');
        }else{
                   $dateREnd   = new \DateTime($dateREnd->format('Y').'-'.$dateREnd->format('m').'-'.$dateREnd->format('d').' 24:59:00.000');
        }

        $interval = $dateRStart->diff($dateREnd);

        $units = 0;
        $numberDays = $interval->format('%R%a dias');

        for ($i = 0; $i < $numberDays; $i++) {
            $dateToCheck = new \DateTime();
            
            $dateToCheck->setDate($dateRStart->format("Y"), $dateRStart->format("m"), intval($dateRStart->format("d")) + $i);

            if ($this->workable($dateToCheck) && isset($wkd[$dateToCheck->format("w")])) {
                if($dateToCheck->format('m') == $dateRStart->format('m')){
                    $units++;
                }

            }
        }

        return $units;
    }

    private function daysWorkedTC($startDate, $endDate)
    {

        $s = new \DateTime($startDate->format('Y-m-d'));
        $e = new \DateTime($endDate->format('Y-m-d'));
        $interval = $s->diff($e);
        $daysWorked = $interval->format('%R%a') + 1;
        return $daysWorked;
    }


    /**
     * @param DateTime $dateToCheck
     * @return bool
     */
    private function workable($dateToCheck)
    {
        $holyDays = array();
        $date = new DateTime("2016-" . "01" . "-" . "01");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "01" . "-" . "11");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "03" . "-" . "21");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "03" . "-" . "24");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "03" . "-" . "25");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "05" . "-" . "01");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "05" . "-" . "09");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "05" . "-" . "30");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "06" . "-" . "06");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "07" . "-" . "04");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "07" . "-" . "20");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "08" . "-" . "07");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "08" . "-" . "15");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "10" . "-" . "17");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "11" . "-" . "07");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "11" . "-" . "14");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "12" . "-" . "08");
        $holyDays[] = $date;
        $date = new DateTime("2016-" . "12" . "-" . "25");
        $holyDays[] = $date;


        $date = new DateTime("2017-" . "01" . "-" . "01");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "01" . "-" . "09");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "03" . "-" . "20");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "04" . "-" . "09");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "04" . "-" . "13");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "04" . "-" . "14");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "04" . "-" . "16");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "05" . "-" . "01");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "05" . "-" . "29");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "06" . "-" . "19");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "06" . "-" . "26");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "07" . "-" . "03");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "07" . "-" . "20");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "08" . "-" . "07");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "08" . "-" . "21");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "10" . "-" . "16");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "11" . "-" . "06");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "11" . "-" . "13");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "12" . "-" . "08");
        $holyDays[] = $date;
        $date = new DateTime("2017-" . "12" . "-" . "25");
        $holyDays[] = $date;


        $date = new DateTime("2018-" . "01" . "-" . "01");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "01" . "-" . "08");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "03" . "-" . "19");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "03" . "-" . "29");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "03" . "-" . "30");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "05" . "-" . "01");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "05" . "-" . "14");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "06" . "-" . "04");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "06" . "-" . "11");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "07" . "-" . "02");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "07" . "-" . "20");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "08" . "-" . "07");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "08" . "-" . "20");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "10" . "-" . "15");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "11" . "-" . "05");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "11" . "-" . "12");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "12" . "-" . "08");
        $holyDays[] = $date;
        $date = new DateTime("2018-" . "12" . "-" . "25");
        $holyDays[] = $date;

        $date = new DateTime("2019-" . "01" . "-" . "01");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "01" . "-" . "07");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "03" . "-" . "25");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "04" . "-" . "14");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "04" . "-" . "18");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "04" . "-" . "19");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "04" . "-" . "21");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "05" . "-" . "01");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "06" . "-" . "03");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "06" . "-" . "24");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "07" . "-" . "01");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "07" . "-" . "20");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "08" . "-" . "07");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "08" . "-" . "19");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "10" . "-" . "14");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "11" . "-" . "04");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "11" . "-" . "11");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "12" . "-" . "08");
        $holyDays[] = $date;
        $date = new DateTime("2019-" . "12" . "-" . "25");
        $holyDays[] = $date;


        /** @var DateTime $hd */
        foreach ($holyDays as $hd) {
            if ($dateToCheck->format("Y-m-d") == $hd->format("Y-m-d"))
                return false;
        }

        return true;
    }

    public function firstPrima($salario, $aux, $dias, $jornada, $base){


        if($jornada == "XD"){
            $prima = (($base * $dias)/360);
        }else{
            $prima = (($salario+$aux)*($dias))/360;
        }

        return intval($prima);

    }

    public function secondPrima($salario, $aux, $dias, $jornada, $base){

        if($jornada == "XD"){
            $prima           = (($base * $dias)/360);
        }else{
            $prima = (($salario+$aux)*($dias))/360;
        }

        return intval($prima);
    }

    public function cesantiasEinteres($salario, $jornada, $dates, $salaryPrevious, $arrayDays, $interna, $pgCesantias){

        // $daysYearCurrent dias 360 transcurridos
        $aux = 0;
        $init = new \DateTime($dates['init']);
        $end = new \DateTime($dates['end']);
        $while = null;
        $diasLaborados = 0;
        $valorCesantiasAnterior = 0;
        $valorInteresAnterior = 0;
        $daysElapsedPrevious = 0; // dias 360 transcurridos en el año anterior
        $year = $end->format('Y') - $init->format('Y');
        //$pago = 1; // 1 = si / 0 = no

        do {
            if ( $year == 0 ){

                if ($interna == 0){
                    $aux = $this->smly($end->format('Y'))->getAux();
                }
                $daysYearCurrent = $this->days360($init->format('Y-m-d'), $end->format('Y-m-d'));
                if ($arrayDays){
                    $diasLaborados1 = $this->daysTrabajados($init->format('Y-m-d'), $end->format('Y-m-d'), $arrayDays);
                }
                if($while == 1){
                    $daysYearCurrent = $this->days360($end->format('Y-01-01'), $end->format('Y-m-d'));
                    if ($arrayDays) {
                        $diasLaborados1 = $this->daysTrabajados($end->format('Y-01-01'), $end->format('Y-m-d'), $arrayDays);
                    }
                }


                if ($jornada == "XD") {
                    //$valorCesantiasActual = (($base * $daysYearCurrent) / 360);
                    $base = (( ($salario) + ($aux / 30)) * $diasLaborados1) / ($daysYearCurrent / 30);
                    $valorCesantiasActual = ($daysYearCurrent * $base) / 360;

                } else {

                    $valorCesantiasActual = ((($salario + $aux) * $daysYearCurrent) / 360);
                }

                $valorInteresActuales = ($valorCesantiasActual * 0.12 * $daysYearCurrent) / 360;
                $while = 0;//hace terminar el proceso de cesantias porque llego al actual.

            } else{

                // ya pago cesantias del año anterior
                if($pgCesantias != "Si") {

                    if ($interna == 0) {
                        $aux = $this->smly($end->format('Y') - 1)->getAux();
                    }
                    if ($year == 1) {
                        $daysElapsedPrevious = $this->days360($init->format('Y-m-d'), $init->format('Y-12-31'));
                        if ($arrayDays) {
                            $diasLaborados = $this->daysTrabajados($init->format('Y-m-d'), $init->format('Y-12-31'), $arrayDays);
                        }
                    } else {
                        $daysElapsedPrevious = $this->days360(($end->format('Y') - 1) . '-01-01', ($end->format('Y') - 1) . '-12-31');
                        if ($arrayDays) {
                            $diasLaborados = $this->daysTrabajados(($end->format('Y') - 1) . '-01-01', ($end->format('Y') - 1) . '-12-31', $arrayDays);
                        }
                    }

                    if ($jornada == "XD") {

                        $base = ((($salario) + ($aux / 30)) * $diasLaborados) / ($daysElapsedPrevious / 30);
                        $valorCesantiasAnterior = ($daysElapsedPrevious * $base) / 360;

                    } else {

                        //$aux = $this->smly($end->format('Y')-1)->getAux();
                        $salaryPrevious != "" ? $salaryPrevious : $salaryPrevious = $salario;
                        $valorCesantiasAnterior = ((($salaryPrevious + $aux) * $daysElapsedPrevious) / 360);
                    }

                    $valorInteresAnterior = ($valorCesantiasAnterior * 0.12 * $daysElapsedPrevious) / 360;
                }
                $year = $year- $year; // igual a 0 para que entre en las cesantias actuales
                $while = 1; // Entra en las cesantias actuales

            }

        } while ($while > 0);

        return array(
            "diasYearActual"                => $jornada == "XD" ? $diasLaborados1 : $daysYearCurrent,
            "valorCesantiasActual"          => $valorCesantiasActual,
            "valorInteres"                  => $valorInteresActuales,
            "diasYearAnterior"              => $jornada == "XD" ? $diasLaborados : $daysElapsedPrevious,
            "valorCesantiasAnterior"        => $valorCesantiasAnterior,
            "valorInteresAnterior"          => $valorInteresAnterior
        );
    }


    public function smlv()
    {
        $em = $this->getDoctrine()->getManager();
        $sql = $em->getRepository('AppBundle:Smlv')->getSmlv();

        return $sql;

    }

    public function smly($year)//salario por año
    {
        $em = $this->getDoctrine()->getManager();
        $sql = $em->getRepository('AppBundle:Smlv')->getSmly($year);

        return $sql;

    }

    private function semesterDays($init, $endDate, $salario, $daysWorks, $interna){

        $daysWorksPreviousXd = 0;
        $daysWorksCurrentXd = 0;
        $year = $endDate->format('Y') - $init->format('Y');
        $initPrimaSecond = null;
        $secondDays180 = 0;
        $primaDiciembre = 0;
        $second = 0;
        $aux = 0;

        if( $init->format('Y') <= $endDate->format('Y') ) {


            //primer periodo enero 1 a junio 30
            // igual de año en inicio y fin
            if ($year == 0) {

                // Externa
                if ($interna == 0){
                    //años iguales en año inicio y fin
                    $aux = $this->smly($endDate->format('Y'))->getAux(); //aux por año de liquidacion
                }

                // inicia entre enero y finaliza antes de 30 de junio
                if ($endDate->format('m') < 7 && $init->format('m') < 7) {
                    $initPrimaFirst = $init->format('Y-m-d');
                    $endPrimaFirst = $endDate->format('Y-m-d');
                } elseif ($init->format('m') < 7) {

                    // inicia en enero y finaliza después de 01 de julio
                    $initPrimaFirst = $init->format('Y-m-d');
                    $endPrimaFirst = $endDate->format('Y').'-06-30';
                }

                // Dias trabajados para un XD
                if ($daysWorks){
                    $daysWorksCurrentXd = $this->daysTrabajados($initPrimaFirst, $endPrimaFirst, $daysWorks);
                }

            } elseif ($year >= 1) {

                $diff = $this->days360( $endDate->format('Y') .'-01-01',$endDate->format('Y-m-d'));
                // Externa
                if ($interna == 0) {
                    $aux = $this->smly($endDate->format('Y'))->getAux();
                }
                if ($init->format('m') > 6 && $endDate->format('m') > 6 && $diff >= 0 && $diff <= 180 ){
                    $initPrimaFirst = $endDate->format('Y-m-d');
                }else{
                    $initPrimaFirst = $endDate->format('Y').'-01-01';
                }

                if ($endDate->format('m') > 6) {
                    $endPrimaFirst = $endDate->format('Y-06-30');
                } else {
                    $endPrimaFirst = $endDate->format('Y-m-d');
                }

                // Dias trabajados para un XD
                if ($daysWorks){
                    $daysWorksCurrentXd = $this->daysTrabajados($initPrimaFirst, $endPrimaFirst, $daysWorks);
                }
            }


            if (($initPrimaFirst && $endPrimaFirst) != NULL) {

                $firstDays180 = $this->days360($initPrimaFirst, $endPrimaFirst);
                $first = $this->firstPrima($salario, $aux, $firstDays180, null, null);

                if ($daysWorks){
                    $baseEneJunio = (( ($salario) + ($aux / 30)) * $daysWorksCurrentXd) / ($firstDays180 / 30);
                    $primaJunio = ($baseEneJunio * $firstDays180) / 360;
                }
            } else {

                $firstDays180 = 0;
                $first = 0;
            }
            //primer periodo enero 1 a junio 30


            //segundo periodo julio 1 al diciembre 31
            // mismo año y liquidación mayor a junio 30
            if ($year == 0 && $endDate->format('m') > 6) {

                if ($init->format('m') < 7) {
                    $initPrimaSecond = $init->format('Y') . '-07-01';
                } else {
                    $initPrimaSecond = $init->format('Y-m-d');
                }
                $endPrimaSecond = $endDate->format('Y-m-d');
                // mismo año y liquidación mayor a junio 30

                // Dias trabajados para un XD
                if ($daysWorks){
                    $daysWorksPreviousXd = $this->daysTrabajados($initPrimaSecond, $endPrimaSecond, $daysWorks);
                }

                // Más de un año o igual
            }elseif ($year >= 1) {

                if ($init->format('m') < 7) {


                    if ($endDate->format('m') < 7) {

                        // Externa
                        if ($interna == 0) {
                            $aux = $this->smly(($endDate->format('Y') - 1))->getAux();
                        }
                        $initPrimaSecond = ($endDate->format('Y') - 1) . '-07-01';
                        $endPrimaSecond = ($endDate->format('Y') - 1) . '-12-31';
                    }else{
                        // Externa
                        if ($interna == 0) {
                            $aux = $this->smly($endDate->format('Y'))->getAux();
                        }
                        $initPrimaSecond = $endDate->format('Y') . '-07-01';
                        $endPrimaSecond = $endDate->format('Y-m-d');
                    }

                    // Dias trabajados para un XD
                    if ($daysWorks){
                        $daysWorksPreviousXd = $this->daysTrabajados($initPrimaSecond, $endPrimaSecond, $daysWorks);
                    }

                } else {

                    if ($endDate->format('m') > 6) {

                        // Externa
                        if ($interna == 0) {
                            $aux = $this->smly($endDate->format('Y'))->getAux();
                        }
                        $initPrimaSecond = $endDate->format('Y-07-01');
                        $endPrimaSecond = $endDate->format('Y-m-d');
                    } else {

                        // Externa
                        if ($interna == 0) {
                            $aux = $this->smly($endDate->format('Y') - 1)->getAux();
                        }
                        $initPrimaSecond = $init->format('Y-m-d');
                        $endPrimaSecond = ($endDate->format('Y') - 1).'-12-31';
                    }

                    // Dias trabajados para un XD
                    if ($daysWorks){
                        $daysWorksPreviousXd = $this->daysTrabajados($initPrimaSecond, $endPrimaSecond, $daysWorks);
                    }

                }
            }
            // Más de un año o igual

            if($initPrimaSecond){
                $secondDays180 = $this->days360($initPrimaSecond, $endPrimaSecond);
                $second = $this->firstPrima($salario, $aux, $secondDays180, null, null);
            }

            if ($daysWorks && $secondDays180 != 0){
                $baseJulioDiciembre = (( ($salario) + ($aux / 30)) * $daysWorksPreviousXd) / ($secondDays180 / 30);
                $primaDiciembre = ($baseJulioDiciembre * $secondDays180) / 360;
            }

            return array(
                "daysPrimerSemestre" => $firstDays180,
                "primeraPrima" => $daysWorks != "" ? $primaJunio : $first,
                "daysSegundoSemestre" => $secondDays180,
                "segundaPrima" => $daysWorks != "" ? $primaDiciembre : $second,
                "daysWorksCurrent" => $daysWorksCurrentXd,
                "daysWorksPrevious" => $daysWorksPreviousXd
            );
        }
    }
}