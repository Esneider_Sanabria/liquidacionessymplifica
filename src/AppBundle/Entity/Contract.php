<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contract
 *
 * @ORM\Table(name="contract", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_E98F28598E971CF3", columns={"id_employer"}), @ORM\UniqueConstraint(name="UNIQ_E98F2859D449934", columns={"id_employee"})})
 * @ORM\Entity
 */
class Contract
{
    /**
     * @var string
     *
     * @ORM\Column(name="cargo", type="string", length=50, nullable=false)
     */
    private $cargo;

    /**
     * @var string
     *
     * @ORM\Column(name="type_contract", type="string", length=30, nullable=false)
     */
    private $typeContract;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_stard", type="datetime", nullable=false)
     */
    private $dateStard;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=false)
     */
    private $dateEnd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_liquidation", type="datetime", nullable=false)
     */
    private $dateLiquidation;

    /**
     * @var string
     *
     * @ORM\Column(name="type_jornada_working_days", type="string", length=30, nullable=false)
     */
    private $typeJornadaWorkingDays;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Person
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Person")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_employee", referencedColumnName="id")
     * })
     */
    private $idEmployee;

    /**
     * @var \AppBundle\Entity\Person
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Person")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_employer", referencedColumnName="id")
     * })
     */
    private $idEmployer;



    /**
     * Set cargo
     *
     * @param string $cargo
     *
     * @return Contract
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get cargo
     *
     * @return string
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * Set typeContract
     *
     * @param string $typeContract
     *
     * @return Contract
     */
    public function setTypeContract($typeContract)
    {
        $this->typeContract = $typeContract;

        return $this;
    }

    /**
     * Get typeContract
     *
     * @return string
     */
    public function getTypeContract()
    {
        return $this->typeContract;
    }

    /**
     * Set dateStard
     *
     * @param \DateTime $dateStard
     *
     * @return Contract
     */
    public function setDateStard($dateStard)
    {
        $this->dateStard = $dateStard;

        return $this;
    }

    /**
     * Get dateStard
     *
     * @return \DateTime
     */
    public function getDateStard()
    {
        return $this->dateStard;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return Contract
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set dateLiquidation
     *
     * @param \DateTime $dateLiquidation
     *
     * @return Contract
     */
    public function setDateLiquidation($dateLiquidation)
    {
        $this->dateLiquidation = $dateLiquidation;

        return $this;
    }

    /**
     * Get dateLiquidation
     *
     * @return \DateTime
     */
    public function getDateLiquidation()
    {
        return $this->dateLiquidation;
    }

    /**
     * Set typeJornadaWorkingDays
     *
     * @param string $typeJornadaWorkingDays
     *
     * @return Contract
     */
    public function setTypeJornadaWorkingDays($typeJornadaWorkingDays)
    {
        $this->typeJornadaWorkingDays = $typeJornadaWorkingDays;

        return $this;
    }

    /**
     * Get typeJornadaWorkingDays
     *
     * @return string
     */
    public function getTypeJornadaWorkingDays()
    {
        return $this->typeJornadaWorkingDays;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idEmployee
     *
     * @param \AppBundle\Entity\Person $idEmployee
     *
     * @return Contract
     */
    public function setIdEmployee(\AppBundle\Entity\Person $idEmployee = null)
    {
        $this->idEmployee = $idEmployee;

        return $this;
    }

    /**
     * Get idEmployee
     *
     * @return \AppBundle\Entity\Person
     */
    public function getIdEmployee()
    {
        return $this->idEmployee;
    }

    /**
     * Set idEmployer
     *
     * @param \AppBundle\Entity\Person $idEmployer
     *
     * @return Contract
     */
    public function setIdEmployer(\AppBundle\Entity\Person $idEmployer = null)
    {
        $this->idEmployer = $idEmployer;

        return $this;
    }

    /**
     * Get idEmployer
     *
     * @return \AppBundle\Entity\Person
     */
    public function getIdEmployer()
    {
        return $this->idEmployer;
    }
}
