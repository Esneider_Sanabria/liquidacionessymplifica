<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Smlv
 *
 * @ORM\Table(name="smlv", uniqueConstraints={@ORM\UniqueConstraint(name="UNIQ_68EF78BF30A26F35", columns={"id_year"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SmlvRepository")
 */
class Smlv
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id_year", type="integer", nullable=false)
     */
    private $idYear;

    /**
     * @var string
     *
     * @ORM\Column(name="salario", type="string", length=50, nullable=false)
     */
    private $salario;

    /**
     * @var string
     *
     * @ORM\Column(name="aux", type="string", length=50, nullable=false)
     */
    private $aux;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=50, nullable=false)
     */
    private $state;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;



    /**
     * Set idYear
     *
     * @param integer $idYear
     *
     * @return Smlv
     */
    public function setIdYear($idYear)
    {
        $this->idYear = $idYear;

        return $this;
    }

    /**
     * Get idYear
     *
     * @return integer
     */
    public function getIdYear()
    {
        return $this->idYear;
    }

    /**
     * Set salario
     *
     * @param string $salario
     *
     * @return Smlv
     */
    public function setSalario($salario)
    {
        $this->salario = $salario;

        return $this;
    }

    /**
     * Get salario
     *
     * @return string
     */
    public function getSalario()
    {
        return $this->salario;
    }

    /**
     * Set aux
     *
     * @param string $aux
     *
     * @return Smlv
     */
    public function setAux($aux)
    {
        $this->aux = $aux;

        return $this;
    }

    /**
     * Get aux
     *
     * @return string
     */
    public function getAux()
    {
        return $this->aux;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
