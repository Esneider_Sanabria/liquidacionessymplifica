<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * register
 *
 * @ORM\Table(name="register")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\registerRepository")
 */
class register
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name_employer", type="string", length=50)
     */
    private $nameEmployer;

    /**
     * @var string
     *
     * @ORM\Column(name="type_doc_employer", type="string", length=20)
     */
    private $typeDocEmployer;

    /**
     * @var string
     *
     * @ORM\Column(name="number_employer", type="string", length=50)
     */
    private $numberEmployer;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_employer", type="string", length=30)
     */
    private $phoneEmployer;

    /**
     * @var string
     *
     * @ORM\Column(name="email_employer", type="string", length=50)
     */
    private $emailEmployer;

    /**
     * @var string
     *
     * @ORM\Column(name="name_employee", type="string", length=50)
     */
    private $nameEmployee;

    /**
     * @var string
     *
     * @ORM\Column(name="type_doc_employee", type="string", length=20)
     */
    private $typeDocEmployee;

    /**
     * @var string
     *
     * @ORM\Column(name="number_doc_employee", type="string", length=50)
     */
    private $numberDocEmployee;

    /**
     * @var string
     *
     * @ORM\Column(name="email_employee", type="string", length=50)
     */
    private $emailEmployee;

    /**
     * @var string
     *
     * @ORM\Column(name="ciudad", type="string", length=100)
     */
    private $ciudad;

    /**
     * @var string
     *
     * @ORM\Column(name="cargo", type="string", length=50)
     */
    private $cargo;

    /**
     * @var string
     *
     * @ORM\Column(name="type_contract", type="string", length=20)
     */
    private $typeContract;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_init", type="datetime")
     */
    private $dateInit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_liquidation", type="datetime")
     */
    private $dateLiquidation;

    /**
     * @var string
     *
     * @ORM\Column(name="term_contract", type="string", length=255)
     */
    private $termContract;

    /**
     * @var int
     *
     * @ORM\Column(name="trans_subsidy", type="integer")
     */
    private $transSubsidy;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_prima", type="string", length=20)
     */
    private $paymentPrima;

    /**
     * @var string
     *
     * @ORM\Column(name="salary", type="string", length=20)
     */
    private $salary;

    /**
     * @var string
     *
     * @ORM\Column(name="jornada_laboral", type="string", length=20)
     */
    private $jornadaLaboral;

    /**
     * @var string
     *
     * @ORM\Column(name="dias_semana", type="string", length=255, nullable=true)
     */
    private $diasSemana;

    /**
     * @var string
     *
     * @ORM\Column(name="days_laborados", type="string", length=255, nullable=true)
     */
    private $daysLaborados;

    /**
     * @var string
     *
     * @ORM\Column(name="vacaciones", type="string", length=30)
     */
    private $vacaciones;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_cesantias", type="string", length=255)
     */
    private $paymentCesantias;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_primera_prima", type="string", length=255)
     */
    private $valorPrimeraPrima;

    /**
     * @var string
     *
     * @ORM\Column(name="base", type="string", length=255)
     */
    private $base;

    /**
     * @var string
     *
     * @ORM\Column(name="days_primer_semestre", type="string", length=255)
     */
    private $daysPrimerSemestre;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_segunda_prima", type="string", length=255)
     */
    private $valorSegundaPrima;

    /**
     * @var string
     *
     * @ORM\Column(name="days_segundo_semestre", type="string", length=255)
     */
    private $daysSegundoSemestre;

    /**
     * @var string
     *
     * @ORM\Column(name="days_cesantias_actual", type="string", length=255)
     */
    private $daysCesantiasActual;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_cesantias_actual", type="string", length=255)
     */
    private $valorCesantiasActual;

    /**
     * @var string
     *
     * @ORM\Column(name="days_cesantias_anterior", type="string", length=255)
     */
    private $daysCesantiasAnterior;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_cesantias_anterior", type="string", length=255)
     */
    private $valorCesantiasAnterior;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_int_cesantias_actual", type="string", length=255)
     */
    private $valorIntCesantiasActual;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_int_cesantias_anterior", type="string", length=255)
     */
    private $valorIntCesantiasAnterior;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_vacaciones", type="string", length=255)
     */
    private $valorVacaciones;

    /**
     * @var string
     *
     * @ORM\Column(name="dias_vacaciones", type="string", length=255)
     */
    private $diasVacaciones;

    /**
     * @var string
     *
     * @ORM\Column(name="days_indemnizacion", type="string", length=255)
     */
    private $daysIndemnizacion;

    /**
     * @var string
     *
     * @ORM\Column(name="valor_indemnizacion", type="string", length=255)
     */
    private $valorIndemnizacion;

    /**
     * @ORM\ManyToOne(targetEntity="RegisterStatus", inversedBy="id")
     * @ORM\JoinColumn(name="status", referencedColumnName="id")
     */
    private $status;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getValorPrimeraPrima()
    {
        return $this->valorPrimeraPrima;
    }

    /**
     * @param string $valorPrimeraPrima
     */
    public function setValorPrimeraPrima($valorPrimeraPrima)
    {
        $this->valorPrimeraPrima = $valorPrimeraPrima;
    }

    /**
     * @return string
     */
    public function getDaysPrimerSemestre()
    {
        return $this->daysPrimerSemestre;
    }

    /**
     * @param string $daysPrimerSemestre
     */
    public function setDaysPrimerSemestre($daysPrimerSemestre)
    {
        $this->daysPrimerSemestre = $daysPrimerSemestre;
    }

    /**
     * @return string
     */
    public function getDaysSegundoSemestre()
    {
        return $this->daysSegundoSemestre;
    }

    /**
     * @param string $daysSegundoSemestre
     */
    public function setDaysSegundoSemestre($daysSegundoSemestre)
    {
        $this->daysSegundoSemestre = $daysSegundoSemestre;
    }

    /**
     * @return string
     */
    public function getValorSegundaPrima()
    {
        return $this->valorSegundaPrima;
    }

    /**
     * @return string
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * @param string $base
     */
    public function setBase($base)
    {
        $this->base = $base;
    }

    /**
     * @param string $valorSegundaPrima
     */
    public function setValorSegundaPrima($valorSegundaPrima)
    {
        $this->valorSegundaPrima = $valorSegundaPrima;
    }

    /**
     * @return string
     */
    public function getDaysCesantiasActual()
    {
        return $this->daysCesantiasActual;
    }

    /**
     * @param string $daysCesantiasActual
     */
    public function setDaysCesantiasActual($daysCesantiasActual)
    {
        $this->daysCesantiasActual = $daysCesantiasActual;
    }

    /**
     * @return string
     */
    public function getDaysCesantiasAnterior()
    {
        return $this->daysCesantiasAnterior;
    }

    /**
     * @param string $daysCesantiasAnterior
     */
    public function setDaysCesantiasAnterior($daysCesantiasAnterior)
    {
        $this->daysCesantiasAnterior = $daysCesantiasAnterior;
    }

    /**
     * @return string
     */
    public function getValorCesantiasActual()
    {
        return $this->valorCesantiasActual;
    }

    /**
     * @param string $valorCesantiasActual
     */
    public function setValorCesantiasActual($valorCesantiasActual)
    {
        $this->valorCesantiasActual = $valorCesantiasActual;
    }

    /**
     * @return string
     */
    public function getValorCesantiasAnterior()
    {
        return $this->valorCesantiasAnterior;
    }

    /**
     * @param string $valorCesantiasAnterior
     */
    public function setValorCesantiasAnterior($valorCesantiasAnterior)
    {
        $this->valorCesantiasAnterior = $valorCesantiasAnterior;
    }

    /**
     * @return string
     */
    public function getValorIntCesantiasActual()
    {
        return $this->valorIntCesantiasActual;
    }

    /**
     * @param string $valorIntCesantiasActual
     */
    public function setValorIntCesantiasActual($valorIntCesantiasActual)
    {
        $this->valorIntCesantiasActual = $valorIntCesantiasActual;
    }

    /**
     * @return string
     */
    public function getValorIntCesantiasAnterior()
    {
        return $this->valorIntCesantiasAnterior;
    }

    /**
     * @param string $valorIntCesantiasAnterior
     */
    public function setValorIntCesantiasAnterior($valorIntCesantiasAnterior)
    {
        $this->valorIntCesantiasAnterior = $valorIntCesantiasAnterior;
    }

    /**
     * @return string
     */
    public function getDaysLaborados()
    {
        return $this->daysLaborados;
    }

    /**
     * @param string $daysLaborados
     */
    public function setDaysLaborados($daysLaborados)
    {
        $this->daysLaborados = $daysLaborados;
    }

    /**
     * @return string
     */
    public function getDiasSemana()
    {
        return $this->diasSemana;
    }

    /**
     * @param string $diasSemana
     */
    public function setDiasSemana($diasSemana)
    {
        $this->diasSemana = $diasSemana;
    }

    /**
     * @return string
     */
    public function getValorVacaciones()
    {
        return $this->valorVacaciones;
    }

    /**
     * @param string $valorVacaciones
     */
    public function setValorVacaciones($valorVacaciones)
    {
        $this->valorVacaciones = $valorVacaciones;
    }

    /**
     * @return string
     */
    public function getDiasVacaciones()
    {
        return $this->diasVacaciones;
    }

    /**
     * @param string $diasVacaciones
     */
    public function setDiasVacaciones($diasVacaciones)
    {
        $this->diasVacaciones = $diasVacaciones;
    }

    /**
     * @return string
     */
    public function getDaysIndemnizacion()
    {
        return $this->daysIndemnizacion;
    }

    /**
     * @param string $daysIndemnizacion
     */
    public function setDaysIndemnizacion($daysIndemnizacion)
    {
        $this->daysIndemnizacion = $daysIndemnizacion;
    }

    /**
     * @return string
     */
    public function getValorIndemnizacion()
    {
        return $this->valorIndemnizacion;
    }

    /**
     * @param string $valorIndemnizacion
     */
    public function setValorIndemnizacion($valorIndemnizacion)
    {
        $this->valorIndemnizacion = $valorIndemnizacion;
    }

    /**
     * Set nameEmployer
     *
     * @param string $nameEmployer
     *
     * @return register
     */
    public function setNameEmployer($nameEmployer)
    {
        $this->nameEmployer = $nameEmployer;

        return $this;
    }

    /**
     * Get nameEmployer
     *
     * @return string
     */
    public function getNameEmployer()
    {
        return $this->nameEmployer;
    }

    /**
     * Set typeDocEmployer
     *
     * @param string $typeDocEmployer
     *
     * @return register
     */
    public function setTypeDocEmployer($typeDocEmployer)
    {
        $this->typeDocEmployer = $typeDocEmployer;

        return $this;
    }

    /**
     * Get typeDocEmployer
     *
     * @return string
     */
    public function getTypeDocEmployer()
    {
        return $this->typeDocEmployer;
    }

    /**
     * Set numberEmployer
     *
     * @param string $numberEmployer
     *
     * @return register
     */
    public function setNumberEmployer($numberEmployer)
    {
        $this->numberEmployer = $numberEmployer;

        return $this;
    }

    /**
     * Get numberEmployer
     *
     * @return string
     */
    public function getNumberEmployer()
    {
        return $this->numberEmployer;
    }

    /**
     * Set phoneEmployer
     *
     * @param string $phoneEmployer
     *
     * @return register
     */
    public function setPhoneEmployer($phoneEmployer)
    {
        $this->phoneEmployer = $phoneEmployer;

        return $this;
    }

    /**
     * Get phoneEmployer
     *
     * @return string
     */
    public function getPhoneEmployer()
    {
        return $this->phoneEmployer;
    }

    /**
     * Set emailEmployer
     *
     * @param string $emailEmployer
     *
     * @return register
     */
    public function setEmailEmployer($emailEmployer)
    {
        $this->emailEmployer = $emailEmployer;

        return $this;
    }

    /**
     * Get emailEmployer
     *
     * @return string
     */
    public function getEmailEmployer()
    {
        return $this->emailEmployer;
    }

    /**
     * Set nameEmployee
     *
     * @param string $nameEmployee
     *
     * @return register
     */
    public function setNameEmployee($nameEmployee)
    {
        $this->nameEmployee = $nameEmployee;

        return $this;
    }

    /**
     * Get nameEmployee
     *
     * @return string
     */
    public function getNameEmployee()
    {
        return $this->nameEmployee;
    }

    /**
     * Set typeDocEmployee
     *
     * @param string $typeDocEmployee
     *
     * @return register
     */
    public function setTypeDocEmployee($typeDocEmployee)
    {
        $this->typeDocEmployee = $typeDocEmployee;

        return $this;
    }

    /**
     * Get typeDocEmployee
     *
     * @return string
     */
    public function getTypeDocEmployee()
    {
        return $this->typeDocEmployee;
    }

    /**
     * Set numberDocEmployee
     *
     * @param string $numberDocEmployee
     *
     * @return register
     */
    public function setNumberDocEmployee($numberDocEmployee)
    {
        $this->numberDocEmployee = $numberDocEmployee;

        return $this;
    }

    /**
     * Get numberDocEmployee
     *
     * @return string
     */
    public function getNumberDocEmployee()
    {
        return $this->numberDocEmployee;
    }

    /**
     * Set emailEmployee
     *
     * @param string $emailEmployee
     *
     * @return register
     */
    public function setEmailEmployee($emailEmployee)
    {
        $this->emailEmployee = $emailEmployee;

        return $this;
    }

    /**
     * Get emailEmployee
     *
     * @return string
     */
    public function getEmailEmployee()
    {
        return $this->emailEmployee;
    }

    /**
     * @return string
     */
    public function getCiudad()
    {
        return $this->ciudad;
    }

    /**
     * @param string $ciudad
     */
    public function setCiudad($ciudad)
    {
        $this->ciudad = $ciudad;
    }

    /**
     * Set cargo
     *
     * @param string $cargo
     *
     * @return register
     */
    public function setCargo($cargo)
    {
        $this->cargo = $cargo;

        return $this;
    }

    /**
     * Get cargo
     *
     * @return string
     */
    public function getCargo()
    {
        return $this->cargo;
    }

    /**
     * Set typeContract
     *
     * @param string $typeContract
     *
     * @return register
     */
    public function setTypeContract($typeContract)
    {
        $this->typeContract = $typeContract;

        return $this;
    }

    /**
     * Get typeContract
     *
     * @return string
     */
    public function getTypeContract()
    {
        return $this->typeContract;
    }

    /**
     * Set dateInit
     *
     * @param \DateTime $dateInit
     *
     * @return register
     */
    public function setDateInit($dateInit)
    {
        $this->dateInit = $dateInit;

        return $this;
    }

    /**
     * Get dateInit
     *
     * @return \DateTime
     */
    public function getDateInit()
    {
        return $this->dateInit;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return register
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set dateLiquidation
     *
     * @param \DateTime $dateLiquidation
     *
     * @return register
     */
    public function setDateLiquidation($dateLiquidation)
    {
        $this->dateLiquidation = $dateLiquidation;

        return $this;
    }

    /**
     * Get dateLiquidation
     *
     * @return \DateTime
     */
    public function getDateLiquidation()
    {
        return $this->dateLiquidation;
    }

    /**
     * Set termContract
     *
     * @param string $termContract
     *
     * @return register
     */
    public function setTermContract($termContract)
    {
        $this->termContract = $termContract;

        return $this;
    }

    /**
     * Get termContract
     *
     * @return string
     */
    public function getTermContract()
    {
        return $this->termContract;
    }

    /**
     * Set transSubsidy
     *
     * @param integer $transSubsidy
     *
     * @return register
     */
    public function setTransSubsidy($transSubsidy)
    {
        $this->transSubsidy = $transSubsidy;

        return $this;
    }

    /**
     * Get transSubsidy
     *
     * @return int
     */
    public function getTransSubsidy()
    {
        return $this->transSubsidy;
    }

    /**
     * Set paymentPrima
     *
     * @param string $paymentPrima
     *
     * @return register
     */
    public function setPaymentPrima($paymentPrima)
    {
        $this->paymentPrima = $paymentPrima;

        return $this;
    }

    /**
     * Get paymentPrima
     *
     * @return string
     */
    public function getPaymentPrima()
    {
        return $this->paymentPrima;
    }

    /**
     * Set salary
     *
     * @param string $salary
     *
     * @return register
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;

        return $this;
    }

    /**
     * Get salary
     *
     * @return string
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * Set jornadaLaboral
     *
     * @param string $jornadaLaboral
     *
     * @return register
     */
    public function setJornadaLaboral($jornadaLaboral)
    {
        $this->jornadaLaboral = $jornadaLaboral;

        return $this;
    }

    /**
     * Get jornadaLaboral
     *
     * @return string
     */
    public function getJornadaLaboral()
    {
        return $this->jornadaLaboral;
    }

    /**
     * Set vacaciones
     *
     * @param string $vacaciones
     *
     * @return register
     */
    public function setVacaciones($vacaciones)
    {
        $this->vacaciones = $vacaciones;

        return $this;
    }

    /**
     * Get vacaciones
     *
     * @return string
     */
    public function getVacaciones()
    {
        return $this->vacaciones;
    }

    /**
     * Set paymentCesantias
     *
     * @param string $paymentCesantias
     *
     * @return register
     */
    public function setPaymentCesantias($paymentCesantias)
    {
        $this->paymentCesantias = $paymentCesantias;

        return $this;
    }

    /**
     * Get paymentCesantias
     *
     * @return string
     */
    public function getPaymentCesantias()
    {
        return $this->paymentCesantias;
    }

    /**
     * Set status
     *
     * @param \AppBundle\Entity\RegisterStatus $status
     *
     * @return register
     */
    public function setStatus(\AppBundle\Entity\RegisterStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AppBundle\Entity\RegisterStatus
     */
    public function getStatus()
    {
        return $this->status;
    }
}
