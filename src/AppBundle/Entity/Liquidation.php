<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Liquidation
 *
 * @ORM\Table(name="liquidation", indexes={@ORM\Index(name="contrato", columns={"contract"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LiquidationRepository")
 */
class Liquidation
{
    /**
     * @var string
     *
     * @ORM\Column(name="cesantias", type="string", length=255, nullable=false)
     */
    private $cesantias;

    /**
     * @var string
     *
     * @ORM\Column(name="interes_cesantias", type="string", length=255, nullable=false)
     */
    private $interesCesantias;

    /**
     * @var string
     *
     * @ORM\Column(name="vacaciones", type="string", length=255, nullable=false)
     */
    private $vacaciones;

    /**
     * @var string
     *
     * @ORM\Column(name="indemnizacion", type="string", length=255, nullable=false)
     */
    private $indemnizacion;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Contract
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Contract")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contract", referencedColumnName="id")
     * })
     */
    private $contract;



    /**
     * Set cesantias
     *
     * @param string $cesantias
     *
     * @return Liquidation
     */
    public function setCesantias($cesantias)
    {
        $this->cesantias = $cesantias;

        return $this;
    }

    /**
     * Get cesantias
     *
     * @return string
     */
    public function getCesantias()
    {
        return $this->cesantias;
    }

    /**
     * Set interesCesantias
     *
     * @param string $interesCesantias
     *
     * @return Liquidation
     */
    public function setInteresCesantias($interesCesantias)
    {
        $this->interesCesantias = $interesCesantias;

        return $this;
    }

    /**
     * Get interesCesantias
     *
     * @return string
     */
    public function getInteresCesantias()
    {
        return $this->interesCesantias;
    }

    /**
     * Set vacaciones
     *
     * @param string $vacaciones
     *
     * @return Liquidation
     */
    public function setVacaciones($vacaciones)
    {
        $this->vacaciones = $vacaciones;

        return $this;
    }

    /**
     * Get vacaciones
     *
     * @return string
     */
    public function getVacaciones()
    {
        return $this->vacaciones;
    }

    /**
     * Set indemnizacion
     *
     * @param string $indemnizacion
     *
     * @return Liquidation
     */
    public function setIndemnizacion($indemnizacion)
    {
        $this->indemnizacion = $indemnizacion;

        return $this;
    }

    /**
     * Get indemnizacion
     *
     * @return string
     */
    public function getIndemnizacion()
    {
        return $this->indemnizacion;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contract
     *
     * @param \AppBundle\Entity\Contract $contract
     *
     * @return Liquidation
     */
    public function setContract(\AppBundle\Entity\Contract $contract = null)
    {
        $this->contract = $contract;

        return $this;
    }

    /**
     * Get contract
     *
     * @return \AppBundle\Entity\Contract
     */
    public function getContract()
    {
        return $this->contract;
    }
}
