<?php

namespace AppBundle\service;

use Doctrine\ORM\EntityManager;
use Swift_Attachment;
use Symfony\Component\Templating\EngineInterface;

class Mailer
{

    /**
     * @var \Twig_Environment
     */
    private $templating;

    private $mailer;


    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $templating)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;

    }

    public function sendEmail($paths,$register = null,$email = null){

        $message = \Swift_Message::newInstance()
            ->setSubject('Tus cálculos de liquidación y documentos ya están aquí')
            ->setFrom('documentos@afiliese.co')
            ->setTo(  ($email != null ? $email : $register->getEmailEmployer()) )
            ->setCc(array('esteban.palma@symplifica.com', 'cindy.riveros@symplifica.com','fernando.leon@symplifica.com','frank.sanabria@symplifica.com'))
            ->setBody($this->templating->render(
                'emails/email.html.twig', array(
                    'register'    => $register,
                    'paths'    => $paths,
            )),'text/html');

        if(COUNT($paths) > 0){
            foreach ($paths as $path){
                $message->attach(\Swift_Attachment::fromPath($path));
            }
        }


        $this->mailer->send($message);

    }


}