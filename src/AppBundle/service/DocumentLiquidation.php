<?php
/**
 * Created by PhpStorm.
 * User: franksanabria
 * Date: 11/21/18
 * Time: 4:13 PM
 */

namespace AppBundle\service;

use Doctrine\ORM\EntityManager;
use Knp\Snappy\Pdf;


class DocumentLiquidation
{
    private $twig;
    private $entityManager;
    private $pdf;
    private $kernelDir;
    private $pathTempFolder;


    public function __construct(\Twig_Environment $twig_Enviroment, EntityManager $entityManager,Pdf $pdf, $kernelDir)
    {
        $this->twig          = $twig_Enviroment;
        $this->entityManager = $entityManager;
        $this->pdf           = $pdf;
        $this->kernelDir     = $kernelDir;

    }

    public function Liquidation($sql){

        $terminationContract = $sql->getTermContract();

        $this->CarpetaTemporal($sql->getId());

        if($terminationContract == "Renuncia"){

            $pdf = $this->RenunciaAction($sql);

        }elseif ($terminationContract == "Sin justa causa"){

            $pdf = $this->despidoSinCausaAction($sql);

        }elseif ($terminationContract == "Periodo de prueba"){
            //pendiente pdf ya que no esta en requerimientos
            $pdf = $this->PeriodoPruebaAction($sql);

        }elseif ($terminationContract == "Mutuo acuerdo"){

            $pdf = $this->MutuoAcuerdoAction($sql);

        }elseif ($terminationContract == "Terminación término fijo"){

            $pdf = $this->TerminoFijoAction($sql);

        }else{

            $pdf = $this->DespidoConCausaAction($sql);
        }

        return $pdf;
    }


    private function despidoSinCausaAction($sql){

        $certificadoLaboral = $this->twig->render('documents/certificado-laboral.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($certificadoLaboral, $this->pathTempFolder."/CertificadoLaboral.pdf");


        $liquidacion  =  $this->twig->render('documents/liquidacion.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($liquidacion, $this->pathTempFolder."/CalculosDeLiquidacion.pdf");


        $cartaSinCausa  =  $this->twig->render('documents/carta-terminacion-sin-justa-causa.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($cartaSinCausa, $this->pathTempFolder."/CartaTerminacionSinCausa.pdf");


        $notificacionTerminacion  =  $this->twig->render('documents/notificacion-termino-contrato.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($notificacionTerminacion, $this->pathTempFolder."/NotificacionTerminoContratoSinJustaCausa.pdf");

        return array(
            '0'       => $this->pathTempFolder."/CertificadoLaboral.pdf",
            '1'       => $this->pathTempFolder."/CalculosDeLiquidacion.pdf",
            '2'       => $this->pathTempFolder."/CartaTerminacionSinCausa.pdf",
            '3'       => $this->pathTempFolder."/NotificacionTerminoContratoSinJustaCausa.pdf"
        );
    }


    private function RenunciaAction($sql){

        $liquidacion  =  $this->twig->render('documents/liquidacion.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($liquidacion, $this->pathTempFolder."/CalculosDeLiquidacion.pdf");


        $aceptaRenuncia  =  $this->twig->render('documents/carta-acepta-renuncia.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($aceptaRenuncia, $this->pathTempFolder."/AceptarRenuncia.pdf");


        $certificadoLaboral = $this->twig->render('documents/certificado-laboral.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($certificadoLaboral, $this->pathTempFolder."/CertificadoLaboral.pdf");


        return array(
            '0'       => $this->pathTempFolder."/CalculosDeLiquidacion.pdf",
            '1'       => $this->pathTempFolder."/AceptarRenuncia.pdf",
            '2'       => $this->pathTempFolder."/CertificadoLaboral.pdf"
        );
    }

    private function PeriodoPruebaAction($sql){

        $certificadoLaboral = $this->twig->render('documents/certificado-laboral.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($certificadoLaboral, $this->pathTempFolder."/CertificadoLaboral.pdf");


        $cartaPeriodo = $this->twig->render('documents/carta-terminacion-periodo.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($cartaPeriodo, $this->pathTempFolder."/CartaTerminacionPeriodo.pdf");

        $liquidacion  =  $this->twig->render('documents/liquidacion.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($liquidacion, $this->pathTempFolder."/CalculosDeLiquidacion.pdf");

        return array(
            '0'       => $this->pathTempFolder."/CertificadoLaboral.pdf",
            '1'       => $this->pathTempFolder."/CartaTerminacionPeriodo.pdf",
            '2'       => $this->pathTempFolder."/CalculosDeLiquidacion.pdf"
        );
    }

    private function TerminoFijoAction($sql){

        $certificadoLaboral = $this->twig->render('documents/certificado-laboral.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($certificadoLaboral, $this->pathTempFolder."/CertificadoLaboral.pdf");


        $liquidacion  =  $this->twig->render('documents/liquidacion.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($liquidacion, $this->pathTempFolder."/CalculosDeLiquidacion.pdf");


        $notificacionTerminacion  =  $this->twig->render('documents/notificacion-termino-contrato.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($notificacionTerminacion, $this->pathTempFolder."/NotificacionTerminoContratoFijo.pdf");

        return array(
            '0'       => $this->pathTempFolder."/CertificadoLaboral.pdf",
            '1'       => $this->pathTempFolder."/CalculosDeLiquidacion.pdf",
            '2'       => $this->pathTempFolder."/NotificacionTerminoContratoFijo.pdf"
        );
    }

    private function DespidoConCausaAction($sql){

        $certificadoLaboral = $this->twig->render('documents/certificado-laboral.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($certificadoLaboral, $this->pathTempFolder."/CertificadoLaboral.pdf");


        $liquidacion  =  $this->twig->render('documents/liquidacion.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($liquidacion, $this->pathTempFolder."/CalculosDeLiquidacion.pdf");


        $notificacionTerminacion  =  $this->twig->render('documents/notificacion-termino-contrato.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($notificacionTerminacion, $this->pathTempFolder."/NotificacionTerminoContratoJustaCausa.pdf");

        return array(
            '0'       => $this->pathTempFolder."/CertificadoLaboral.pdf",
            '1'       => $this->pathTempFolder."/CalculosDeLiquidacion.pdf",
            '2'       => $this->pathTempFolder."/NotificacionTerminoContratoJustaCausa.pdf"
        );
    }

    private function MutuoAcuerdoAction($sql){

        $certificadoLaboral = $this->twig->render('documents/certificado-laboral.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($certificadoLaboral, $this->pathTempFolder."/CertificadoLaboral.pdf");


        $liquidacion  =  $this->twig->render('documents/liquidacion.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($liquidacion, $this->pathTempFolder."/CalculosDeLiquidacion.pdf");


        $cartaMutuoAcuerdo  =  $this->twig->render('documents/carta-mutuo-acuerdo.html.twig',
            array('reg' => $sql));
        $this->pdf->generateFromHtml($cartaMutuoAcuerdo, $this->pathTempFolder."/CartaDeMutuoAcuerdo.pdf");

        return array(
            '0'       => $this->pathTempFolder."/CertificadoLaboral.pdf",
            '1'       => $this->pathTempFolder."/CalculosDeLiquidacion.pdf",
            '2'       => $this->pathTempFolder."/CartaDeMutuoAcuerdo.pdf"
        );
    }

    public function deleteTemp(){

        $files = glob($this->pathTempFolder.'/*');
        foreach($files as $file){
            if(is_file($file))
                unlink($file);
        }

    }

    private  function CarpetaTemporal($id){

        $this->pathTempFolder = $this->kernelDir."/../web/temp/".$id;
        if (!file_exists($this->pathTempFolder)) {
            mkdir($this->pathTempFolder, 0777, true);
        }else{
            $this->deleteTemp();
        }

    }

}