<?php
/**
 * Created by PhpStorm.
 * User: symplificadev
 * Date: 5/12/18
 * Time: 9:07
 */

namespace AppBundle\service;


class Utilities
{

    public function monthToText($month){
        $months = [
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre',
        ];
        return $months[$month];
    }

    public function getDayMonthToLeters($day){
        switch ($day){
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
                return $this->numberOneToNineTeen($day);
            case 20:

                return 'Veinte';
            case 21:
            case 22:
            case 23:
            case 24:
            case 25:
            case 26:
            case 27:
            case 28:
            case 29:
                return 'Veinti'.$this->numberOneToNineTeen($day%10);
            case 30:
                return 'Treinta';
            case 31:
                return 'treinta y uno';
        }
        return "";
    }

    private function numberOneToNineTeen($number){
        switch ($number){
            case 1:
                return 'Un';
            case 2:
                return 'Dós';
            case 3:
                return 'Trés';
            case 4:
                return 'Cuatro';
            case 5:
                return 'Cinco';
            case 6:
                return 'Séis';
            case 7:
                return 'Siete';
            case 8:
                return 'Ocho';
            case 9:
                return 'Nueve';
            case 10:
                return 'Diez';
            case 11:
                return 'Once';
            case 12:
                return 'Doce';
            case 13:
                return 'Trece';
            case 14:
                return 'Catorce';
            case 15:
                return 'Quince';
            case 16:
                return 'Dieciseis';
            case 17:
                return 'Diecisiete';
            case 18:
                return 'Dieciocho';
            case 19:
                return 'Diecinueve';
        }
    }

    public function letersTypeContract($typeContract){
        if($typeContract == 'TI'){
            return 'Término Indefinido';
        }else{
            return 'Término Fijo';
        }
    }

    public function letersTypeOfDay($typeContract){
        if($typeContract == 'TC'){
            return 'tiempo completo';
        }else{
            return 'tiempo parcial';
        }
    }

    /**
     * @param \DateTime $dateToCheck
     * @return bool
     */
    public function workable($dateToCheck)
    {
        $holyDays = array();
        $date = new \DateTime("2016-" . "01" . "-" . "01");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "01" . "-" . "11");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "03" . "-" . "21");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "03" . "-" . "24");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "03" . "-" . "25");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "05" . "-" . "01");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "05" . "-" . "09");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "05" . "-" . "30");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "06" . "-" . "06");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "07" . "-" . "04");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "07" . "-" . "20");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "08" . "-" . "07");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "08" . "-" . "15");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "10" . "-" . "17");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "11" . "-" . "07");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "11" . "-" . "14");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "12" . "-" . "08");
        $holyDays[] = $date;
        $date = new \DateTime("2016-" . "12" . "-" . "25");
        $holyDays[] = $date;


        $date = new \DateTime("2017-" . "01" . "-" . "01");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "01" . "-" . "09");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "03" . "-" . "20");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "04" . "-" . "09");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "04" . "-" . "13");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "04" . "-" . "14");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "04" . "-" . "16");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "05" . "-" . "01");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "05" . "-" . "29");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "06" . "-" . "19");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "06" . "-" . "26");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "07" . "-" . "03");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "07" . "-" . "20");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "08" . "-" . "07");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "08" . "-" . "21");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "10" . "-" . "16");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "11" . "-" . "06");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "11" . "-" . "13");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "12" . "-" . "08");
        $holyDays[] = $date;
        $date = new \DateTime("2017-" . "12" . "-" . "25");
        $holyDays[] = $date;


        $date = new \DateTime("2018-" . "01" . "-" . "01");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "01" . "-" . "08");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "03" . "-" . "19");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "03" . "-" . "29");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "03" . "-" . "30");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "05" . "-" . "01");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "05" . "-" . "14");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "06" . "-" . "04");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "06" . "-" . "11");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "07" . "-" . "02");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "07" . "-" . "20");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "08" . "-" . "07");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "08" . "-" . "20");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "10" . "-" . "15");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "11" . "-" . "05");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "11" . "-" . "12");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "12" . "-" . "08");
        $holyDays[] = $date;
        $date = new \DateTime("2018-" . "12" . "-" . "25");
        $holyDays[] = $date;


        $date = new \DateTime("2019-" . "01" . "-" . "01");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "01" . "-" . "07");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "03" . "-" . "25");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "04" . "-" . "14");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "04" . "-" . "18");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "04" . "-" . "19");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "04" . "-" . "21");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "05" . "-" . "01");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "06" . "-" . "03");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "06" . "-" . "24");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "07" . "-" . "01");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "07" . "-" . "20");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "08" . "-" . "07");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "08" . "-" . "19");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "10" . "-" . "14");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "11" . "-" . "04");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "11" . "-" . "11");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "12" . "-" . "08");
        $holyDays[] = $date;
        $date = new \DateTime("2019-" . "12" . "-" . "25");
        $holyDays[] = $date;

        /** @var \DateTime $hd */
        foreach ($holyDays as $hd) {
            if ($dateToCheck->format("Y-m-d") == $hd->format("Y-m-d"))
                return false;
        }

        return true;
    }

}