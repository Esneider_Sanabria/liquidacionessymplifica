<?php

namespace AppBundle\service;

class Compensation{

    private $period;
    private $typeContract;
    private $utilities;
    private $dateEnd;

    public function __construct(Utilities $utilities){

        $this->utilities = $utilities;
    }

    public function calculate($reason,$period, $typeContract, $salary, $daysMonthWorcable, $timeCommitment, $daysWeek){
        $this->period = $period;
        $this->typeContract = $typeContract;

        $result = [
            'units' => 0,
            'base' => 0,
            'value' => 0
        ];

        if($this->isValid($this->getTestPeriod(), $reason)){
            switch ($typeContract){
                case 'TI':
                    $result = $this->calculateTI($salary, $daysMonthWorcable, $timeCommitment);
                    break;
                case 'TF':
                    $result = $this->calculateTF($salary, $daysMonthWorcable, $timeCommitment, $daysWeek);
                    break;
            }
        }
        return $result;
    }

    private function calculateTI($salary, $daysMonthWorcable, $timeCommitment){
        $this->period['start_date']->modify('+1 day');
        $years = ceil($this->days360(
            $this->period['start_date']->format('Y-m-d'),
            $this->period['end_date']->format('Y-m-d')
        )/360);

        $base = ($timeCommitment == 'TC')
            ? ($salary/$daysMonthWorcable)
            : $salary;

        $units = ($years > 1) ? (30 + (--$years * 20)) : 30;
        $value = $base * $units;
        return [
            'units' => $units,
            'base' => $base,
            'value' => $value
        ];
    }

    private function calculateTF($salary, $daysMonthWorcable, $timeCommitment, $daysWeek){

        $this->period['date_end']->modify('+1 day');
        $this->dateEnd = $this->period['end_date'];
        error_log('Fecha Fin de liquidación '.$this->period['end_date']->format('Y-m-d'));

        $units =
            ($timeCommitment == 'TC')
            ? $this->days360($this->period['date_end']->format('Y-m-d'), $this->period['end_date']->format('Y-m-d'))
            : $this->daysTrabajados($this->period['date_end']->format('Y-m-d'), $this->period['end_date']->format('Y-m-d'), $daysWeek)
            ;
        $base = ($timeCommitment == 'TC')
            ? ($salary/$daysMonthWorcable)
            : $salary;
        $value = $base * $units;
        return [
            'units' => $units,
            'base' => $base,
            'value' => $value
        ];
    }

    private function getTestPeriod(){
        $end_test_period = clone $this->period['start_date'];
        switch ($this->typeContract){
            case 'TI':
                $end_test_period->modify('+2 months');
                break;
            case 'TF':
                $diff = intval($this->period['start_date']->diff($this->period['end_date'])->days/5);
                $days =
                    ($diff > 60)
                    ? 60
                    : $diff
                ;
                $end_test_period->modify('+'.$days.' days');
                break;
        }
        return $end_test_period;
    }

    private function isValid(\DateTime $end_test, $reason){
        if($reason != 'Sin justa causa' && $reason != 'Periodo de prueba'){
            return false;
        }
        return ($end_test <= $this->period['date_end']);
    }

    public function days360($dateInit,$dateEnd){

        $period = array();
        $period['dateInit'] =  new \DateTime($dateInit);
        $period['dateEnd']  =  new \DateTime($dateEnd);

        return   $this->calculateUnitsPrestacionesSociales($period,'TC');
    }

    public function daysTrabajados($dateInit,$dateEnd, $diasLaborados){

        $period = array();
        $period['dateInit'] =  new \DateTime($dateInit);
        $period['dateEnd']  =  new \DateTime($dateEnd);

        return   $this->calculateUnitsPrestacionesSociales($period,'XD',$diasLaborados);
    }

    public function calculateUnitsPrestacionesSociales($period, $typeContract, $arrayDays = false)
    {
        $units = 0;
        $dateReferenced = new \DateTime($period['dateInit']->format('Y') . '-' . $period['dateInit']->format('m') . '-' . $period['dateInit']->format('d'));
        $dateReferencedEnd = new \DateTime($period['dateInit']->format('Y') . '-' . $period['dateInit']->format('m') . '-30');

        if ($period['dateEnd']->format('d') > 30){
            $period['dateEnd'] = new \DateTime($period['dateEnd']->format('Y-m-30'));
        }
        if ($dateReferencedEnd > $period['dateEnd']) {
            $dateReferencedEnd = new \DateTime($period['dateEnd']->format('Y-m-d'));
        }

        do {
            $unitsMonth = $this->unitsMonth($dateReferenced, $dateReferencedEnd,$typeContract,$arrayDays);

            if ($dateReferenced->format('m') == '02' && $typeContract == 'TC' && $unitsMonth > 27) {
                $unitsMonth += 30 - $unitsMonth;
            }

            $month = $dateReferenced->format('m') + 1;
            $year = $dateReferenced->format('Y');

            if ($month > 12) {
                $year++;
                $month = '01';
            }
            $dayEnd = $month == 2 ? 28 : 30;

            $dateReferencedEnd = new \DateTime($year . '-' . $month . '-' . $dayEnd);
            $dateReferenced = new \DateTime($year . '-' . $month . '-01');

            if ($dateReferencedEnd->format('m') == $period['dateEnd']->format('m') && $dateReferencedEnd->format('Y') == $period['dateEnd']->format('Y')) {
                $dateReferencedEnd = clone $period['dateEnd'];
            }
            $units += $unitsMonth;


        } while ($dateReferencedEnd <= $period['dateEnd']);

        return $units;
    }

    //Se calculan los dias contrato trabajados en el perido
    private function unitsMonth(\DateTime $startDate,\DateTime $endDate,$typeContract,$arrayDays = false)
    {
        if ($typeContract == 'XD') {
            return $this->daysWorkedXD($startDate, $endDate,$arrayDays);
        }
        return  $this->daysWorkedTC($startDate, $endDate);
    }


    public function daysWorkedXD(\DateTime $dateRStart, \DateTime $dateREnd,$arrayDays)
    {
        $wkd = array();
        /** @var WeekWorkableDays $wwd */
        foreach ($arrayDays as $wwd) {
            $wkd[$wwd] = true;
        }

        $dateREnd->modify('+1 day');

        $dateRStart = new \DateTime($dateRStart->format('Y').'-'.$dateRStart->format('m').'-'.$dateRStart->format('d').' 00:00:00.000');

        if(($this->dateEnd->format('Y') == $dateREnd->format('Y') and $this->dateEnd->format('m') == $dateREnd->format('m')) and $this->dateEnd->format('d') != 31 ){
            $dateREnd   = new \DateTime($dateREnd->format('Y').'-'.$dateREnd->format('m').'-'.$dateREnd->format('d').' 00:00:00.000');
        }else{
            $dateREnd   = new \DateTime($dateREnd->format('Y').'-'.$dateREnd->format('m').'-'.$dateREnd->format('d').' 24:59:00.000');
        }

        $interval = $dateRStart->diff($dateREnd);
        $units = 0;

        $numberDays = $interval->format('%R%a dias');

        for ($i = 0; $i < $numberDays; $i++) {
            $dateToCheck = new \DateTime();
            $dateToCheck->setDate($dateRStart->format("Y"), $dateRStart->format("m"), intval($dateRStart->format("d")) + $i);
            if ($this->utilities->workable($dateToCheck) && isset($wkd[$dateToCheck->format("w")])) {
                if($dateToCheck->format('m') == $dateRStart->format('m')){
                     if($dateToCheck->format('m') == '05'){
                         error_log($dateToCheck->format('Y-m-d'));
                     }
                    $units++;
                }

            }
        }
        return $units;
    }

    private function daysWorkedTC(\DateTime $startDate,\DateTime $endDate)
    {
        $s = new \DateTime($startDate->format('Y-m-d'));
        $e = new \DateTime($endDate->format('Y-m-d'));
        $interval = $s->diff($e);
        $daysWorked = $interval->format('%R%a') + 1;
        return $daysWorked;
    }
}