<?php

/* documents/liquidacion.html.twig */
class __TwigTemplate_88077d39c7b89708533fbeb52402149c2571d65a13dfb366c6328aca45f335f3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "documents/liquidacion.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "documents/liquidacion.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Simulación de liquidación</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        body, h1, h2, h3, h4, h5, p, a {
            font-family: 'Arial', sans-serif;
            color: #444;
        }

        h2 {
            font-size: 18px;
            text-align: center;
        }

        h3 {
            background: #e4e4e4;
            border: 2px solid #bebebe;
            border-bottom: none;
            width: 100%;
            height: 20px;
            line-height: 20px;
            text-align: center;
            font-size: 15px;
        }

        p, li {
            line-height: 1.1;
            font-size: 12px;
        }

        th {
            font-size: 13px;
            text-align: left;
            background: #ededed;
            border: 1px solid #bebebe;
            height: 15px;
            padding: 2px 5px;
        }

        td {
            font-size: 13px;
            min-height: 45px;

        }

        table tr td table {
            border: 1px solid #bebebe;
        }

        table tr td table tr td {
            border: 1px solid #bebebe;
            height: 10px;
            padding: 2px 5px;
        }

        table td table tr.no-border td {
            border: none !important;
        }

        table td table tr.sub td {
            background: #ededed;
            font-weight: bold;
        }

        table td table tr.total td {
            background: #bebebe;
            font-weight: bold;
            font-size: 15px;
            border-color: #7c7c7c;
        }
    </style>
</head>
<body>
<table width=\"90%\" style=\"max-width: 2000px; margin: 0 auto;\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
    ";
        // line 82
        $context["cantidad"] = twig_split_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 82, $this->source); })()), "diasSemana", []), ",");
        // line 83
        echo "    <tr>
        <td colspan=\"3\" height=\"15\"></td>
    </tr>
    <tr>
        <td colspan=\"3\"><h2>CÁLCULOS DE LIQUIDACIÓN FINAL DE CONTRATO DE TRABAJO</h2></td>
    </tr>
    <tr>
        <td colspan=\"3\" height=\"15\"></td>
    </tr>
    <tr>
        <td width=\"960\" valign=\"top\">
            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                <tr>
                    <th bgcolor=\"#ededed\">EMPLEADOR</th>
                </tr>
                <tr>
                    <td>";
        // line 99
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 99, $this->source); })()), "nameEmployer", []), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <td>";
        // line 102
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 102, $this->source); })()), "typeDocEmployer", []), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 102, $this->source); })()), "numberEmployer", []), "html", null, true);
        echo "</td>
                </tr>
            </table>
        </td>
        <td width=\"90\"></td>
        <td width=\"960\" valign=\"top\">
            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                <tr>
                    <th bgcolor=\"#ededed\">EMPLEADO</th>
                </tr>
                <tr>
                    <td>";
        // line 113
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 113, $this->source); })()), "nameEmployee", []), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <td>";
        // line 116
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 116, $this->source); })()), "typeDocEmployee", []), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 116, $this->source); })()), "numberDocEmployee", []), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <td>Cargo: ";
        // line 119
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 119, $this->source); })()), "cargo", []), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <td>Causa de la liquidación: ";
        // line 122
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 122, $this->source); })()), "termContract", []), "html", null, true);
        echo "</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td colspan=\"3\" height=\"15\"></td>
    </tr>

    <tr>
        <td width=\"960\" valign=\"top\" colspan=\"3\">
            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                <tr>
                    <th colspan=\"2\" bgcolor=\"#ededed\">CONTRATO</th>
                </tr>
                <tr>
                    <td width=\"507\">Días laborados por semana:</td>
                    ";
        // line 140
        if ((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 140, $this->source); })()), "jornadaLaboral", []) == "TC")) {
            // line 141
            echo "                        <td width=\"300\">7</td>
                    ";
        } else {
            // line 143
            echo "                        <td width=\"300\">
                            ";
            // line 144
            echo twig_escape_filter($this->env, twig_length_filter($this->env, (isset($context["cantidad"]) || array_key_exists("cantidad", $context) ? $context["cantidad"] : (function () { throw new Twig_Error_Runtime('Variable "cantidad" does not exist.', 144, $this->source); })())), "html", null, true);
            echo "
                        </td>
                    ";
        }
        // line 147
        echo "                </tr>
                <tr>
                    <td width=\"507\">Días laborados por mes:</td>
                    ";
        // line 150
        if ((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 150, $this->source); })()), "jornadaLaboral", []) == "TC")) {
            // line 151
            echo "                        <td width=\"300\">30</td>
                    ";
        } else {
            // line 153
            echo "                        <td width=\"300\">";
            echo twig_escape_filter($this->env, (twig_length_filter($this->env, (isset($context["cantidad"]) || array_key_exists("cantidad", $context) ? $context["cantidad"] : (function () { throw new Twig_Error_Runtime('Variable "cantidad" does not exist.', 153, $this->source); })())) * 4), "html", null, true);
            echo "</td>
                    ";
        }
        // line 155
        echo "                </tr>
                <tr>
                    <td width=\"507\">Salario:</td>
                    ";
        // line 158
        if ((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 158, $this->source); })()), "jornadaLaboral", []) == "TC")) {
            // line 159
            echo "                        <td width=\"300\">\$ ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 159, $this->source); })()), "salary", [])), "html", null, true);
            echo "</td>
                    ";
        } else {
            // line 161
            echo "                            <td width=\"300\">\$ ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 161, $this->source); })()), "salary", []) * twig_length_filter($this->env, (isset($context["cantidad"]) || array_key_exists("cantidad", $context) ? $context["cantidad"] : (function () { throw new Twig_Error_Runtime('Variable "cantidad" does not exist.', 161, $this->source); })()))) * 4)), "html", null, true);
            echo "</td>
                    ";
        }
        // line 163
        echo "                </tr>
                <tr>
                    <td width=\"507\">Fecha de inicio del contrato:</td>
                    <td width=\"300\">";
        // line 166
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 166, $this->source); })()), "dateInit", []), "d/m/Y"), "html", null, true);
        echo "</td>
                </tr>
                ";
        // line 168
        if ((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 168, $this->source); })()), "typeContract", []) == "TC")) {
            // line 169
            echo "                    <tr>
                        <td width=\"507\">Fecha final del contrato:</td>
                        <td width=\"300\">";
            // line 171
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 171, $this->source); })()), "dateEnd", []), "d/m/Y"), "html", null, true);
            echo "</td>
                    </tr>
                ";
        }
        // line 174
        echo "                <tr>
                    <td width=\"507\">Fecha liquidación del contrato:</td>
                    <td width=\"300\">";
        // line 176
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 176, $this->source); })()), "dateLiquidation", []), "d/m/Y"), "html", null, true);
        echo "</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan=\"3\" height=\"15\"></td>
    </tr>
    <tr>
        <td width=\"960\">
            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                <tr>
                    <th colspan=\"2\" bgcolor=\"#ededed\">VACACIONES</th>
                </tr>
                <tr>
                    <td width=\"507\">Días causados:</td>
                    <td width=\"300\">";
        // line 192
        echo twig_escape_filter($this->env, twig_round(twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 192, $this->source); })()), "diasVacaciones", []), 1), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <td width=\"507\">Días tomados:</td>
                    <td width=\"300\">";
        // line 196
        echo twig_escape_filter($this->env, twig_round(twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 196, $this->source); })()), "vacaciones", []), 1), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <td width=\"507\">Días pendientes:</td>
                    <td width=\"300\">";
        // line 200
        echo twig_escape_filter($this->env, twig_round((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 200, $this->source); })()), "diasVacaciones", []) - twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 200, $this->source); })()), "vacaciones", [])), 1), "html", null, true);
        echo "</td>
                </tr>
            </table>
        </td>
        <td width=\"90\"></td>
        <td width=\"960\">
            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                <tr>
                    <th colspan=\"2\" bgcolor=\"#ededed\">BASES DE LIQUIDACIÓN</th>
                </tr>

                <tr>
                    <td width=\"507\">Base cesantías:</td>
                    <td width=\"300\">\$ ";
        // line 213
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 213, $this->source); })()), "base", [])), "html", null, true);
        echo "</td>
                </tr>

                <tr>
                    <td width=\"507\">Base prima:</td>
                    <td width=\"300\">\$ ";
        // line 218
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 218, $this->source); })()), "base", [])), "html", null, true);
        echo "</td>
                </tr>

                <tr>
                    <td width=\"507\">Base vacaciones::</td>
                    ";
        // line 223
        if ((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 223, $this->source); })()), "jornadaLaboral", []) == "TC")) {
            // line 224
            echo "                        <td width=\"300\">\$ ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 224, $this->source); })()), "base", []) / 30)), "html", null, true);
            echo "</td>
                    ";
        } else {
            // line 226
            echo "                        <td width=\"300\">\$ ";
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 226, $this->source); })()), "base", []) / (twig_length_filter($this->env, (isset($context["cantidad"]) || array_key_exists("cantidad", $context) ? $context["cantidad"] : (function () { throw new Twig_Error_Runtime('Variable "cantidad" does not exist.', 226, $this->source); })())) * 4))), "html", null, true);
            echo "</td>
                    ";
        }
        // line 228
        echo "                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td colspan=\"3\" height=\"15\"></td>
    </tr>

    <tr>
        <td colspan=\"3\" height=\"15\"></td>
    </tr>
    <tr>
        <td colspan=\"3\">
            <h3>CONCEPTOS LIQUIDACIÓN DEL CONTRATO DE TRABAJO</h3>
            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                <tr>
                    <th align=\"center\" width=\"813\" style=\"text-align: center;\" bgcolor=\"#ededed\">CONCEPTO</th>
                    <th align=\"center\" width=\"320\" style=\"text-align: center;\" bgcolor=\"#ededed\">DÍAS</th>
                    <th align=\"center\" width=\"440\" style=\"text-align: center;\" bgcolor=\"#ededed\">DEVENGADO</th>
                    <th align=\"center\" width=\"440\" style=\"text-align: center;\" bgcolor=\"#ededed\">DEDUCIDO</th>
                </tr>

                ";
        // line 251
        if ((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 251, $this->source); })()), "valorPrimeraPrima", []) > 0)) {
            // line 252
            echo "                    <tr class=\"sub\">
                        <td colspan=\"1\">Prima mitad de año</td>
                        <td>";
            // line 254
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 254, $this->source); })()), "daysPrimerSemestre", []), "html", null, true);
            echo "</td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ ";
            // line 255
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 255, $this->source); })()), "valorPrimeraPrima", [])), "html", null, true);
            echo "</strong></td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                    </tr>
                ";
        }
        // line 259
        echo "
                ";
        // line 260
        if ((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 260, $this->source); })()), "valorSegundaPrima", []) > 0)) {
            // line 261
            echo "                    <tr class=\"sub\">
                        <td colspan=\"1\">Prima fin de año</td>
                        <td>";
            // line 263
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 263, $this->source); })()), "daysSegundoSemestre", []), "html", null, true);
            echo "</td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ ";
            // line 264
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 264, $this->source); })()), "valorSegundaPrima", [])), "html", null, true);
            echo "</strong></td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                    </tr>
                ";
        }
        // line 268
        echo "                ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 268, $this->source); })()), "valorCesantiasAnterior", []) > 0)) {
            // line 269
            echo "
                    <tr class=\"sub\">
                        <td colspan=\"1\">Cesantias año ";
            // line 271
            echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 271, $this->source); })()), "dateLiquidation", []), "Y") - 1), "html", null, true);
            echo "</td>
                        <td>";
            // line 272
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 272, $this->source); })()), "daysCesantiasAnterior", []), "html", null, true);
            echo "</td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ ";
            // line 273
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 273, $this->source); })()), "valorCesantiasAnterior", [])), "html", null, true);
            echo "</strong>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                    </tr>
                ";
        }
        // line 277
        echo "
                ";
        // line 278
        if ((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 278, $this->source); })()), "valorIntCesantiasAnterior", []) > 0)) {
            // line 279
            echo "                    <tr class=\"sub\">
                        <td colspan=\"1\">Intereses cesantias año ";
            // line 280
            echo twig_escape_filter($this->env, (twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 280, $this->source); })()), "dateLiquidation", []), "Y") - 1), "html", null, true);
            echo "</td>
                        <td>";
            // line 281
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 281, $this->source); })()), "daysCesantiasAnterior", []), "html", null, true);
            echo "</td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ ";
            // line 282
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 282, $this->source); })()), "valorIntCesantiasAnterior", [])), "html", null, true);
            echo "</strong></td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                    </tr>
                ";
        }
        // line 286
        echo "
                ";
        // line 287
        if ((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 287, $this->source); })()), "valorCesantiasActual", []) > 0)) {
            // line 288
            echo "                    <tr class=\"sub\">
                        <td colspan=\"1\">Cesantias año ";
            // line 289
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 289, $this->source); })()), "dateLiquidation", []), "Y"), "html", null, true);
            echo "</td>
                        <td>";
            // line 290
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 290, $this->source); })()), "daysCesantiasActual", []), "html", null, true);
            echo "</td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ ";
            // line 291
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 291, $this->source); })()), "valorCesantiasActual", [])), "html", null, true);
            echo "</strong>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                    </tr>
                ";
        }
        // line 295
        echo "
                ";
        // line 296
        if ((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 296, $this->source); })()), "valorIntCesantiasActual", []) > 0)) {
            // line 297
            echo "                    <tr class=\"sub\">
                        <td colspan=\"1\">Intereses cesantias año ";
            // line 298
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 298, $this->source); })()), "dateLiquidation", []), "Y"), "html", null, true);
            echo "</td>
                        <td>";
            // line 299
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 299, $this->source); })()), "daysCesantiasActual", []), "html", null, true);
            echo "</td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ ";
            // line 300
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 300, $this->source); })()), "valorIntCesantiasActual", [])), "html", null, true);
            echo "</strong></td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                    </tr>
                ";
        }
        // line 304
        echo "
                ";
        // line 305
        $context["vacacionesA"] = 0;
        // line 306
        echo "                ";
        $context["vacacionesP"] = 0;
        // line 307
        echo "                ";
        if ((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 307, $this->source); })()), "valorVacaciones", []) > 0)) {
            // line 308
            echo "                        ";
            $context["vacacionesA"] = twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 308, $this->source); })()), "valorVacaciones", []);
            // line 309
            echo "                        <tr class=\"sub\">
                            <td colspan=\"1\">Vacaciones</td>
                            <td>";
            // line 311
            echo twig_escape_filter($this->env, twig_round((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 311, $this->source); })()), "diasVacaciones", []) - twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 311, $this->source); })()), "vacaciones", [])), 1), "html", null, true);
            echo "</td>
                            <td align=\"center\" style=\"border-right:none;\"><strong>\$ ";
            // line 312
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["vacacionesA"]) || array_key_exists("vacacionesA", $context) ? $context["vacacionesA"] : (function () { throw new Twig_Error_Runtime('Variable "vacacionesA" does not exist.', 312, $this->source); })())), "html", null, true);
            echo "</strong></td>
                            <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                        </tr>
                ";
        } else {
            // line 316
            echo "                        ";
            $context["vacacionesP"] = twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 316, $this->source); })()), "valorVacaciones", []);
            // line 317
            echo "                        <tr class=\"sub\">
                            <td colspan=\"1\">Vacaciones</td>
                            <td>";
            // line 319
            echo twig_escape_filter($this->env, twig_round((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 319, $this->source); })()), "diasVacaciones", []) - twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 319, $this->source); })()), "vacaciones", [])), 1), "html", null, true);
            echo "</td>
                            <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                            <td align=\"center\" style=\"border-right:none;\"><strong>\$ ";
            // line 321
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["vacacionesP"]) || array_key_exists("vacacionesP", $context) ? $context["vacacionesP"] : (function () { throw new Twig_Error_Runtime('Variable "vacacionesP" does not exist.', 321, $this->source); })())), "html", null, true);
            echo "</strong></td>
                        </tr>

                ";
        }
        // line 325
        echo "
                ";
        // line 326
        $context["devengado"] = (((((((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 326, $this->source); })()), "valorPrimeraPrima", []) + twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 326, $this->source); })()), "valorSegundaPrima", [])) + twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 326, $this->source); })()), "valorCesantiasAnterior", [])) + twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 326, $this->source); })()), "valorIntCesantiasAnterior", [])) + twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 326, $this->source); })()), "valorCesantiasActual", [])) + twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 326, $this->source); })()), "valorIntCesantiasActual", [])) + (isset($context["vacacionesA"]) || array_key_exists("vacacionesA", $context) ? $context["vacacionesA"] : (function () { throw new Twig_Error_Runtime('Variable "vacacionesA" does not exist.', 326, $this->source); })())) + twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 326, $this->source); })()), "valorIndemnizacion", []));
        // line 327
        echo "                ";
        $context["deducido"] = (isset($context["vacacionesP"]) || array_key_exists("vacacionesP", $context) ? $context["vacacionesP"] : (function () { throw new Twig_Error_Runtime('Variable "vacacionesP" does not exist.', 327, $this->source); })());
        // line 328
        echo "
                ";
        // line 330
        echo "
                ";
        // line 331
        if ((twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 331, $this->source); })()), "valorIndemnizacion", []) > 0)) {
            // line 332
            echo "                    <tr class=\"sub\">
                        <td colspan=\"1\">Indemnización</td>
                        <td>";
            // line 334
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 334, $this->source); })()), "daysIndemnizacion", []), "html", null, true);
            echo "</td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ ";
            // line 335
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["reg"]) || array_key_exists("reg", $context) ? $context["reg"] : (function () { throw new Twig_Error_Runtime('Variable "reg" does not exist.', 335, $this->source); })()), "valorIndemnizacion", [])), "html", null, true);
            echo "</strong></td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                    </tr>
                ";
        }
        // line 339
        echo "
                <tr class=\"total\">
                    <td style=\"border-left:none;\" bgcolor=\"#bebebe\" colspan=\"2\">Subtotal</td>
                    <td style=\"border-left:none;\" bgcolor=\"#bebebe\">
                        \$ ";
        // line 343
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["devengado"]) || array_key_exists("devengado", $context) ? $context["devengado"] : (function () { throw new Twig_Error_Runtime('Variable "devengado" does not exist.', 343, $this->source); })())), "html", null, true);
        echo "
                    </td>
                    <td style=\"border-left:none;\" bgcolor=\"#bebebe\">\$ ";
        // line 345
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["deducido"]) || array_key_exists("deducido", $context) ? $context["deducido"] : (function () { throw new Twig_Error_Runtime('Variable "deducido" does not exist.', 345, $this->source); })())), "html", null, true);
        echo "</td>
                </tr>

                <tr class=\"total\">
                    <td style=\"border-left:none;\" bgcolor=\"#bebebe\" colspan=\"2\">Total</td>
                    <td style=\"border-left:none;\" bgcolor=\"#bebebe\" colspan=\"2\">\$ ";
        // line 350
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ((isset($context["devengado"]) || array_key_exists("devengado", $context) ? $context["devengado"] : (function () { throw new Twig_Error_Runtime('Variable "devengado" does not exist.', 350, $this->source); })()) + (isset($context["deducido"]) || array_key_exists("deducido", $context) ? $context["deducido"] : (function () { throw new Twig_Error_Runtime('Variable "deducido" does not exist.', 350, $this->source); })()))), "html", null, true);
        echo "</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan=\"3\" height=\"15\"></td>
    </tr>
    <tr>
        <td colspan=\"3\">
            <p>Constancia: al aceptar y recibir la presente liquidación, hago constar que queda a paz y salvo por concepto de prestaciones sociales, ya que todos los derechos me han sido reconocido oportunamente de acuerdo con las disposiciones legales y en consecuencia firmo.</p>
        </td>
    </tr>
    <tr>
        <td colspan=\"3\" height=\"80\"></td>
    </tr>
    <tr>
        <td width=\"960\">
            <hr>
            <p><strong>Firma Empleador(a)</strong></p>
        </td>
        <td width=\"90\"></td>
        <td width=\"960\">
            <hr>
            <p><strong>Firma Empleado(a)</strong></p>
        </td>
    </tr>
    <tr>
        <td colspan=\"3\" height=\"80\"></td>
    </tr>
    <tr>
        <td colspan=\"3\">
            <p style=\"font-size:9pt;\">Los cálculos aquí enviados y realizados mediante el servicio de simulación de liquidación de
                Calculadora de liquidación, no exime a el empleador de obligaciones u omisiones de la relación laboral con respecto al
                pasado.</p>
            <p style=\"font-size:9pt;\">Los cálculos entregados son basados en la información proporcionada por el
                empleador y Calculadora de liquidación no tiene responsabilidad sobre estos, tampoco actúa en ningún momento como
                responsable de la relación laboral pactada ni anterior ni posterior.</p>
            <p style=\"font-size:9pt;\">El empleador es el único responsable de terminar correctamente la relación
                laboral, mediante el pago de la liquidación e indemnizaciones (si dan a lugar) así como de brindar copia
                de los soportes a su empleado.</p>
        </td>
    </tr>
</table>
</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "documents/liquidacion.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  602 => 350,  594 => 345,  589 => 343,  583 => 339,  576 => 335,  572 => 334,  568 => 332,  566 => 331,  563 => 330,  560 => 328,  557 => 327,  555 => 326,  552 => 325,  545 => 321,  540 => 319,  536 => 317,  533 => 316,  526 => 312,  522 => 311,  518 => 309,  515 => 308,  512 => 307,  509 => 306,  507 => 305,  504 => 304,  497 => 300,  493 => 299,  489 => 298,  486 => 297,  484 => 296,  481 => 295,  474 => 291,  470 => 290,  466 => 289,  463 => 288,  461 => 287,  458 => 286,  451 => 282,  447 => 281,  443 => 280,  440 => 279,  438 => 278,  435 => 277,  428 => 273,  424 => 272,  420 => 271,  416 => 269,  413 => 268,  406 => 264,  402 => 263,  398 => 261,  396 => 260,  393 => 259,  386 => 255,  382 => 254,  378 => 252,  376 => 251,  351 => 228,  345 => 226,  339 => 224,  337 => 223,  329 => 218,  321 => 213,  305 => 200,  298 => 196,  291 => 192,  272 => 176,  268 => 174,  262 => 171,  258 => 169,  256 => 168,  251 => 166,  246 => 163,  240 => 161,  234 => 159,  232 => 158,  227 => 155,  221 => 153,  217 => 151,  215 => 150,  210 => 147,  204 => 144,  201 => 143,  197 => 141,  195 => 140,  174 => 122,  168 => 119,  160 => 116,  154 => 113,  138 => 102,  132 => 99,  114 => 83,  112 => 82,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
    <meta charset=\"UTF-8\">
    <title>Simulación de liquidación</title>
    <style>
        * {
            margin: 0;
            padding: 0;
        }

        body, h1, h2, h3, h4, h5, p, a {
            font-family: 'Arial', sans-serif;
            color: #444;
        }

        h2 {
            font-size: 18px;
            text-align: center;
        }

        h3 {
            background: #e4e4e4;
            border: 2px solid #bebebe;
            border-bottom: none;
            width: 100%;
            height: 20px;
            line-height: 20px;
            text-align: center;
            font-size: 15px;
        }

        p, li {
            line-height: 1.1;
            font-size: 12px;
        }

        th {
            font-size: 13px;
            text-align: left;
            background: #ededed;
            border: 1px solid #bebebe;
            height: 15px;
            padding: 2px 5px;
        }

        td {
            font-size: 13px;
            min-height: 45px;

        }

        table tr td table {
            border: 1px solid #bebebe;
        }

        table tr td table tr td {
            border: 1px solid #bebebe;
            height: 10px;
            padding: 2px 5px;
        }

        table td table tr.no-border td {
            border: none !important;
        }

        table td table tr.sub td {
            background: #ededed;
            font-weight: bold;
        }

        table td table tr.total td {
            background: #bebebe;
            font-weight: bold;
            font-size: 15px;
            border-color: #7c7c7c;
        }
    </style>
</head>
<body>
<table width=\"90%\" style=\"max-width: 2000px; margin: 0 auto;\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\">
    {% set cantidad = reg.diasSemana|split(',') %}
    <tr>
        <td colspan=\"3\" height=\"15\"></td>
    </tr>
    <tr>
        <td colspan=\"3\"><h2>CÁLCULOS DE LIQUIDACIÓN FINAL DE CONTRATO DE TRABAJO</h2></td>
    </tr>
    <tr>
        <td colspan=\"3\" height=\"15\"></td>
    </tr>
    <tr>
        <td width=\"960\" valign=\"top\">
            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                <tr>
                    <th bgcolor=\"#ededed\">EMPLEADOR</th>
                </tr>
                <tr>
                    <td>{{ reg.nameEmployer }}</td>
                </tr>
                <tr>
                    <td>{{ reg.typeDocEmployer }} {{ reg.numberEmployer }}</td>
                </tr>
            </table>
        </td>
        <td width=\"90\"></td>
        <td width=\"960\" valign=\"top\">
            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                <tr>
                    <th bgcolor=\"#ededed\">EMPLEADO</th>
                </tr>
                <tr>
                    <td>{{ reg.nameEmployee }}</td>
                </tr>
                <tr>
                    <td>{{ reg.typeDocEmployee }} {{ reg.numberDocEmployee }}</td>
                </tr>
                <tr>
                    <td>Cargo: {{ reg.cargo }}</td>
                </tr>
                <tr>
                    <td>Causa de la liquidación: {{ reg.termContract }}</td>
                </tr>
            </table>
        </td>
    </tr>

    <tr>
        <td colspan=\"3\" height=\"15\"></td>
    </tr>

    <tr>
        <td width=\"960\" valign=\"top\" colspan=\"3\">
            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                <tr>
                    <th colspan=\"2\" bgcolor=\"#ededed\">CONTRATO</th>
                </tr>
                <tr>
                    <td width=\"507\">Días laborados por semana:</td>
                    {% if reg.jornadaLaboral == \"TC\" %}
                        <td width=\"300\">7</td>
                    {% else %}
                        <td width=\"300\">
                            {{ cantidad|length }}
                        </td>
                    {% endif %}
                </tr>
                <tr>
                    <td width=\"507\">Días laborados por mes:</td>
                    {% if reg.jornadaLaboral == \"TC\" %}
                        <td width=\"300\">30</td>
                    {% else %}
                        <td width=\"300\">{{ cantidad|length * 4}}</td>
                    {% endif %}
                </tr>
                <tr>
                    <td width=\"507\">Salario:</td>
                    {% if reg.jornadaLaboral == \"TC\" %}
                        <td width=\"300\">\$ {{ reg.salary|number_format }}</td>
                    {% else %}
                            <td width=\"300\">\$ {{ (reg.salary * cantidad|length * 4)|number_format }}</td>
                    {% endif %}
                </tr>
                <tr>
                    <td width=\"507\">Fecha de inicio del contrato:</td>
                    <td width=\"300\">{{ reg.dateInit|date(\"d/m/Y\") }}</td>
                </tr>
                {% if reg.typeContract == \"TC\" %}
                    <tr>
                        <td width=\"507\">Fecha final del contrato:</td>
                        <td width=\"300\">{{ reg.dateEnd|date(\"d/m/Y\") }}</td>
                    </tr>
                {% endif %}
                <tr>
                    <td width=\"507\">Fecha liquidación del contrato:</td>
                    <td width=\"300\">{{ reg.dateLiquidation|date(\"d/m/Y\") }}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan=\"3\" height=\"15\"></td>
    </tr>
    <tr>
        <td width=\"960\">
            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                <tr>
                    <th colspan=\"2\" bgcolor=\"#ededed\">VACACIONES</th>
                </tr>
                <tr>
                    <td width=\"507\">Días causados:</td>
                    <td width=\"300\">{{ reg.diasVacaciones|round(1) }}</td>
                </tr>
                <tr>
                    <td width=\"507\">Días tomados:</td>
                    <td width=\"300\">{{ reg.vacaciones|round(1) }}</td>
                </tr>
                <tr>
                    <td width=\"507\">Días pendientes:</td>
                    <td width=\"300\">{{ (reg.diasVacaciones - reg.vacaciones)|round(1) }}</td>
                </tr>
            </table>
        </td>
        <td width=\"90\"></td>
        <td width=\"960\">
            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                <tr>
                    <th colspan=\"2\" bgcolor=\"#ededed\">BASES DE LIQUIDACIÓN</th>
                </tr>

                <tr>
                    <td width=\"507\">Base cesantías:</td>
                    <td width=\"300\">\$ {{ reg.base|number_format }}</td>
                </tr>

                <tr>
                    <td width=\"507\">Base prima:</td>
                    <td width=\"300\">\$ {{ reg.base|number_format }}</td>
                </tr>

                <tr>
                    <td width=\"507\">Base vacaciones::</td>
                    {% if reg.jornadaLaboral == \"TC\" %}
                        <td width=\"300\">\$ {{ (reg.base/30)|number_format }}</td>
                    {% else %}
                        <td width=\"300\">\$ {{ (reg.base  / (cantidad|length * 4) )|number_format }}</td>
                    {% endif %}
                </tr>

            </table>
        </td>
    </tr>
    <tr>
        <td colspan=\"3\" height=\"15\"></td>
    </tr>

    <tr>
        <td colspan=\"3\" height=\"15\"></td>
    </tr>
    <tr>
        <td colspan=\"3\">
            <h3>CONCEPTOS LIQUIDACIÓN DEL CONTRATO DE TRABAJO</h3>
            <table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">
                <tr>
                    <th align=\"center\" width=\"813\" style=\"text-align: center;\" bgcolor=\"#ededed\">CONCEPTO</th>
                    <th align=\"center\" width=\"320\" style=\"text-align: center;\" bgcolor=\"#ededed\">DÍAS</th>
                    <th align=\"center\" width=\"440\" style=\"text-align: center;\" bgcolor=\"#ededed\">DEVENGADO</th>
                    <th align=\"center\" width=\"440\" style=\"text-align: center;\" bgcolor=\"#ededed\">DEDUCIDO</th>
                </tr>

                {% if reg.valorPrimeraPrima > 0 %}
                    <tr class=\"sub\">
                        <td colspan=\"1\">Prima mitad de año</td>
                        <td>{{ reg.daysPrimerSemestre }}</td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ {{ reg.valorPrimeraPrima |number_format }}</strong></td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                    </tr>
                {% endif %}

                {% if reg.valorSegundaPrima > 0 %}
                    <tr class=\"sub\">
                        <td colspan=\"1\">Prima fin de año</td>
                        <td>{{ reg.daysSegundoSemestre }}</td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ {{ reg.valorSegundaPrima |number_format }}</strong></td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                    </tr>
                {% endif %}
                {% if reg.valorCesantiasAnterior > 0 %}

                    <tr class=\"sub\">
                        <td colspan=\"1\">Cesantias año {{ reg.dateLiquidation|date(\"Y\")-1 }}</td>
                        <td>{{ reg.daysCesantiasAnterior }}</td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ {{ reg.valorCesantiasAnterior |number_format }}</strong>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                    </tr>
                {% endif %}

                {% if reg.valorIntCesantiasAnterior > 0 %}
                    <tr class=\"sub\">
                        <td colspan=\"1\">Intereses cesantias año {{ reg.dateLiquidation|date(\"Y\")-1 }}</td>
                        <td>{{ reg.daysCesantiasAnterior }}</td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ {{ reg.valorIntCesantiasAnterior |number_format }}</strong></td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                    </tr>
                {% endif %}

                {% if reg.valorCesantiasActual > 0 %}
                    <tr class=\"sub\">
                        <td colspan=\"1\">Cesantias año {{ reg.dateLiquidation|date(\"Y\") }}</td>
                        <td>{{ reg.daysCesantiasActual }}</td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ {{ reg.valorCesantiasActual |number_format }}</strong>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                    </tr>
                {% endif %}

                {% if reg.valorIntCesantiasActual > 0 %}
                    <tr class=\"sub\">
                        <td colspan=\"1\">Intereses cesantias año {{ reg.dateLiquidation|date(\"Y\") }}</td>
                        <td>{{ reg.daysCesantiasActual }}</td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ {{ reg.valorIntCesantiasActual |number_format }}</strong></td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                    </tr>
                {% endif %}

                {% set vacacionesA = 0 %}
                {% set vacacionesP =  0 %}
                {% if reg.valorVacaciones > 0 %}
                        {% set vacacionesA =  reg.valorVacaciones %}
                        <tr class=\"sub\">
                            <td colspan=\"1\">Vacaciones</td>
                            <td>{{ (reg.diasVacaciones - reg.vacaciones)|round(1)  }}</td>
                            <td align=\"center\" style=\"border-right:none;\"><strong>\$ {{ vacacionesA|number_format }}</strong></td>
                            <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                        </tr>
                {% else %}
                        {% set vacacionesP =  reg.valorVacaciones %}
                        <tr class=\"sub\">
                            <td colspan=\"1\">Vacaciones</td>
                            <td>{{ (reg.diasVacaciones - reg.vacaciones)|round(1) }}</td>
                            <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                            <td align=\"center\" style=\"border-right:none;\"><strong>\$ {{ vacacionesP|number_format }}</strong></td>
                        </tr>

                {% endif %}

                {% set devengado = reg.valorPrimeraPrima + reg.valorSegundaPrima + reg.valorCesantiasAnterior + reg.valorIntCesantiasAnterior + reg.valorCesantiasActual + reg.valorIntCesantiasActual + vacacionesA + reg.valorIndemnizacion %}
                {% set deducido = vacacionesP %}

                {# set indemnizacion = reg.valorIndemnizacion #}

                {% if reg.valorIndemnizacion > 0 %}
                    <tr class=\"sub\">
                        <td colspan=\"1\">Indemnización</td>
                        <td>{{ reg.daysIndemnizacion }}</td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ {{  reg.valorIndemnizacion|number_format }}</strong></td>
                        <td align=\"center\" style=\"border-right:none;\"><strong>\$ 0</strong></td>
                    </tr>
                {% endif %}

                <tr class=\"total\">
                    <td style=\"border-left:none;\" bgcolor=\"#bebebe\" colspan=\"2\">Subtotal</td>
                    <td style=\"border-left:none;\" bgcolor=\"#bebebe\">
                        \$ {{ (devengado)|number_format }}
                    </td>
                    <td style=\"border-left:none;\" bgcolor=\"#bebebe\">\$ {{ deducido|number_format }}</td>
                </tr>

                <tr class=\"total\">
                    <td style=\"border-left:none;\" bgcolor=\"#bebebe\" colspan=\"2\">Total</td>
                    <td style=\"border-left:none;\" bgcolor=\"#bebebe\" colspan=\"2\">\$ {{ (devengado + deducido)|number_format }}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan=\"3\" height=\"15\"></td>
    </tr>
    <tr>
        <td colspan=\"3\">
            <p>Constancia: al aceptar y recibir la presente liquidación, hago constar que queda a paz y salvo por concepto de prestaciones sociales, ya que todos los derechos me han sido reconocido oportunamente de acuerdo con las disposiciones legales y en consecuencia firmo.</p>
        </td>
    </tr>
    <tr>
        <td colspan=\"3\" height=\"80\"></td>
    </tr>
    <tr>
        <td width=\"960\">
            <hr>
            <p><strong>Firma Empleador(a)</strong></p>
        </td>
        <td width=\"90\"></td>
        <td width=\"960\">
            <hr>
            <p><strong>Firma Empleado(a)</strong></p>
        </td>
    </tr>
    <tr>
        <td colspan=\"3\" height=\"80\"></td>
    </tr>
    <tr>
        <td colspan=\"3\">
            <p style=\"font-size:9pt;\">Los cálculos aquí enviados y realizados mediante el servicio de simulación de liquidación de
                Calculadora de liquidación, no exime a el empleador de obligaciones u omisiones de la relación laboral con respecto al
                pasado.</p>
            <p style=\"font-size:9pt;\">Los cálculos entregados son basados en la información proporcionada por el
                empleador y Calculadora de liquidación no tiene responsabilidad sobre estos, tampoco actúa en ningún momento como
                responsable de la relación laboral pactada ni anterior ni posterior.</p>
            <p style=\"font-size:9pt;\">El empleador es el único responsable de terminar correctamente la relación
                laboral, mediante el pago de la liquidación e indemnizaciones (si dan a lugar) así como de brindar copia
                de los soportes a su empleado.</p>
        </td>
    </tr>
</table>
</body>
</html>", "documents/liquidacion.html.twig", "C:\\xampp\\htdocs\\PHP\\liquidacionesclienteexterno\\app\\Resources\\views\\documents\\liquidacion.html.twig");
    }
}
