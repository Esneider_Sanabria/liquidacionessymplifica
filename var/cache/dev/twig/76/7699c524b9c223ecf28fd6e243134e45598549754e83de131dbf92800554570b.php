<?php

/* dashboard/previa.html.twig */
class __TwigTemplate_973c0f762b478ca1f76c3eef3c2b0050c66809a283f333e7dcb419d2a4483828 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "dashboard/previa.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "dashboard/previa.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "dashboard/previa.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Calcula cuanto debes pagar al liquidar un empleado";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<section id=\"pre-form\">
\t<div class=\"row\">
\t\t<h2>Antes de comenzar ten en cuenta</h2>
\t\t<p class=\"text-center\">Esta liquidación consta de 3 pasos:</p>
\t\t<div class=\"col-sm-12 pasos\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-sm-4 col-xs-12 paso\">
\t\t\t\t\t<span>1</span>
\t\t\t\t\t<p>Tú como empleador suministras todos los datos que conciernen a la relación laboral que está por terminar.</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-4 col-xs-12 paso\">
\t\t\t\t\t<span>2</span>
\t\t\t\t\t<p>Te redirigiremos a la plataforma de PayU, para que pagues la liquidación.</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-4 col-xs-12 paso\">
\t\t\t\t\t<span>3</span>
\t\t\t\t\t<p>Con los datos que nos suministraste, calculamos lo que el empleado debe recibir como liquidación y enviamos toda la documentación a tu correo.</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-lg-9 col-md-11 col-xs-12 importante\">
\t\t\t<h3>Importante:</h3>
\t\t\t<p>El resultado de la simulación es aproximado y de carácter informativo. Todos los montos son calculados con base en la información y parámetros ingresados por el usuario y con base en los criterios para el cálculo determinados en las leyes y reglamentos aplicables y vigentes para la fecha del cálculo. El resultado tiene limitaciones y puede no producir resultados válidos para todas las posibles combinaciones de datos de entrada por razones, entre otras, que corresponden a la realidad específica de una relación laboral o de servicios. Errores reales y potenciales podrían no ser detectados. Ni el sitio ni su gestor o administrador asumen responsabilidad alguna de revisar, probar o detectar errores. Ninguna simulación será proporcionada o tratada como un asesoramiento y en tal sentido, no podrá ser utilizada para la toma de decisiones o como elemento probatorio en acciones judiciales o extrajudiciales. El gestor y administrador de este simulador, no realizará devoluciones de pagos realizados por en uso del Simulador salvo cuando se demuestre que la imposibilidad de uso del mismo por razones distintas a su conformidad o inconformidad con los resultados arrojados por la herramienta. No se otorga ninguna representación, garantía o compromiso (expreso o implícito) de ningún tipo sobre la exactitud, idoneidad o suficiencia para las necesidades propias del usuario. Conozca a continuación los términos y condiciones de uso de este simulador.</p>
\t\t</div>
\t\t<div class=\"col-sm-12\" id=\"terminos\">
\t\t\t<h3>Términos y condiciones</h3>
\t\t\t<div class=\"terminos row\">
\t\t\t\t<h4>CONDICIONES DE USO DEL SIMULADOR CALCULADORA DE LIQUIDACIÓN</h4>
\t\t\t\t<p>Las presentes son las condiciones de uso del servicio de simulación para la estimación del valor aplicable a la liquidación de acreencias laborales que resulta de obligatorio pago en las relaciones de servicios en la República de Colombia conforme a la ley y reglamentos vigentes (las \"Condiciones de Uso\") y que son de aplicación a todos los cálculos que realicen personas (un \"Usuario\") en uso de una herramienta informática publicada en el sitio web www.afiliese.com (el \"Simulador\"), la cual es gestionada y administrada por Symplifica S.A.S. (\"Symplifica\"). En esa medida, estas Condiciones de Uso tienen carácter adicional a las condiciones específicas detalladas en el sitio web de Symplifica en lo que respecta a la administración de páginas web, al tratamiento de datos personales y al alcance de su responsabilidad en esa calidad, aplicables a cualquier cálculo realizado por un Usuario en uso del Simulador.</p>
\t\t\t\t<ol>
\t\t\t\t\t<li>
\t\t\t\t\t\t<strong>En uso del Simulador el Usuario reconoce que:</strong>
\t\t\t\t\t\t<ol>
\t\t\t\t\t\t\t<li>el Simulador ha sido diseñado con el único propósito de facilitar a los Usuarios la estimación de los valores aplicables al cálculo de la liquidación de acreencias laborales porla finalización de las relaciones de servicios en Colombia;</li>
\t\t\t\t\t\t\t<li>el Simulador no es un liquidador y, en tal sentido, no suple ni sustituye el análisis específico del caso concreto que corresponda a la situación legal y fáctica del Usuario, sea que éste se trate del beneficiario del pago de la liquidación o se goce de la calidad de empleador;</li>
\t\t\t\t\t\t\t<li>sin perjuicio de la veracidad o exactitud de la información y parámetros ingresados por el Usuario en el Simulador, el Simulador tiene limitaciones y puede no producir resultados válidos para todas las posibles combinaciones de datos de entrada por razones, entre otras, que corresponden a la realidad específica de una relación laboral o de servicios o de la omisión en información o parámetros necesarios para mitigar la probabilidad u ocurrencia de errores;</li>
\t\t\t\t\t\t\t<li>ni el sitio ni Symplifica asumen responsabilidad alguna de revisar, probar o detectar errores; y</li>
\t\t\t\t\t\t\t<li>ninguna simulación del Simulador será proporcionada o tratada como un asesoramiento y en tal sentido, no podrá ser utilizada para la toma de decisiones o como elemento probatorio en acciones judiciales o extrajudiciales.</li>
\t\t\t\t\t\t</ol>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<strong>En uso del Simulador el Usuario acepta que:</strong>
\t\t\t\t\t\t<ol>
\t\t\t\t\t\t\t<li>el uso del Simulador tiene un costo publicado en el sitio web donde se encuentra el Simulador y que este valor remunera el diseño, puesta a punto y mantenimiento del Simulador. En ese sentido, en ningún caso el valor pagado por el Usuario representa un pago por la autenticidad, veracidad o precisión de los resultados del Simulador;</li>
\t\t\t\t\t\t\t<li>todos los montos son calculados con base en la información y parámetros ingresados por el Usuario y con base en los criterios para el cálculo determinados en las leyes y reglamentos aplicables y vigentes para la fecha del cálculo;</li>
\t\t\t\t\t\t\t<li>el uso del Simulador es para uso exclusivo del Usuario y lo empleará para uso personal. En tal medida, el Usuario no empleará los resultados del Simulador para fines contractuales, legales o judiciales;</li>
\t\t\t\t\t\t\t<li>no usará los resultados del Simulador para suplantar o reemplazar a cualquier persona incluyendo, sin limitación, a su empleador, si fuera el caso;</li>
\t\t\t\t\t\t\t<li>no empleará los resultados del Simulador para fines o actividades inmorales o ilícitas incluyendo, sin limitación, extorsión, chantaje o fraude;</li>
\t\t\t\t\t\t\t<li>el uso del Simulador está protegido por las reglas aplicables en materia de Propiedad Industrial y en esa medida, se prohíbe la modificación, reproducción, publicación o transferencia de cualquier contenido a otras personas, o su uso para cualquier fin;</li>
\t\t\t\t\t\t\t<li>le está prohibido desensamblar, descompilar, aplicar ingeniería inversa o intentar por cualquier medio romper la protección del contenido del Simulador;</li>
\t\t\t\t\t\t\t<li>el uso del Simulador es exclusivo en el sitio donde se encuentra publicado, por lo que no se permite la creación de páginas web, sitios de Internet, documentos electrónicos o programas de computador o aplicaciones informáticas de cualquier tipo que contengan hipervínculos o marcas que redirijan al navegante al Simulador; y</li>
\t\t\t\t\t\t\t<li>Symplifica, como gestor y administrador del Simulador, no realizará devoluciones de pagos realizados por el Usuario en uso del Simulador salvo cuando el Usuario demuestre que no le fue posible el uso del Simulador por razones distintas a su conformidad o inconformidad con los resultados arrojados por el Simulador.</li>
\t\t\t\t\t\t</ol>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<strong>En uso del Simulador el Usuario sabe, entiende y acepta que:</strong>
\t\t\t\t\t\t<ol>
\t\t\t\t\t\t\t<li>relaciones laborales que tengan una duración máxima de dieciocho (18) meses;</li>
\t\t\t\t\t\t\t<li>la afiliación del beneficiario del pago al sistema de Seguridad Social en Colombia;</li>
\t\t\t\t\t\t\t<li>el beneficiario del pago no se encuentra, al momento de la simulación, disfrutando de vacaciones o con novedades laborales activas;</li>
\t\t\t\t\t\t\t<li>el beneficiario no goza de fueros legales o de beneficios o restricciones que impidan la desvinculación laboral del beneficiario;</li>
\t\t\t\t\t\t\t<li>la relación laboral presupone un periodo de prueba contractual en el máximo periodo permitido por la ley aplicable;</li>
\t\t\t\t\t\t\t<li>el valor del salario no es inferior, en ningún caso, al valor del salario mínimo mensual legal vigente para la fecha del cálculo;</li>
\t\t\t\t\t\t\t<li>el beneficiario del pago no se remunera contractualmente mediante salario integral (en los términos de la ley aplicable);</li>
\t\t\t\t\t\t\t<li>el empleador ha dado cumplimiento a la ley en sus aspectos materiales como, a título ilustrativo, los pagos durante la relación laboral y los procedimientos para la desvinculación del empleado; y</li>
\t\t\t\t\t\t\t<li>las prestaciones sociales y otros criterios como aportes parafiscales, se presumen por defecto en los criterios mínimos y estandarizados de la ley vigente al momento del cálculo.</li>
\t\t\t\t\t\t</ol>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<strong>Avisos</strong>
\t\t\t\t\t\tEl Usuario reconoce y acepta, adicionalmente, que:
\t\t\t\t\t\t<ol>
\t\t\t\t\t\t\t<li>La parametrización, los cálculos, resultados y uso del Simulador, quedan limitados exclusivamente a la jurisdicción colombiana;</li>
\t\t\t\t\t\t\t<li>el uso del Simulador está sujeto al pago del valor publicado;</li>
\t\t\t\t\t\t\t<li>Symplifica no será responsable en ningún caso de las consecuencias que puedan seguirse para el Usuario por el hecho de haber remitido éste información o documentación no veraz, inexacta o incompleta;</li>
\t\t\t\t\t\t\t<li>Symplifica se obliga a salvaguardar la confidencialidad de toda la información y documentación recibidas del Usuario que no sean de dominio público, y únicamente podrá desvelarla con autorización del Usuario o por orden de cualquier autoridad administrativa, judicial o legalmente autorizada para ello;</li>
\t\t\t\t\t\t\t<li>el Usuario acepta el correo electrónico no encriptado como medio hábil para el flujo e intercambio de documentación, información y, en general, como canal de comunicación. El Usuario exime a Symplifica, como administrador del Simulador, de cualquier responsabilidad por la interceptación o acceso a los correos electrónicos por personas no autorizadas, así como de cualquier daño o perjuicio que pueda producirse al Usuario como consecuencia de virus informáticos, fallos en la red o supuestos análogos, salvo que fuera por causa imputable a Symplifica;</li>
\t\t\t\t\t\t\t<li>Symplifica podrá estar sujeta a la obligación de comunicar a la Unidad de Información y Análisis Financiero (UIAF) cualquier hecho u operación respecto de los cuales exista indicio o certeza de que está relacionado con el lavado de activos o la financiación del terrorismo, debiendo abstenerse de ejecutar cualquier operación respecto de la que se pongan de manifiesto tales circunstancias. Symplifica no será responsable ante el Usuario de los daños y perjuicios que este pueda sufrir como consecuencia del cumplimiento, por parte de Symplifica, de dichas obligaciones legales;</li>
\t\t\t\t\t\t\t<li>en ningún caso Symplifica será responsable de los daños derivados, o causados, en todo o en parte, como consecuencia de la falsedad, el ocultamiento o cualquier otra conducta del Usuario que fuera dolosa o negligente, o no realizada conforme a los principios de la buena fe, o de incumplimientos que se produzcan por causas que están fuera de su control razonable;</li>
\t\t\t\t\t\t\t<li>la eventual responsabilidad de Symplifica tendrá lugar únicamente frente al Usuario. Symplifica no será responsable de los daños que puedan ocasionarse a terceros como consecuencia del uso que el Usuario pueda hacer del Simulador fuera del destino propio de los mismos;</li>
\t\t\t\t\t\t\t<li>el Usuario se obliga a no instigar a que un tercero demande a profesionales o empleados de Symplifica en relación con el Simulador;</li>
\t\t\t\t\t\t\t<li>Symplifica se reserva el derecho de modificar, cambiar o terminar estas Condiciones de Uso en cualquier momento y bajo su total discreción, sin necesidad de notificarlo previamente. Symplifica le advierte al Usuario que es su deber visitar esta página regularmente para tener conocimiento de los cambios;</li>
\t\t\t\t\t\t\t<li>los datos personales que facilite el Usuario a Symplifica mediante la aceptación de las presentes Condiciones de Uso, así como todos aquellos que facilite en el futuro como consecuencia de su vinculación con Symplifica (los \"Datos Personales\"), se incorporarán a una base de datos sobre la que Symplifica actuará como responsable y encargado y que tendrán el tratamiento previsto en su Política de Privacidad; y</li>
\t\t\t\t\t\t\t<li>la finalidad del tratamiento de los Datos Personales abarca las siguientes actividades: (i) mantenimiento, desarrollo, control y ejecución de la relación que, en el marco del uso del Simulador, mantenga con Symplifica; (ii) comunicaciones con el Usuario con fines de información o de mercadeo; y/o (iii) facturación por el uso del Simulador.</li>
\t\t\t\t\t\t</ol>
\t\t\t\t\t</li>
\t\t\t\t\t
\t\t\t\t</ol>
\t\t\t</div>
\t\t\t<div class=\"acepto\">
\t\t\t\t<input type=\"checkbox\" onchange=\"aceptar()\" id=\"terminos1\">
\t\t\t\t<label>Acepto los términos y condiciones de este servicio</label>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<a id=\"acepto\" class=\"disabled\">Continuar</a>
\t\t\t</div>
\t\t</div>
\t</div>
</section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 105
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 106
        echo "
<script type=\"text/javascript\">


        function aceptar(){

            \$('#acepto').addClass('disabled');
            \$('#acepto').removeAttr(\"href\");

            if(\$(\"#terminos1\").is(\":checked\") == true){

            \$('#acepto').removeClass('disabled');
            \$('#acepto').attr({
                href: \"";
        // line 119
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("form");
        echo "\"
            });
        }
\t}

</script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "dashboard/previa.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  206 => 119,  191 => 106,  182 => 105,  73 => 4,  64 => 3,  46 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block title %}Calcula cuanto debes pagar al liquidar un empleado{% endblock %}
{% block body %}
<section id=\"pre-form\">
\t<div class=\"row\">
\t\t<h2>Antes de comenzar ten en cuenta</h2>
\t\t<p class=\"text-center\">Esta liquidación consta de 3 pasos:</p>
\t\t<div class=\"col-sm-12 pasos\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-sm-4 col-xs-12 paso\">
\t\t\t\t\t<span>1</span>
\t\t\t\t\t<p>Tú como empleador suministras todos los datos que conciernen a la relación laboral que está por terminar.</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-4 col-xs-12 paso\">
\t\t\t\t\t<span>2</span>
\t\t\t\t\t<p>Te redirigiremos a la plataforma de PayU, para que pagues la liquidación.</p>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-sm-4 col-xs-12 paso\">
\t\t\t\t\t<span>3</span>
\t\t\t\t\t<p>Con los datos que nos suministraste, calculamos lo que el empleado debe recibir como liquidación y enviamos toda la documentación a tu correo.</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-lg-9 col-md-11 col-xs-12 importante\">
\t\t\t<h3>Importante:</h3>
\t\t\t<p>El resultado de la simulación es aproximado y de carácter informativo. Todos los montos son calculados con base en la información y parámetros ingresados por el usuario y con base en los criterios para el cálculo determinados en las leyes y reglamentos aplicables y vigentes para la fecha del cálculo. El resultado tiene limitaciones y puede no producir resultados válidos para todas las posibles combinaciones de datos de entrada por razones, entre otras, que corresponden a la realidad específica de una relación laboral o de servicios. Errores reales y potenciales podrían no ser detectados. Ni el sitio ni su gestor o administrador asumen responsabilidad alguna de revisar, probar o detectar errores. Ninguna simulación será proporcionada o tratada como un asesoramiento y en tal sentido, no podrá ser utilizada para la toma de decisiones o como elemento probatorio en acciones judiciales o extrajudiciales. El gestor y administrador de este simulador, no realizará devoluciones de pagos realizados por en uso del Simulador salvo cuando se demuestre que la imposibilidad de uso del mismo por razones distintas a su conformidad o inconformidad con los resultados arrojados por la herramienta. No se otorga ninguna representación, garantía o compromiso (expreso o implícito) de ningún tipo sobre la exactitud, idoneidad o suficiencia para las necesidades propias del usuario. Conozca a continuación los términos y condiciones de uso de este simulador.</p>
\t\t</div>
\t\t<div class=\"col-sm-12\" id=\"terminos\">
\t\t\t<h3>Términos y condiciones</h3>
\t\t\t<div class=\"terminos row\">
\t\t\t\t<h4>CONDICIONES DE USO DEL SIMULADOR CALCULADORA DE LIQUIDACIÓN</h4>
\t\t\t\t<p>Las presentes son las condiciones de uso del servicio de simulación para la estimación del valor aplicable a la liquidación de acreencias laborales que resulta de obligatorio pago en las relaciones de servicios en la República de Colombia conforme a la ley y reglamentos vigentes (las \"Condiciones de Uso\") y que son de aplicación a todos los cálculos que realicen personas (un \"Usuario\") en uso de una herramienta informática publicada en el sitio web www.afiliese.com (el \"Simulador\"), la cual es gestionada y administrada por Symplifica S.A.S. (\"Symplifica\"). En esa medida, estas Condiciones de Uso tienen carácter adicional a las condiciones específicas detalladas en el sitio web de Symplifica en lo que respecta a la administración de páginas web, al tratamiento de datos personales y al alcance de su responsabilidad en esa calidad, aplicables a cualquier cálculo realizado por un Usuario en uso del Simulador.</p>
\t\t\t\t<ol>
\t\t\t\t\t<li>
\t\t\t\t\t\t<strong>En uso del Simulador el Usuario reconoce que:</strong>
\t\t\t\t\t\t<ol>
\t\t\t\t\t\t\t<li>el Simulador ha sido diseñado con el único propósito de facilitar a los Usuarios la estimación de los valores aplicables al cálculo de la liquidación de acreencias laborales porla finalización de las relaciones de servicios en Colombia;</li>
\t\t\t\t\t\t\t<li>el Simulador no es un liquidador y, en tal sentido, no suple ni sustituye el análisis específico del caso concreto que corresponda a la situación legal y fáctica del Usuario, sea que éste se trate del beneficiario del pago de la liquidación o se goce de la calidad de empleador;</li>
\t\t\t\t\t\t\t<li>sin perjuicio de la veracidad o exactitud de la información y parámetros ingresados por el Usuario en el Simulador, el Simulador tiene limitaciones y puede no producir resultados válidos para todas las posibles combinaciones de datos de entrada por razones, entre otras, que corresponden a la realidad específica de una relación laboral o de servicios o de la omisión en información o parámetros necesarios para mitigar la probabilidad u ocurrencia de errores;</li>
\t\t\t\t\t\t\t<li>ni el sitio ni Symplifica asumen responsabilidad alguna de revisar, probar o detectar errores; y</li>
\t\t\t\t\t\t\t<li>ninguna simulación del Simulador será proporcionada o tratada como un asesoramiento y en tal sentido, no podrá ser utilizada para la toma de decisiones o como elemento probatorio en acciones judiciales o extrajudiciales.</li>
\t\t\t\t\t\t</ol>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<strong>En uso del Simulador el Usuario acepta que:</strong>
\t\t\t\t\t\t<ol>
\t\t\t\t\t\t\t<li>el uso del Simulador tiene un costo publicado en el sitio web donde se encuentra el Simulador y que este valor remunera el diseño, puesta a punto y mantenimiento del Simulador. En ese sentido, en ningún caso el valor pagado por el Usuario representa un pago por la autenticidad, veracidad o precisión de los resultados del Simulador;</li>
\t\t\t\t\t\t\t<li>todos los montos son calculados con base en la información y parámetros ingresados por el Usuario y con base en los criterios para el cálculo determinados en las leyes y reglamentos aplicables y vigentes para la fecha del cálculo;</li>
\t\t\t\t\t\t\t<li>el uso del Simulador es para uso exclusivo del Usuario y lo empleará para uso personal. En tal medida, el Usuario no empleará los resultados del Simulador para fines contractuales, legales o judiciales;</li>
\t\t\t\t\t\t\t<li>no usará los resultados del Simulador para suplantar o reemplazar a cualquier persona incluyendo, sin limitación, a su empleador, si fuera el caso;</li>
\t\t\t\t\t\t\t<li>no empleará los resultados del Simulador para fines o actividades inmorales o ilícitas incluyendo, sin limitación, extorsión, chantaje o fraude;</li>
\t\t\t\t\t\t\t<li>el uso del Simulador está protegido por las reglas aplicables en materia de Propiedad Industrial y en esa medida, se prohíbe la modificación, reproducción, publicación o transferencia de cualquier contenido a otras personas, o su uso para cualquier fin;</li>
\t\t\t\t\t\t\t<li>le está prohibido desensamblar, descompilar, aplicar ingeniería inversa o intentar por cualquier medio romper la protección del contenido del Simulador;</li>
\t\t\t\t\t\t\t<li>el uso del Simulador es exclusivo en el sitio donde se encuentra publicado, por lo que no se permite la creación de páginas web, sitios de Internet, documentos electrónicos o programas de computador o aplicaciones informáticas de cualquier tipo que contengan hipervínculos o marcas que redirijan al navegante al Simulador; y</li>
\t\t\t\t\t\t\t<li>Symplifica, como gestor y administrador del Simulador, no realizará devoluciones de pagos realizados por el Usuario en uso del Simulador salvo cuando el Usuario demuestre que no le fue posible el uso del Simulador por razones distintas a su conformidad o inconformidad con los resultados arrojados por el Simulador.</li>
\t\t\t\t\t\t</ol>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<strong>En uso del Simulador el Usuario sabe, entiende y acepta que:</strong>
\t\t\t\t\t\t<ol>
\t\t\t\t\t\t\t<li>relaciones laborales que tengan una duración máxima de dieciocho (18) meses;</li>
\t\t\t\t\t\t\t<li>la afiliación del beneficiario del pago al sistema de Seguridad Social en Colombia;</li>
\t\t\t\t\t\t\t<li>el beneficiario del pago no se encuentra, al momento de la simulación, disfrutando de vacaciones o con novedades laborales activas;</li>
\t\t\t\t\t\t\t<li>el beneficiario no goza de fueros legales o de beneficios o restricciones que impidan la desvinculación laboral del beneficiario;</li>
\t\t\t\t\t\t\t<li>la relación laboral presupone un periodo de prueba contractual en el máximo periodo permitido por la ley aplicable;</li>
\t\t\t\t\t\t\t<li>el valor del salario no es inferior, en ningún caso, al valor del salario mínimo mensual legal vigente para la fecha del cálculo;</li>
\t\t\t\t\t\t\t<li>el beneficiario del pago no se remunera contractualmente mediante salario integral (en los términos de la ley aplicable);</li>
\t\t\t\t\t\t\t<li>el empleador ha dado cumplimiento a la ley en sus aspectos materiales como, a título ilustrativo, los pagos durante la relación laboral y los procedimientos para la desvinculación del empleado; y</li>
\t\t\t\t\t\t\t<li>las prestaciones sociales y otros criterios como aportes parafiscales, se presumen por defecto en los criterios mínimos y estandarizados de la ley vigente al momento del cálculo.</li>
\t\t\t\t\t\t</ol>
\t\t\t\t\t</li>
\t\t\t\t\t<li>
\t\t\t\t\t\t<strong>Avisos</strong>
\t\t\t\t\t\tEl Usuario reconoce y acepta, adicionalmente, que:
\t\t\t\t\t\t<ol>
\t\t\t\t\t\t\t<li>La parametrización, los cálculos, resultados y uso del Simulador, quedan limitados exclusivamente a la jurisdicción colombiana;</li>
\t\t\t\t\t\t\t<li>el uso del Simulador está sujeto al pago del valor publicado;</li>
\t\t\t\t\t\t\t<li>Symplifica no será responsable en ningún caso de las consecuencias que puedan seguirse para el Usuario por el hecho de haber remitido éste información o documentación no veraz, inexacta o incompleta;</li>
\t\t\t\t\t\t\t<li>Symplifica se obliga a salvaguardar la confidencialidad de toda la información y documentación recibidas del Usuario que no sean de dominio público, y únicamente podrá desvelarla con autorización del Usuario o por orden de cualquier autoridad administrativa, judicial o legalmente autorizada para ello;</li>
\t\t\t\t\t\t\t<li>el Usuario acepta el correo electrónico no encriptado como medio hábil para el flujo e intercambio de documentación, información y, en general, como canal de comunicación. El Usuario exime a Symplifica, como administrador del Simulador, de cualquier responsabilidad por la interceptación o acceso a los correos electrónicos por personas no autorizadas, así como de cualquier daño o perjuicio que pueda producirse al Usuario como consecuencia de virus informáticos, fallos en la red o supuestos análogos, salvo que fuera por causa imputable a Symplifica;</li>
\t\t\t\t\t\t\t<li>Symplifica podrá estar sujeta a la obligación de comunicar a la Unidad de Información y Análisis Financiero (UIAF) cualquier hecho u operación respecto de los cuales exista indicio o certeza de que está relacionado con el lavado de activos o la financiación del terrorismo, debiendo abstenerse de ejecutar cualquier operación respecto de la que se pongan de manifiesto tales circunstancias. Symplifica no será responsable ante el Usuario de los daños y perjuicios que este pueda sufrir como consecuencia del cumplimiento, por parte de Symplifica, de dichas obligaciones legales;</li>
\t\t\t\t\t\t\t<li>en ningún caso Symplifica será responsable de los daños derivados, o causados, en todo o en parte, como consecuencia de la falsedad, el ocultamiento o cualquier otra conducta del Usuario que fuera dolosa o negligente, o no realizada conforme a los principios de la buena fe, o de incumplimientos que se produzcan por causas que están fuera de su control razonable;</li>
\t\t\t\t\t\t\t<li>la eventual responsabilidad de Symplifica tendrá lugar únicamente frente al Usuario. Symplifica no será responsable de los daños que puedan ocasionarse a terceros como consecuencia del uso que el Usuario pueda hacer del Simulador fuera del destino propio de los mismos;</li>
\t\t\t\t\t\t\t<li>el Usuario se obliga a no instigar a que un tercero demande a profesionales o empleados de Symplifica en relación con el Simulador;</li>
\t\t\t\t\t\t\t<li>Symplifica se reserva el derecho de modificar, cambiar o terminar estas Condiciones de Uso en cualquier momento y bajo su total discreción, sin necesidad de notificarlo previamente. Symplifica le advierte al Usuario que es su deber visitar esta página regularmente para tener conocimiento de los cambios;</li>
\t\t\t\t\t\t\t<li>los datos personales que facilite el Usuario a Symplifica mediante la aceptación de las presentes Condiciones de Uso, así como todos aquellos que facilite en el futuro como consecuencia de su vinculación con Symplifica (los \"Datos Personales\"), se incorporarán a una base de datos sobre la que Symplifica actuará como responsable y encargado y que tendrán el tratamiento previsto en su Política de Privacidad; y</li>
\t\t\t\t\t\t\t<li>la finalidad del tratamiento de los Datos Personales abarca las siguientes actividades: (i) mantenimiento, desarrollo, control y ejecución de la relación que, en el marco del uso del Simulador, mantenga con Symplifica; (ii) comunicaciones con el Usuario con fines de información o de mercadeo; y/o (iii) facturación por el uso del Simulador.</li>
\t\t\t\t\t\t</ol>
\t\t\t\t\t</li>
\t\t\t\t\t
\t\t\t\t</ol>
\t\t\t</div>
\t\t\t<div class=\"acepto\">
\t\t\t\t<input type=\"checkbox\" onchange=\"aceptar()\" id=\"terminos1\">
\t\t\t\t<label>Acepto los términos y condiciones de este servicio</label>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<a id=\"acepto\" class=\"disabled\">Continuar</a>
\t\t\t</div>
\t\t</div>
\t</div>
</section>
{% endblock %}

{% block javascripts %}

<script type=\"text/javascript\">


        function aceptar(){

            \$('#acepto').addClass('disabled');
            \$('#acepto').removeAttr(\"href\");

            if(\$(\"#terminos1\").is(\":checked\") == true){

            \$('#acepto').removeClass('disabled');
            \$('#acepto').attr({
                href: \"{{ url('form') }}\"
            });
        }
\t}

</script>

{% endblock %}
", "dashboard/previa.html.twig", "C:\\xampp\\htdocs\\PHP\\liquidacionesclienteexterno\\app\\Resources\\views\\dashboard\\previa.html.twig");
    }
}
