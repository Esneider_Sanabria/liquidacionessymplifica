<?php

/* base.html.twig */
class __TwigTemplate_54de69bae46782497827fdbc3ea88e481aa43a3afc12d0abec57c6b32f4894be extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'title' => [$this, 'block_title'],
            'nav' => [$this, 'block_nav'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>

    ";
        // line 4
        $this->displayBlock('head', $context, $blocks);
        // line 27
        echo "    <body>
        ";
        // line 28
        $this->displayBlock('nav', $context, $blocks);
        // line 36
        echo "
        ";
        // line 37
        $this->displayBlock('body', $context, $blocks);
        // line 38
        echo "            <footer>
                <div class=\"row\">
                    <div class=\"col-md-3 col-sm-6 col-xs-6 d-inline-block\">
                        <img src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo-bw.png"), "html", null, true);
        echo "\" alt=\"Calculadora de liquidación\" style=\"max-width:169px;\">
                    </div>
                    <div class=\"col-md-6 d-none d-md-inline-block\">
                        <p>&copy;2018. Todos los derechos reservados. <a href=\"/info#terminos\">Términos y Condiciones</a></p>
                        <div class=\"powered\">
                            <p><small>Gestionado por:</small></p>
                            <img src=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo-symplifica.png"), "html", null, true);
        echo "\" alt=\"Symplifica\" style=\"max-width:115px;\">
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6 col-xs-6 d-inline-block powered\">
                        <a href=\"http://www.payulatam.com/co/\" target=\"_blank\"><img src=\"https://ecommerce.payulatam.com/logos/PayU_180x100_Paga.png\" alt=\"Paga aquí con PayU\" border=\"0\" width=\"100%\" style=\"max-width:120px;\" /></a>
                    </div>
                    <div class=\"col-sm-12 d-block d-sm-block d-md-none\">
                         <p>&copy;2018. Todos los derechos reservados. <a href=\"/info#terminos\">Términos y Condiciones</a></p>
                         <div class=\"powered\">
                            <p><small>Gestionado por:</small></p>
                            <img src=\"";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo-symplifica.png"), "html", null, true);
        echo "\" alt=\"Symplifica\" style=\"max-width:115px;\">
                        </div>
                    </div>
                </div>
            </footer>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-70850811-2\"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-70850811-2');
        </script>
        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\" integrity=\"sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49\" crossorigin=\"anonymous\"></script>
        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bootstrap/jquery.min.map"), "html", null, true);
        echo "\"></script>
        <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js\"> </script>
        <script type=\"text/javascript\" src=\"";
        // line 77
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bootstrap/moment.js"), "html", null, true);
        echo "\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js\"></script>
        <script src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/bootstrap-datepicker.es.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js\"></script>
        <script type=\"text/javascript\" src=\"";
        // line 82
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bootstrap/jquery.number.min.js"), "html", null, true);
        echo "\"></script>
        <script>
            \$(function() {
                var header = \$(\".empezar\");
                \$(window).scroll(function() {
                    var scroll = \$(window).scrollTop();

                    if (scroll >= 200) {
                        header.addClass(\"shine\");
                    } else {
                        header.removeClass(\"shine\");
                    }
                });
            });
        </script>
        ";
        // line 97
        $this->displayBlock('javascripts', $context, $blocks);
        // line 99
        echo "
    </body>
</html>
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 4
    public function block_head($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 5
        echo "    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\" />
        <title>";
        // line 8
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <meta property=\"og:title\" content=\"Calcula cuanto debes pagar al liquidar un empleado\"/>
        <meta property=\"og:description\" content=\"Conoce en 5 minutos cuánto pagar por prestaciones e indemnizaciones al liquidar a un empleado.\"/>
        <meta name=\"keywords\" content=\"Liquidación, Liquidacion laboral, Liquidar empleado, Indemnización, Pago liquidacion, Calculadora salarial, Terminar contrato, Finalizar contrato, Despido empleado\">
        <meta property=\"og:image\" content=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/fb-img.png"), "html", null, true);
        echo "\"/>
        <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/favicon/apple-touch-icon.png"), "html", null, true);
        echo "\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/favicon/favicon-32x32.png"), "html", null, true);
        echo "\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/favicon/favicon-16x16.png"), "html", null, true);
        echo "\">
        <link rel=\"manifest\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/favicon/site.webmanifest"), "html", null, true);
        echo "\">
        <link rel=\"mask-icon\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/favicon/safari-pinned-tab.svg"), "html", null, true);
        echo "\" color=\"#07ea58\">
        <meta name=\"msapplication-TileColor\" content=\"#ffffff\">
        <meta name=\"theme-color\" content=\"#ffffff\">
        <link rel=\"stylesheet\" href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/estilos.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">
        <link href=\"https://fonts.googleapis.com/css?family=Barlow:100,200,300,400,500,600,700|Work+Sans:500,800\" rel=\"stylesheet\">
        <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.4.2/css/all.css\" integrity=\"sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css\" />
    </head>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 8
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 28
    public function block_nav($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "nav"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "nav"));

        // line 29
        echo "            <header>
                <nav>
                    <h1 class=\"logo\"><a href=\"/\"><img src=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/logo-calculadora.png"), "html", null, true);
        echo "\" alt=\"Calculadora de liquidación\"></a></h1>
                    <a href=\"javascript:;\" class=\"empezar go2modal\">Empezar</a>
                </nav>
            </header>
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 37
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 97
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 98
        echo "        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 98,  274 => 97,  257 => 37,  242 => 31,  238 => 29,  229 => 28,  212 => 8,  195 => 20,  189 => 17,  185 => 16,  181 => 15,  177 => 14,  173 => 13,  169 => 12,  162 => 8,  157 => 5,  148 => 4,  135 => 99,  133 => 97,  115 => 82,  109 => 79,  104 => 77,  99 => 75,  78 => 57,  65 => 47,  56 => 41,  51 => 38,  49 => 37,  46 => 36,  44 => 28,  41 => 27,  39 => 4,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>

    {% block head %}
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\" />
        <title>{% block title %}{% endblock %}</title>
        <meta property=\"og:title\" content=\"Calcula cuanto debes pagar al liquidar un empleado\"/>
        <meta property=\"og:description\" content=\"Conoce en 5 minutos cuánto pagar por prestaciones e indemnizaciones al liquidar a un empleado.\"/>
        <meta name=\"keywords\" content=\"Liquidación, Liquidacion laboral, Liquidar empleado, Indemnización, Pago liquidacion, Calculadora salarial, Terminar contrato, Finalizar contrato, Despido empleado\">
        <meta property=\"og:image\" content=\"{{ asset('img/fb-img.png') }}\"/>
        <link rel=\"apple-touch-icon\" sizes=\"180x180\" href=\"{{ asset('img/favicon/apple-touch-icon.png') }}\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"32x32\" href=\"{{ asset('img/favicon/favicon-32x32.png') }}\">
        <link rel=\"icon\" type=\"image/png\" sizes=\"16x16\" href=\"{{ asset('img/favicon/favicon-16x16.png') }}\">
        <link rel=\"manifest\" href=\"{{ asset('img/favicon/site.webmanifest') }}\">
        <link rel=\"mask-icon\" href=\"{{ asset('img/favicon/safari-pinned-tab.svg') }}\" color=\"#07ea58\">
        <meta name=\"msapplication-TileColor\" content=\"#ffffff\">
        <meta name=\"theme-color\" content=\"#ffffff\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/estilos.css') }}\">
        <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css\" integrity=\"sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO\" crossorigin=\"anonymous\">
        <link href=\"https://fonts.googleapis.com/css?family=Barlow:100,200,300,400,500,600,700|Work+Sans:500,800\" rel=\"stylesheet\">
        <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.4.2/css/all.css\" integrity=\"sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns\" crossorigin=\"anonymous\">
        <link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css\" />
    </head>
    {% endblock %}
    <body>
        {% block nav %}
            <header>
                <nav>
                    <h1 class=\"logo\"><a href=\"/\"><img src=\"{{ asset('img/logo-calculadora.png') }}\" alt=\"Calculadora de liquidación\"></a></h1>
                    <a href=\"javascript:;\" class=\"empezar go2modal\">Empezar</a>
                </nav>
            </header>
        {% endblock %}

        {% block body %}{% endblock %}
            <footer>
                <div class=\"row\">
                    <div class=\"col-md-3 col-sm-6 col-xs-6 d-inline-block\">
                        <img src=\"{{ asset('img/logo-bw.png') }}\" alt=\"Calculadora de liquidación\" style=\"max-width:169px;\">
                    </div>
                    <div class=\"col-md-6 d-none d-md-inline-block\">
                        <p>&copy;2018. Todos los derechos reservados. <a href=\"/info#terminos\">Términos y Condiciones</a></p>
                        <div class=\"powered\">
                            <p><small>Gestionado por:</small></p>
                            <img src=\"{{ asset('img/logo-symplifica.png') }}\" alt=\"Symplifica\" style=\"max-width:115px;\">
                        </div>
                    </div>
                    <div class=\"col-md-3 col-sm-6 col-xs-6 d-inline-block powered\">
                        <a href=\"http://www.payulatam.com/co/\" target=\"_blank\"><img src=\"https://ecommerce.payulatam.com/logos/PayU_180x100_Paga.png\" alt=\"Paga aquí con PayU\" border=\"0\" width=\"100%\" style=\"max-width:120px;\" /></a>
                    </div>
                    <div class=\"col-sm-12 d-block d-sm-block d-md-none\">
                         <p>&copy;2018. Todos los derechos reservados. <a href=\"/info#terminos\">Términos y Condiciones</a></p>
                         <div class=\"powered\">
                            <p><small>Gestionado por:</small></p>
                            <img src=\"{{ asset('img/logo-symplifica.png') }}\" alt=\"Symplifica\" style=\"max-width:115px;\">
                        </div>
                    </div>
                </div>
            </footer>

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-70850811-2\"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-70850811-2');
        </script>
        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js\" integrity=\"sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49\" crossorigin=\"anonymous\"></script>
        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js\" integrity=\"sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy\" crossorigin=\"anonymous\"></script>
        <script type=\"text/javascript\" src=\"{{ asset('bootstrap/jquery.min.map') }}\"></script>
        <script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js\"> </script>
        <script type=\"text/javascript\" src=\"{{ asset('bootstrap/moment.js') }}\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js\"></script>
        <script src=\"{{ asset('js/bootstrap-datepicker.es.min.js') }}\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js\"></script>
        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js\"></script>
        <script type=\"text/javascript\" src=\"{{ asset('bootstrap/jquery.number.min.js') }}\"></script>
        <script>
            \$(function() {
                var header = \$(\".empezar\");
                \$(window).scroll(function() {
                    var scroll = \$(window).scrollTop();

                    if (scroll >= 200) {
                        header.addClass(\"shine\");
                    } else {
                        header.removeClass(\"shine\");
                    }
                });
            });
        </script>
        {% block javascripts %}
        {% endblock %}

    </body>
</html>
", "base.html.twig", "C:\\xampp\\htdocs\\PHP\\liquidacionesclienteexterno\\app\\Resources\\views\\base.html.twig");
    }
}
