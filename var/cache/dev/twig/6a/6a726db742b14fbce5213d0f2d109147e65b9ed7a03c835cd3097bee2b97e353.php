<?php

/* dashboard/inicio.html.twig */
class __TwigTemplate_1a8e1623a30b6d440f2b3484db4c03b7f3ab87c4a4071c565d87d9a7269c6301 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "dashboard/inicio.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "dashboard/inicio.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "dashboard/inicio.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Calcula cuanto debes pagar al liquidar un empleado";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<section id=\"inicio\">
\t<div class=\"row\">
\t\t<div class=\"col-md-6 col-sm-12 col-xs-12\">
\t\t\t<h2>Calcula las liquidaciones de tus empleados</h2>
\t\t\t<p><strong>¡En sólo 5 minutos!</strong></p>
\t\t\t<p>Por solo <strong>\$";
        // line 9
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, (isset($context["amount"]) || array_key_exists("amount", $context) ? $context["amount"] : (function () { throw new Twig_Error_Runtime('Variable "amount" does not exist.', 9, $this->source); })()), 0, "."), "html", null, true);
        echo "</strong> conoce cuánto debes pagar por la liquidación de tus empleados y recibe toda la documentación necesaria.</p>
\t\t\t<p>Paga fácil con Tarjeta de crédito, PSE, en puntos Baloto y Efecty.</p>
\t\t\t<a href=\"javascript:;\" class=\"go2modal\">Calcular liquidación</a>
\t\t</div>
\t\t<div class=\"col-md-6 col-sm-12 col-xs-12\"></div>
\t</div>
</section>
<section id=\"banner\">
\t<div class=\"row\">
\t\t<div class=\"col-md-2 col-sm-3 col-xs-12\">
\t\t\t<img src=\"";
        // line 19
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/ok-blanco.png"), "html", null, true);
        echo "\" alt=\"Bien\">
\t\t</div>
\t\t<div class=\"col-md-10 col-sm-9 col-xs-12\">
\t\t\t<h3>Genera los cálculos de liquidación de tus empleados acorde a la ley colombiana</h3>
\t\t\t<h4>Y evítate dolores de cabeza...</h4>
\t\t</div>
\t</div>
</section>
<section id=\"empleado\">
\t<div class=\"row\">
\t\t<h3>¿A qué empleados aplica?</h3>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-sm-5 d-none d-sm-block\">
\t\t\t<img src=\"";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/bg2-mobile.png"), "html", null, true);
        echo "\" alt=\"Bien\" width=\"100%\" style=\"max-width:600px;\">
\t\t</div>
\t\t<div class=\"col-sm-7 col-xs-12\">
\t\t\t<p>Aplica para todos los empleados (incluye empleados domésticos), con contratos a <strong>termino fijo e indefinido</strong>, que no superen los 1,5 años de antigüedad; tanto por días como tiempo completo.</p>
\t\t\t<a href=\"javascript:;\" class=\"go2modal\">Calcular liquidación</a>
\t\t</div>
\t\t<div class=\"col-xs-12 d-block d-sm-none image-xs\">
\t\t\t<img src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/bg2-mobile.png"), "html", null, true);
        echo "\" alt=\"Bien\" width=\"100%\" style=\"max-width:600px;\">
\t\t</div>
\t</div>
</section>
<section id=\"que-tiene\">
\t<div class=\"row\">
\t\t<h3>¿Qué incluye el paquete de liquidación?</h3>
\t\t<p>El paquete de liquidación va acorde al motivo de la terminación del contrato.</p>
\t</div>
\t<div class=\"row documentos\">
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/liquidacion.png"), "html", null, true);
        echo "\" alt=\"documento\">
\t\t\t<p><strong>Cálculos de liquidación:</strong> Aquí encuentras los cálculos de cuanto deberías pagarle a tu empleado para finalizar la relación laboral y la indemnización en caso de que aplique.</p>
\t\t</div>
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/certificado.png"), "html", null, true);
        echo "\" alt=\"documento\">
\t\t\t<p><strong>Documento soporte indemnización:</strong> Explica los cálculos para la obtención de la indemnización.</p>
\t\t</div>
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/carta.png"), "html", null, true);
        echo "\" alt=\"documento\">
\t\t\t<p><strong>Certificado laboral:</strong> Rectifica que tu empleado laboró para ti por el periodo y cargo en cuestión.</p>
\t\t</div>
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/carta.png"), "html", null, true);
        echo "\" alt=\"documento\">
\t\t\t<p><strong>Notificación de terminación de contrato despido CON justa causa:</strong> Anuncia formalmente el despido y el efecto de terminación de contrato, así como la causa de este.</p>
\t\t</div>
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/carta.png"), "html", null, true);
        echo "\" alt=\"documento\">
\t\t\t<p><strong>Notificación de terminación de contrato despido SIN justa causa:</strong> Anuncia formalmente el despido y el efecto de terminación de contrato.</p>
\t\t</div>
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/carta.png"), "html", null, true);
        echo "\" alt=\"documento\">
\t\t\t<p><strong>Notificación de terminación de contrato a término fijo:</strong> Anuncia formalmente la terminación de contrato debido a la finalización del tiempo el contrato pactado.</p>
\t\t</div>
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/carta.png"), "html", null, true);
        echo "\" alt=\"documento\">
\t\t\t<p><strong>Aceptación mutuo acuerdo:</strong> Rectifica formalmente la disposición de ambos de dar por terminado el contrato.</p>
\t\t</div>
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/carta.png"), "html", null, true);
        echo "\" alt=\"documento\">
\t\t\t<p><strong>Aceptación de renuncia:</strong> Notifica formalmente estar de acuerdo con la renuncia del empleado.</p>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<a href=\"javascript:;\" class=\"go2modal\">Calcular liquidación</a>
\t</div>
</section>
<div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"prevLead\" style=\"display: none;\">
\t<div class=\"modal-dialog\" role=\"document\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<h5 class=\"modal-title\">Deja tus datos para empezar los cálculos:</h5>
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t\t\t</button>
\t\t\t</div>
\t\t\t<form method=\"post\" id=\"formLead\">
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"\">Nombre Empleador o Razón Social</label>
\t\t\t\t\t\t<input type=\"text\" name=\"name_employer\" id=\"name_employer\" required />
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"\">Celular</label>
\t\t\t\t\t\t<input type=\"number\" name=\"celular\" id=\"phone_employer\" class=\"number\" minlength=\"10\" maxlength=\"10\" required />
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"\">Email</label>
\t\t\t\t\t\t<input type=\"email\" name=\"email\" id=\"email_employer\" required />
\t\t\t\t\t</div>

\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"submit\" id=\"btnGuardar\">Continuar</button>
\t\t\t</div>
\t\t\t</form>
\t\t</div>
\t</div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 121
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 122
        echo "
\t<script>
\t\t\$(document).ready(function (e) {

\t\t\t\$(document).on('click','.go2modal',function (e) {
\t\t\t\t\$('#prevLead').css('display','block');

\t\t\t});

\t\t\t\$(document).on('click','.close',function (e) {
\t\t\t\t\$('#prevLead').css('display','none');
\t\t\t});

\t\t\t\$(document).on('submit','#formLead',function (e) {
\t\t\t\te.preventDefault(0);
\t\t\t\te.stopPropagation();
\t\t\t\tvar data = \$(this).serialize();

\t\t\t\t\$.ajax({
\t\t\t\t\ttype: 'POST',
\t\t\t\t\turl: '";
        // line 142
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("crearLead");
        echo "',
\t\t\t\t\tdata: data,
\t\t\t\t\terror: function() {
\t\t\t\t\t\tswal('Se presento un error');
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(data, textStatus, xhr) {
\t\t\t\t\t\tconsole.log(data);
\t\t\t\t\t\tif(data['flag'] == true){
\t\t\t\t\t\t\tlocalStorage.setItem('leadData',data['id']);
\t\t\t\t\t\t\twindow.location.replace('/info');
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tswal('Se presento un error');
\t\t\t\t\t\t}
\t\t\t\t\t},
\t\t\t\t});

\t\t\t});

\t\t});
\t</script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "dashboard/inicio.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  265 => 142,  243 => 122,  234 => 121,  183 => 79,  176 => 75,  169 => 71,  162 => 67,  155 => 63,  148 => 59,  141 => 55,  134 => 51,  120 => 40,  110 => 33,  93 => 19,  80 => 9,  73 => 4,  64 => 3,  46 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block title %}Calcula cuanto debes pagar al liquidar un empleado{% endblock %}
{% block body %}
<section id=\"inicio\">
\t<div class=\"row\">
\t\t<div class=\"col-md-6 col-sm-12 col-xs-12\">
\t\t\t<h2>Calcula las liquidaciones de tus empleados</h2>
\t\t\t<p><strong>¡En sólo 5 minutos!</strong></p>
\t\t\t<p>Por solo <strong>\${{ amount|number_format(0, '.') }}</strong> conoce cuánto debes pagar por la liquidación de tus empleados y recibe toda la documentación necesaria.</p>
\t\t\t<p>Paga fácil con Tarjeta de crédito, PSE, en puntos Baloto y Efecty.</p>
\t\t\t<a href=\"javascript:;\" class=\"go2modal\">Calcular liquidación</a>
\t\t</div>
\t\t<div class=\"col-md-6 col-sm-12 col-xs-12\"></div>
\t</div>
</section>
<section id=\"banner\">
\t<div class=\"row\">
\t\t<div class=\"col-md-2 col-sm-3 col-xs-12\">
\t\t\t<img src=\"{{ asset('img/ok-blanco.png') }}\" alt=\"Bien\">
\t\t</div>
\t\t<div class=\"col-md-10 col-sm-9 col-xs-12\">
\t\t\t<h3>Genera los cálculos de liquidación de tus empleados acorde a la ley colombiana</h3>
\t\t\t<h4>Y evítate dolores de cabeza...</h4>
\t\t</div>
\t</div>
</section>
<section id=\"empleado\">
\t<div class=\"row\">
\t\t<h3>¿A qué empleados aplica?</h3>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-sm-5 d-none d-sm-block\">
\t\t\t<img src=\"{{ asset('img/bg2-mobile.png') }}\" alt=\"Bien\" width=\"100%\" style=\"max-width:600px;\">
\t\t</div>
\t\t<div class=\"col-sm-7 col-xs-12\">
\t\t\t<p>Aplica para todos los empleados (incluye empleados domésticos), con contratos a <strong>termino fijo e indefinido</strong>, que no superen los 1,5 años de antigüedad; tanto por días como tiempo completo.</p>
\t\t\t<a href=\"javascript:;\" class=\"go2modal\">Calcular liquidación</a>
\t\t</div>
\t\t<div class=\"col-xs-12 d-block d-sm-none image-xs\">
\t\t\t<img src=\"{{ asset('img/bg2-mobile.png') }}\" alt=\"Bien\" width=\"100%\" style=\"max-width:600px;\">
\t\t</div>
\t</div>
</section>
<section id=\"que-tiene\">
\t<div class=\"row\">
\t\t<h3>¿Qué incluye el paquete de liquidación?</h3>
\t\t<p>El paquete de liquidación va acorde al motivo de la terminación del contrato.</p>
\t</div>
\t<div class=\"row documentos\">
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"{{ asset('img/liquidacion.png') }}\" alt=\"documento\">
\t\t\t<p><strong>Cálculos de liquidación:</strong> Aquí encuentras los cálculos de cuanto deberías pagarle a tu empleado para finalizar la relación laboral y la indemnización en caso de que aplique.</p>
\t\t</div>
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"{{ asset('img/certificado.png') }}\" alt=\"documento\">
\t\t\t<p><strong>Documento soporte indemnización:</strong> Explica los cálculos para la obtención de la indemnización.</p>
\t\t</div>
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"{{ asset('img/carta.png') }}\" alt=\"documento\">
\t\t\t<p><strong>Certificado laboral:</strong> Rectifica que tu empleado laboró para ti por el periodo y cargo en cuestión.</p>
\t\t</div>
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"{{ asset('img/carta.png') }}\" alt=\"documento\">
\t\t\t<p><strong>Notificación de terminación de contrato despido CON justa causa:</strong> Anuncia formalmente el despido y el efecto de terminación de contrato, así como la causa de este.</p>
\t\t</div>
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"{{ asset('img/carta.png') }}\" alt=\"documento\">
\t\t\t<p><strong>Notificación de terminación de contrato despido SIN justa causa:</strong> Anuncia formalmente el despido y el efecto de terminación de contrato.</p>
\t\t</div>
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"{{ asset('img/carta.png') }}\" alt=\"documento\">
\t\t\t<p><strong>Notificación de terminación de contrato a término fijo:</strong> Anuncia formalmente la terminación de contrato debido a la finalización del tiempo el contrato pactado.</p>
\t\t</div>
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"{{ asset('img/carta.png') }}\" alt=\"documento\">
\t\t\t<p><strong>Aceptación mutuo acuerdo:</strong> Rectifica formalmente la disposición de ambos de dar por terminado el contrato.</p>
\t\t</div>
\t\t<div class=\"col-sm-4 col-xs-12 documento\">
\t\t\t<img src=\"{{ asset('img/carta.png') }}\" alt=\"documento\">
\t\t\t<p><strong>Aceptación de renuncia:</strong> Notifica formalmente estar de acuerdo con la renuncia del empleado.</p>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<a href=\"javascript:;\" class=\"go2modal\">Calcular liquidación</a>
\t</div>
</section>
<div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"prevLead\" style=\"display: none;\">
\t<div class=\"modal-dialog\" role=\"document\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<h5 class=\"modal-title\">Deja tus datos para empezar los cálculos:</h5>
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
\t\t\t\t\t<span aria-hidden=\"true\">&times;</span>
\t\t\t\t</button>
\t\t\t</div>
\t\t\t<form method=\"post\" id=\"formLead\">
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"\">Nombre Empleador o Razón Social</label>
\t\t\t\t\t\t<input type=\"text\" name=\"name_employer\" id=\"name_employer\" required />
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"\">Celular</label>
\t\t\t\t\t\t<input type=\"number\" name=\"celular\" id=\"phone_employer\" class=\"number\" minlength=\"10\" maxlength=\"10\" required />
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t<label for=\"\">Email</label>
\t\t\t\t\t\t<input type=\"email\" name=\"email\" id=\"email_employer\" required />
\t\t\t\t\t</div>

\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"submit\" id=\"btnGuardar\">Continuar</button>
\t\t\t</div>
\t\t\t</form>
\t\t</div>
\t</div>
</div>
{% endblock %}

{% block javascripts %}

\t<script>
\t\t\$(document).ready(function (e) {

\t\t\t\$(document).on('click','.go2modal',function (e) {
\t\t\t\t\$('#prevLead').css('display','block');

\t\t\t});

\t\t\t\$(document).on('click','.close',function (e) {
\t\t\t\t\$('#prevLead').css('display','none');
\t\t\t});

\t\t\t\$(document).on('submit','#formLead',function (e) {
\t\t\t\te.preventDefault(0);
\t\t\t\te.stopPropagation();
\t\t\t\tvar data = \$(this).serialize();

\t\t\t\t\$.ajax({
\t\t\t\t\ttype: 'POST',
\t\t\t\t\turl: '{{ url(\"crearLead\") }}',
\t\t\t\t\tdata: data,
\t\t\t\t\terror: function() {
\t\t\t\t\t\tswal('Se presento un error');
\t\t\t\t\t},
\t\t\t\t\tdataType: 'json',
\t\t\t\t\tsuccess: function(data, textStatus, xhr) {
\t\t\t\t\t\tconsole.log(data);
\t\t\t\t\t\tif(data['flag'] == true){
\t\t\t\t\t\t\tlocalStorage.setItem('leadData',data['id']);
\t\t\t\t\t\t\twindow.location.replace('/info');
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tswal('Se presento un error');
\t\t\t\t\t\t}
\t\t\t\t\t},
\t\t\t\t});

\t\t\t});

\t\t});
\t</script>

{% endblock %}
", "dashboard/inicio.html.twig", "C:\\xampp\\htdocs\\PHP\\liquidacionesclienteexterno\\app\\Resources\\views\\dashboard\\inicio.html.twig");
    }
}
