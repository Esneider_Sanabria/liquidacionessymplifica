<?php

/* dashboard/form.html.twig */
class __TwigTemplate_2818d77eda7191f786137c289427f3161844a81e5fd7b2606db840ba581b01da extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "dashboard/form.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "dashboard/form.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "dashboard/form.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Calcula cuanto debes pagar al liquidar un empleado";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "\t<style>
\t\t.ocultar {
\t\t\tdisplay: none;
\t\t}
\t</style>
\t<section id=\"form\">
\t\t<form id=\"form_registro\" autocomplete=\"off\">
\t\t\t<div class=\"row\">
\t\t\t\t<h2>GENERA el cálculo de LA LIQUIDACIÓN DE TU EMPLEADO</h2>
\t\t\t\t<p>Diligencia los siguientes datos</p>
\t\t\t</div>

\t\t\t<!-- ================================================ -->
\t\t\t<!-- Datos Empleado/Empleador -->
\t\t\t<!-- ================================================ -->
\t\t\t<div>
\t\t\t\t<input type=\"hidden\" name=\"salario_minimo\" id=\"salario_minimo\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["smlv"]) || array_key_exists("smlv", $context) ? $context["smlv"] : (function () { throw new Twig_Error_Runtime('Variable "smlv" does not exist.', 20, $this->source); })()), "salario", []), "html", null, true);
        echo "\">
\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t<div class=\"linea\"></div>
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12\" style=\"max-width:480px;\">
\t\t\t\t\t\t<legend>TUS DATOS (Datos empleador)</legend>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Nombre Empleador o Razón Social</label>
\t\t\t\t\t\t\t<input type=\"text\" name=\"name_employer\" id=\"name_employer\">
\t\t\t\t\t\t\t<div class=\"error_name_employer\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group selectDivLiquidation\">
\t\t\t\t\t\t\t<label for=\"\">Tipo de documento</label>
\t\t\t\t\t\t\t<div class=\"select-custom\" id=\"DocumEmployer\">
\t\t\t\t\t\t\t\t<select name=\"type_doct_employer\" id=\"type_doct_employer\" class=\"selectLiquidation\" onchange=\"typeDoc()\">
\t\t\t\t\t\t\t\t\t<option value=\"\">Seleccionar</option>
\t\t\t\t\t\t\t\t\t<option value=\"CC\">Cédula de ciudadanía</option>
\t\t\t\t\t\t\t\t\t<option value=\"CE\">Cédula de extranjería</option>
\t\t\t\t\t\t\t\t\t<option value=\"NIT\">Nit</option>
\t\t\t\t\t\t\t\t\t<option value=\"RUT\">Rut</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t<span class=\"select-look\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t\t\t\t\t<p class=\"label-select\">Seleccionar</p>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t</div>";
        // line 45
        echo "\t\t\t\t\t\t\t<div class=\"error_type_doct_employer\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Número de documento</label>
\t\t\t\t\t\t\t<input type=\"number\" name=\"number_employer\" id=\"number_employer\" class=\"number\">
\t\t\t\t\t\t\t<div class=\"error_number_employer\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Celular</label>
\t\t\t\t\t\t\t<input type=\"number\" name=\"phone_employer\" id=\"phone_employer\" class=\"number\" minlength=\"10\" maxlength=\"10\">
\t\t\t\t\t\t\t<div class=\"error_phone_employer\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Email</label>
\t\t\t\t\t\t\t<input type=\"email\" name=\"email_employer\" id=\"email_employer\">
\t\t\t\t\t\t\t<div class=\"error_email_employer\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12\" style=\"max-width:480px;\">
\t\t\t\t\t\t<legend>DATOS EMPLEADO A LIQUIDAR</legend>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Nombre Completo</label>
\t\t\t\t\t\t\t<input type=\"text\" name=\"name_employee\" id=\"name_employee\">
\t\t\t\t\t\t\t<div class=\"error_name_employee\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group selectDivLiquidation\">
\t\t\t\t\t\t\t<label for=\"\">Tipo de documento</label>
\t\t\t\t\t\t\t<div class=\"select-custom\" id=\"DocumEmployee\">
\t\t\t\t\t\t\t\t<select name=\"type_doct_employee\" id=\"type_doct_employee\" class=\"selectLiquidation\" onchange=\"typeDoc()\">
\t\t\t\t\t\t\t\t\t<option value=\"\">Seleccionar tipo documento</option>
\t\t\t\t\t\t\t\t\t<option value=\"CC\">Cédula de ciudadanía</option>
\t\t\t\t\t\t\t\t\t<option value=\"CE\">Cédula de extranjería</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t<span class=\"select-look\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t\t\t\t<p class=\"label-select\">Seleccionar tipo documento</p>
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t</div>";
        // line 83
        echo "\t\t\t\t\t\t\t<div class=\"error_type_doct_employee\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Número de documento</label>
\t\t\t\t\t\t\t<input type=\"number\" name=\"number_employee\" id=\"number_employee\">
\t\t\t\t\t\t\t<div class=\"error_number_employee\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Email (opcional)</label>
\t\t\t\t\t\t\t<input type=\"email\" name=\"email_employee\" id=\"email_employee\">
\t\t\t\t\t\t\t<div class=\"error_email_employee\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Ciudad de trabajo</label>
\t\t\t\t\t\t\t<input type=\"text\" name=\"city\" id=\"city\">
\t\t\t\t\t\t\t<div class=\"error_city\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<!-- ===================================================== -->
\t\t\t<!-- FIN datos Empleado/Empleador -->
\t\t\t<!-- ===================================================== -->


\t\t\t<!-- ===================================================== -->
\t\t\t<!-- Datos Contrato -->
\t\t\t<!-- ===================================================== -->
\t\t\t<div class=\"contractuales\">
\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t<legend>DATOS CONTRACTUALES</legend>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group\">
\t\t\t\t\t\t<label for=\"\">Cargo</label>
\t\t\t\t\t\t<input id=\"cargo\" type=\"text\" name=\"cargo\">
\t\t\t\t\t\t<div class=\"error_cargo\"></div>
\t\t\t\t\t</div>


\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group selectDivLiquidation\">
\t\t\t\t\t\t<label for=\"\">Tipo de contrato</label>
\t\t\t\t\t\t<div class=\"select-custom\" id=\"selectTipContract\">
\t\t\t\t\t\t\t<select id=\"tipContract\" name=\"tipContract\" class=\"selectLiquidation\">
\t\t\t\t\t\t\t\t<option value=\"\">Seleccione el tipo de contrato</option>
\t\t\t\t\t\t\t\t<option value=\"TF\">Término fijo</option>
\t\t\t\t\t\t\t\t<option value=\"TI\">Término indefinido</option>
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t<span class=\"select-look\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t\t\t\t<p class=\"label-select\">Seleccionar el tipo de contrato</p>
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t</div>";
        // line 136
        echo "\t\t\t\t\t\t<div class=\"error_tipContract\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group selectDivLiquidation\">
\t\t\t\t\t\t<label for=\"\">Causa terminación del contrato</label>
\t\t\t\t\t\t<div class=\"select-custom\" id=\"cuasaTerminacionContrato\">
\t\t\t\t\t\t\t<select id=\"terminacionContrato\" name=\"terminacionContrato\" class=\"selectLiquidation\">
\t\t\t\t\t\t\t\t<option value=\"\">Seleccionar causa terminación de contrato</option>
\t\t\t\t\t\t\t\t<option value=\"Con justa causa\">Despido con justa causa</option>
\t\t\t\t\t\t\t\t<option value=\"Sin justa causa\">Despido sin justa causa</option>
\t\t\t\t\t\t\t\t<option value=\"Renuncia\">Renuncia</option>
\t\t\t\t\t\t\t\t<option value=\"Periodo de prueba\">Periodo de prueba</option>
\t\t\t\t\t\t\t\t<option value=\"Mutuo acuerdo\">Mutuo acuerdo</option>
\t\t\t\t\t\t\t\t<option class=\"optionTf\" value=\"Terminación término fijo\" style=\"display: none\">Terminación término fijo</option>
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t<span class=\"select-look\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t\t\t\t<p class=\"label-select\">Seleccionar causa terminación de contrato</p>
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t</div>";
        // line 155
        echo "\t\t\t\t\t\t<div class=\"error_terminacionContrato\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"causa\" class=\"col-sm-12 form-group razones\" style=\"display: none\">
\t\t\t\t\t\t<p>Razón del despido</p>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t";
        // line 160
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["causa"]) || array_key_exists("causa", $context) ? $context["causa"] : (function () { throw new Twig_Error_Runtime('Variable "causa" does not exist.', 160, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["c"]) {
            // line 161
            echo "\t\t\t\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-check form-check-inline\">
\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"despido\" value=\"";
            // line 162
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "name", []), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t\t\t<label for=\"\">";
            // line 163
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["c"], "name", []), "html", null, true);
            echo "</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['c'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 166
        echo "\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"error_despedido\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group _jornadaLaboral\">
\t\t\t\t\t\t<p>¿Cuál es la jornada de trabajo?</p>
\t\t\t\t\t\t<div class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t<input class=\"form-check-input jornadaLaboral\" type=\"radio\" name=\"jornadaLaboral\" id=\"inlineRadio1\" value=\"30\" onchange=\"jornada('30')\">
\t\t\t\t\t\t\t<label class=\"form-check-label\" for=\"inlineRadio1\">Tiempo Completo</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t<input class=\"form-check-input jornadaLaboral\" type=\"radio\" name=\"jornadaLaboral\" id=\"inlineRadio2\" value=\"XD\" onchange=\"jornada('XD')\">
\t\t\t\t\t\t\t<label class=\"form-check-label\" for=\"inlineRadio2\">Por días</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"error_jornadaLaboral\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"tipFijo\" class=\"col-sm-6 col-xs-12 form-group date-input\" style=\"display: none\">
\t\t\t\t\t\t<label for=\"\">Fecha de inicio del contrato</label>
\t\t\t\t\t\t<input type=\"text\" class=\"datepicker\" name=\"dateInicio\" id=\"dateInicio\">
\t\t\t\t\t\t<i class=\"fas fa-calendar-alt calendar1\"></i>
\t\t\t\t\t\t<div class=\"error_dateInicio\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"tipDefinido\" class=\"col-sm-6 col-xs-12 form-group date-input\" style=\"display:none\">
\t\t\t\t\t\t<label for=\"\">Fecha finalización del contrato</label>
\t\t\t\t\t\t<input type=\"text\" class=\"datepicker\" name=\"dateFin\" id=\"dateFin\">
\t\t\t\t\t\t<i class=\"fas fa-calendar-alt\"></i>
\t\t\t\t\t\t<div class=\"error_dateFin\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"tipLiquid\" class=\"col-sm-6 col-xs-12 form-group date-input\" style=\"display: none\">
\t\t\t\t\t\t<label for=\"\">Último día de trabajo</label>
\t\t\t\t\t\t<input type=\"text\" class=\"datepicker\" name=\"dateLiquidacion\" id=\"dateLiquidacion\">
\t\t\t\t\t\t<i class=\"fas fa-calendar-alt\"></i>
\t\t\t\t\t\t<div class=\"error_dateLiquidacion\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"salariosMensual\" class=\"col-sm-6 col-xs-12 form-group\" style=\"display: none\">
\t\t\t\t\t\t<label for=\"\">Salario Mensual</label>
\t\t\t\t\t\t<input type=\"number\" name=\"salario\" id=\"salario\" >
\t\t\t\t\t\t<button type=\"button\" onclick=\"smlv('')\">Salario mínimo</button>
\t\t\t\t\t\t<div class=\"error_salario\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"salarioDiaDiv\" class=\"col-sm-6 col-xs-12 form-group\" style=\"display: none\">
\t\t\t\t\t\t<label for=\"\">Salario Diario</label>
\t\t\t\t\t\t<input type=\"number\" name=\"salarioXd\" id=\"salarioXd\">
\t\t\t\t\t\t<button type=\"button\" onclick=\"smlv('xd')\">Salario mínimo diario</button>
\t\t\t\t\t\t<div class=\"error_salarioXd\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"diasLaborados\" class=\"col-sm-6 col-xs-12 form-group\" style=\"display: none\">
\t\t\t\t\t\t<p>¿Cuáles días a la semana trabajó?</p>
\t\t\t\t\t\t<div id=\"diasLaborados1\" class=\"semanario\">
\t\t\t\t\t\t\t<label class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"diasLaborados\" value=\"1\">
\t\t\t\t\t\t\t\t<span class=\"checkbox-look\">Lunes</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"diasLaborados\" value=\"2\">
\t\t\t\t\t\t\t\t<span class=\"checkbox-look\">Martes</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"diasLaborados\" value=\"3\">
\t\t\t\t\t\t\t\t<span class=\"checkbox-look\">Miércoles</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"diasLaborados\" value=\"4\">
\t\t\t\t\t\t\t\t<span class=\"checkbox-look\">Jueves</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"diasLaborados\" value=\"5\">
\t\t\t\t\t\t\t\t<span class=\"checkbox-look\">Viernes</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"diasLaborados\" value=\"6\">
\t\t\t\t\t\t\t\t<span class=\"checkbox-look\">Sábado</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"diasLaborados\" value=\"0\">
\t\t\t\t\t\t\t\t<span class=\"checkbox-look\">Domingo</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"error_diasLaborados\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group\">
\t\t\t\t\t\t<label for=\"\">¿Cuántos días de vacaciones tomó?</label>
\t\t\t\t\t\t<input type=\"number\" name=\"vacacionesDais\" id=\"vacacionesDais\" value=\"0\">
\t\t\t\t\t\t<div class=\"error_vacacionesDias\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group\" id=\"isAux\">
\t\t\t\t\t\t<p>¿Reside a menos de 1km del trabajo?</p>
\t\t\t\t\t\t<div class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t<input class=\"form-check-input aux\" type=\"radio\" name=\"aux\" value=\"1\">
\t\t\t\t\t\t\t<label class=\"form-check-label\" for=\"internaSi\">Si</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t<input class=\"form-check-input aux\" type=\"radio\" name=\"aux\" value=\"0\">
\t\t\t\t\t\t\t<label class=\"form-check-label\" for=\"internaNo\">No</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"error_aux\"></div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group\" id=\"pagoPrima\" style=\"display: none\">
\t\t\t\t\t\t<p></p>
\t\t        <div class=\"form-check form-check-inline\">
\t\t            <input class=\"form-check-input pgPrima\" type=\"radio\" name=\"pgPrima\" value=\"Si\">
\t\t            <label class=\"form-check-label\" for=\"primaSi\">Si</label>
\t\t        </div>
\t\t        <div class=\"form-check form-check-inline\">
\t\t            <input class=\"form-check-input pgPrima\" type=\"radio\" name=\"pgPrima\" value=\"No\">
\t\t            <label class=\"form-check-label\" for=\"primaNo\">No</label>
\t\t        </div>
\t\t        <div class=\"error_pgPrima\"></div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group\" id=\"cesantiasPregunta\" style=\"display: none\">
\t\t\t\t\t\t<p></p>
\t\t        <div class=\"form-check form-check-inline\">
\t\t            <input class=\"form-check-input pgCesantias\" type=\"radio\" name=\"pgCesantias\" value=\"Si\">
\t\t            <label class=\"form-check-label\" for=\"cesantiasSi\">Si</label>
\t\t        </div>
\t\t        <div class=\"form-check form-check-inline\">
\t\t            <input class=\"form-check-input pgCesantias\" type=\"radio\" name=\"pgCesantias\" value=\"No\">
\t\t            <label class=\"form-check-label\" for=\"cesantiasNo\">No</label>
\t\t        </div>
\t\t        <div class=\"error_pgCesantias\"></div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group\" id=\"cambioSalarioCheck\">
\t\t\t\t\t\t<p>¿A cambiado el salario en el último año?</p>
\t\t\t\t\t\t<div class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t<input class=\"form-check-input aux\" type=\"radio\" name=\"cambioSalario\" value=\"1\">
\t\t\t\t\t\t\t<label class=\"form-check-label\" for=\"cambioSalarioSi\">Si</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t<input class=\"form-check-input aux\" type=\"radio\" name=\"cambioSalario\" value=\"0\">
\t\t\t\t\t\t\t<label class=\"form-check-label\" for=\"cambioSalarioNo\">No</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"error_cambioSalario\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group cambioSalario\" style=\"display: none;\">
\t\t\t\t\t\t<dl>
\t\t\t\t\t\t\t<p>¿Cuando realizó el último cambio de salario?</p>
\t\t\t\t\t\t\t<input type=\"date\" name=\"dateSalari\" id=\"dateSalari\">
\t\t\t\t\t\t\t<p>Anterior salario</p>
\t\t\t\t\t\t\t<div class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"number\" id=\"salaryPrevious\" name=\"salaryPrevious\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"error_salaryPrevius\"></div>
\t\t\t\t\t\t</dl>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t<button type=\"submit\" id=\"btnGuardar\" data-id=\"\">Calcular liquidación</button>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<!-- ===================================================== -->
\t\t\t<!-- FIN Datos Contrato -->
\t\t\t<!-- ===================================================== -->
\t\t</form>

\t</section>

\t<div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"checkout\" style=\"display: none\">
      <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <h5 class=\"modal-title\">Revisa los datos antes de continuar</h5>
            <button type=\"button\" class=\"close hideElement\" data-dismiss=\"modal\" aria-label=\"Close\">
              <span aria-hidden=\"true\">&times;</span>
            </button>
          </div>
          <div class=\"modal-body\">

            <div class=\"row\">
                <div class=\"col-sm-12 col-xs-12\">
                </div>
                <div class=\"col-sm-12 col-xs-12\">
                    <h3>TUS DATOS (Datos empleador)</h3>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Nombre Completo o Nombre empresa</dt>
                        <dd id=\"Dname\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Tipo de documento</dt>
                        <dd id=\"DtypeDocument\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Número de documento</dt>
                        <dd id=\"DnumberEm\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Celular</dt>
                        <dd id=\"DphonEm\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Email</dt>
\t\t\t\t\t\t<dd id=\"DemailEm\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-12 col-xs-12\">
                    <h3>DATOS EMPLEADO A LIQUIDAR</h3>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Nombre Completo</dt>
\t\t\t\t\t\t<dd id=\"DnameEmplo\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Tipo de documento</dt>
\t\t\t\t\t\t<dd id=\"DtypeDocEmplo\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Número de documento</dt>
\t\t\t\t\t\t<dd id=\"DnumberEmplo\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Email (opcional)</dt>
\t\t\t\t\t\t<dd id=\"DemailEmplo\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-12 col-xs-12\">
                    <h3>DATOS CONTRACTUALES</h3>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Cargo</dt>
\t\t\t\t\t\t<dd id=\"Dcargo\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Tipo de contrato</dt>
\t\t\t\t\t\t<dd id=\"DtypeContract\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Causa terminación del contrato</dt>
\t\t\t\t\t\t<dd id=\"DcausaTer\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>¿Cuál es la jornada de trabajo?</dt>
\t\t\t\t\t\t<dd id=\"Djornada\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Fecha de inicio del contrato</dt>
\t\t\t\t\t\t<dd id=\"DdateIni\"></dd>
                    </dl>
                </div>
                <div id=\"tc2\" class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Fecha finalización del contrato</dt>
\t\t\t\t\t\t<dd id=\"DdateEnd\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Último día de trabajo</dt>
\t\t\t\t\t\t<dd id=\"DdateLiqui\"></dd>
                    </dl>
                </div>
                <div id=\"xd\" class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Salario Mensual</dt>
\t\t\t\t\t\t<dd id=\"Dsalary\"></dd>
                    </dl>
                </div>
                <div id=\"tc\" class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Salario Diario</dt>
\t\t\t\t\t\t<dd id=\"DdSalary\"></dd>
                    </dl>
                </div>
                <div id=\"tc1\" class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>¿Cuáles días a la semana trabajó?</dt>
\t\t\t\t\t\t<dd id=\"DdaysXD\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>¿Reside a menos de 1km del trabajo?</dt>
\t\t\t\t\t\t<dd id=\"Daux\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>¿Cuántos días de vacaciones tomó?</dt>
\t\t\t\t\t\t<dd id=\"Dvacaciones\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>¿Pagaste la prima del último periodo (30 junio 2018)?</dt>
\t\t\t\t\t\t<dd id=\"Dprima\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>¿Pagaste las cesantías del periodo 2017 en febrero de 2018?</dt>
\t\t\t\t\t\t<dd id=\"Dces\"></dd>
                    </dl>
                </div>
            </div>
          </div>
          <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-secondary hideElement\" data-dismiss=\"modal\">Corregir</button>
\t\t\t  <div class=\"btn btn-payU\">
\t\t\t\t  <form id=\"formPayU\" method=\"post\" action=\"https://checkout.payulatam.com/ppp-web-gateway-payu/\" accept-charset=\"UTF-8\">
\t\t\t\t\t  <input type=\"image\" border=\"0\" alt=\"\" src=\"http://www.payulatam.com/img-secure-2015/boton_pagar_mediano.png\" onClick=\"this.form.urlOrigen.value = window.location.href;\"/>
\t\t\t\t\t  <input name=\"merchantId\" type=\"hidden\" value=\"";
        // line 493
        echo twig_escape_filter($this->env, (isset($context["merchantId"]) || array_key_exists("merchantId", $context) ? $context["merchantId"] : (function () { throw new Twig_Error_Runtime('Variable "merchantId" does not exist.', 493, $this->source); })()), "html", null, true);
        echo "\"/>
\t\t\t\t\t  <input name=\"accountId\" type=\"hidden\" value=\"556965\"/>
\t\t\t\t\t  <input name=\"referenceCode\" type=\"hidden\" value=\"";
        // line 495
        echo twig_escape_filter($this->env, (isset($context["reference"]) || array_key_exists("reference", $context) ? $context["reference"] : (function () { throw new Twig_Error_Runtime('Variable "reference" does not exist.', 495, $this->source); })()), "html", null, true);
        echo "\"/>
\t\t\t\t\t  <input name=\"description\" type=\"hidden\" value=\"Pago cálculos de liquidación\"/>
\t\t\t\t\t  <input name=\"amount\" type=\"hidden\" value=\"";
        // line 497
        echo twig_escape_filter($this->env, (isset($context["amount"]) || array_key_exists("amount", $context) ? $context["amount"] : (function () { throw new Twig_Error_Runtime('Variable "amount" does not exist.', 497, $this->source); })()), "html", null, true);
        echo "\"/>
\t\t\t\t\t      <input name=\"tax\" type=\"hidden\" value=\"";
        // line 498
        echo twig_escape_filter($this->env, (isset($context["tax"]) || array_key_exists("tax", $context) ? $context["tax"] : (function () { throw new Twig_Error_Runtime('Variable "tax" does not exist.', 498, $this->source); })()), "html", null, true);
        echo "\"/>
\t\t\t\t\t      <input name=\"taxReturnBase\" type=\"hidden\" value=\"";
        // line 499
        echo twig_escape_filter($this->env, (isset($context["valWithoutTax"]) || array_key_exists("valWithoutTax", $context) ? $context["valWithoutTax"] : (function () { throw new Twig_Error_Runtime('Variable "valWithoutTax" does not exist.', 499, $this->source); })()), "html", null, true);
        echo "\"/>
\t\t\t\t\t  <input name=\"shipmentValue\" value=\"0\" type=\"hidden\"/>
\t\t\t\t\t  <input name=\"currency\" type=\"hidden\" value=\"";
        // line 501
        echo twig_escape_filter($this->env, (isset($context["currency"]) || array_key_exists("currency", $context) ? $context["currency"] : (function () { throw new Twig_Error_Runtime('Variable "currency" does not exist.', 501, $this->source); })()), "html", null, true);
        echo "\"/>
\t\t\t\t\t  <input name=\"lng\" type=\"hidden\" value=\"es\"/>
\t\t\t\t\t  <input name=\"test\" type=\"hidden\" value=\"0\"/>
\t\t\t\t\t  <input name=\"buyerEmail\" id=\"buyerEmail\" type=\"hidden\">
\t\t\t\t\t  <input name=\"extra1\" id=\"extra1\" type=\"hidden\"/>
\t\t\t\t\t  <input name=\"approvedResponseUrl\" type=\"hidden\" value=\"https://www.afiliese.co/aprobado\"/>
\t\t\t\t\t  <input name=\"declinedResponseUrl\" type=\"hidden\" value=\"https://www.afiliese.co/no-aprobado\"/>
\t\t\t\t\t  <input name=\"pendingResponseUrl\" type=\"hidden\" value=\"https://www.afiliese.co/espera\"/>
                      <input name=\"confirmationUrl\"    type=\"hidden\"  value=\"https://www.afiliese.co/payu/confirmacion\" >
\t\t\t\t\t      <input name=\"displayShippingInformation\" type=\"hidden\" value=\"NO\"/>
\t\t\t\t\t    <input name=\"sourceUrl\" id=\"urlOrigen\" value=\"\" type=\"hidden\"/>
\t\t\t\t\t    <input name=\"signature\" value=\"";
        // line 512
        echo twig_escape_filter($this->env, (isset($context["signature"]) || array_key_exists("signature", $context) ? $context["signature"] : (function () { throw new Twig_Error_Runtime('Variable "signature" does not exist.', 512, $this->source); })()), "html", null, true);
        echo "\" type=\"hidden\"/>
\t\t\t\t  </form>
\t\t\t  </div>
          </div>
        </div>
      </div>
    </div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 521
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 522
        echo "\t<script src=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/bootstrap-datepicker.es.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 523
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/scripts/formulario.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\" src=\"";
        // line 524
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bootstrap/accounting.min.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "dashboard/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  660 => 524,  656 => 523,  651 => 522,  642 => 521,  624 => 512,  610 => 501,  605 => 499,  601 => 498,  597 => 497,  592 => 495,  587 => 493,  258 => 166,  249 => 163,  245 => 162,  242 => 161,  238 => 160,  231 => 155,  211 => 136,  157 => 83,  118 => 45,  91 => 20,  73 => 4,  64 => 3,  46 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block title %}Calcula cuanto debes pagar al liquidar un empleado{% endblock %}
{% block body %}
\t<style>
\t\t.ocultar {
\t\t\tdisplay: none;
\t\t}
\t</style>
\t<section id=\"form\">
\t\t<form id=\"form_registro\" autocomplete=\"off\">
\t\t\t<div class=\"row\">
\t\t\t\t<h2>GENERA el cálculo de LA LIQUIDACIÓN DE TU EMPLEADO</h2>
\t\t\t\t<p>Diligencia los siguientes datos</p>
\t\t\t</div>

\t\t\t<!-- ================================================ -->
\t\t\t<!-- Datos Empleado/Empleador -->
\t\t\t<!-- ================================================ -->
\t\t\t<div>
\t\t\t\t<input type=\"hidden\" name=\"salario_minimo\" id=\"salario_minimo\" value=\"{{ smlv.salario }}\">
\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t<div class=\"linea\"></div>
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12\" style=\"max-width:480px;\">
\t\t\t\t\t\t<legend>TUS DATOS (Datos empleador)</legend>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Nombre Empleador o Razón Social</label>
\t\t\t\t\t\t\t<input type=\"text\" name=\"name_employer\" id=\"name_employer\">
\t\t\t\t\t\t\t<div class=\"error_name_employer\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group selectDivLiquidation\">
\t\t\t\t\t\t\t<label for=\"\">Tipo de documento</label>
\t\t\t\t\t\t\t<div class=\"select-custom\" id=\"DocumEmployer\">
\t\t\t\t\t\t\t\t<select name=\"type_doct_employer\" id=\"type_doct_employer\" class=\"selectLiquidation\" onchange=\"typeDoc()\">
\t\t\t\t\t\t\t\t\t<option value=\"\">Seleccionar</option>
\t\t\t\t\t\t\t\t\t<option value=\"CC\">Cédula de ciudadanía</option>
\t\t\t\t\t\t\t\t\t<option value=\"CE\">Cédula de extranjería</option>
\t\t\t\t\t\t\t\t\t<option value=\"NIT\">Nit</option>
\t\t\t\t\t\t\t\t\t<option value=\"RUT\">Rut</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t<span class=\"select-look\">
\t\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t\t\t\t\t<p class=\"label-select\">Seleccionar</p>
\t\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t</div>{# select-custom #}
\t\t\t\t\t\t\t<div class=\"error_type_doct_employer\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Número de documento</label>
\t\t\t\t\t\t\t<input type=\"number\" name=\"number_employer\" id=\"number_employer\" class=\"number\">
\t\t\t\t\t\t\t<div class=\"error_number_employer\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Celular</label>
\t\t\t\t\t\t\t<input type=\"number\" name=\"phone_employer\" id=\"phone_employer\" class=\"number\" minlength=\"10\" maxlength=\"10\">
\t\t\t\t\t\t\t<div class=\"error_phone_employer\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Email</label>
\t\t\t\t\t\t\t<input type=\"email\" name=\"email_employer\" id=\"email_employer\">
\t\t\t\t\t\t\t<div class=\"error_email_employer\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12\" style=\"max-width:480px;\">
\t\t\t\t\t\t<legend>DATOS EMPLEADO A LIQUIDAR</legend>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Nombre Completo</label>
\t\t\t\t\t\t\t<input type=\"text\" name=\"name_employee\" id=\"name_employee\">
\t\t\t\t\t\t\t<div class=\"error_name_employee\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group selectDivLiquidation\">
\t\t\t\t\t\t\t<label for=\"\">Tipo de documento</label>
\t\t\t\t\t\t\t<div class=\"select-custom\" id=\"DocumEmployee\">
\t\t\t\t\t\t\t\t<select name=\"type_doct_employee\" id=\"type_doct_employee\" class=\"selectLiquidation\" onchange=\"typeDoc()\">
\t\t\t\t\t\t\t\t\t<option value=\"\">Seleccionar tipo documento</option>
\t\t\t\t\t\t\t\t\t<option value=\"CC\">Cédula de ciudadanía</option>
\t\t\t\t\t\t\t\t\t<option value=\"CE\">Cédula de extranjería</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t<span class=\"select-look\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t\t\t\t<p class=\"label-select\">Seleccionar tipo documento</p>
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t\t</div>{# select-custom #}
\t\t\t\t\t\t\t<div class=\"error_type_doct_employee\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Número de documento</label>
\t\t\t\t\t\t\t<input type=\"number\" name=\"number_employee\" id=\"number_employee\">
\t\t\t\t\t\t\t<div class=\"error_number_employee\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Email (opcional)</label>
\t\t\t\t\t\t\t<input type=\"email\" name=\"email_employee\" id=\"email_employee\">
\t\t\t\t\t\t\t<div class=\"error_email_employee\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<label for=\"\">Ciudad de trabajo</label>
\t\t\t\t\t\t\t<input type=\"text\" name=\"city\" id=\"city\">
\t\t\t\t\t\t\t<div class=\"error_city\"></div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<!-- ===================================================== -->
\t\t\t<!-- FIN datos Empleado/Empleador -->
\t\t\t<!-- ===================================================== -->


\t\t\t<!-- ===================================================== -->
\t\t\t<!-- Datos Contrato -->
\t\t\t<!-- ===================================================== -->
\t\t\t<div class=\"contractuales\">
\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t<legend>DATOS CONTRACTUALES</legend>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group\">
\t\t\t\t\t\t<label for=\"\">Cargo</label>
\t\t\t\t\t\t<input id=\"cargo\" type=\"text\" name=\"cargo\">
\t\t\t\t\t\t<div class=\"error_cargo\"></div>
\t\t\t\t\t</div>


\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group selectDivLiquidation\">
\t\t\t\t\t\t<label for=\"\">Tipo de contrato</label>
\t\t\t\t\t\t<div class=\"select-custom\" id=\"selectTipContract\">
\t\t\t\t\t\t\t<select id=\"tipContract\" name=\"tipContract\" class=\"selectLiquidation\">
\t\t\t\t\t\t\t\t<option value=\"\">Seleccione el tipo de contrato</option>
\t\t\t\t\t\t\t\t<option value=\"TF\">Término fijo</option>
\t\t\t\t\t\t\t\t<option value=\"TI\">Término indefinido</option>
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t<span class=\"select-look\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t\t\t\t<p class=\"label-select\">Seleccionar el tipo de contrato</p>
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t</div>{# select-custom #}
\t\t\t\t\t\t<div class=\"error_tipContract\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group selectDivLiquidation\">
\t\t\t\t\t\t<label for=\"\">Causa terminación del contrato</label>
\t\t\t\t\t\t<div class=\"select-custom\" id=\"cuasaTerminacionContrato\">
\t\t\t\t\t\t\t<select id=\"terminacionContrato\" name=\"terminacionContrato\" class=\"selectLiquidation\">
\t\t\t\t\t\t\t\t<option value=\"\">Seleccionar causa terminación de contrato</option>
\t\t\t\t\t\t\t\t<option value=\"Con justa causa\">Despido con justa causa</option>
\t\t\t\t\t\t\t\t<option value=\"Sin justa causa\">Despido sin justa causa</option>
\t\t\t\t\t\t\t\t<option value=\"Renuncia\">Renuncia</option>
\t\t\t\t\t\t\t\t<option value=\"Periodo de prueba\">Periodo de prueba</option>
\t\t\t\t\t\t\t\t<option value=\"Mutuo acuerdo\">Mutuo acuerdo</option>
\t\t\t\t\t\t\t\t<option class=\"optionTf\" value=\"Terminación término fijo\" style=\"display: none\">Terminación término fijo</option>
\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t<span class=\"select-look\">
\t\t\t\t\t\t\t\t<i class=\"fa fa-angle-down\"></i>
\t\t\t\t\t\t\t\t<p class=\"label-select\">Seleccionar causa terminación de contrato</p>
\t\t\t\t\t\t\t</span>
\t\t\t\t\t\t</div>{# select-custom #}
\t\t\t\t\t\t<div class=\"error_terminacionContrato\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"causa\" class=\"col-sm-12 form-group razones\" style=\"display: none\">
\t\t\t\t\t\t<p>Razón del despido</p>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t{% for c in causa %}
\t\t\t\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-check form-check-inline\">
\t\t\t\t\t\t\t\t\t<input type=\"radio\" name=\"despido\" value=\"{{ c.name }}\">
\t\t\t\t\t\t\t\t\t<label for=\"\">{{ c.name }}</label>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"error_despedido\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group _jornadaLaboral\">
\t\t\t\t\t\t<p>¿Cuál es la jornada de trabajo?</p>
\t\t\t\t\t\t<div class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t<input class=\"form-check-input jornadaLaboral\" type=\"radio\" name=\"jornadaLaboral\" id=\"inlineRadio1\" value=\"30\" onchange=\"jornada('30')\">
\t\t\t\t\t\t\t<label class=\"form-check-label\" for=\"inlineRadio1\">Tiempo Completo</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t<input class=\"form-check-input jornadaLaboral\" type=\"radio\" name=\"jornadaLaboral\" id=\"inlineRadio2\" value=\"XD\" onchange=\"jornada('XD')\">
\t\t\t\t\t\t\t<label class=\"form-check-label\" for=\"inlineRadio2\">Por días</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"error_jornadaLaboral\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"tipFijo\" class=\"col-sm-6 col-xs-12 form-group date-input\" style=\"display: none\">
\t\t\t\t\t\t<label for=\"\">Fecha de inicio del contrato</label>
\t\t\t\t\t\t<input type=\"text\" class=\"datepicker\" name=\"dateInicio\" id=\"dateInicio\">
\t\t\t\t\t\t<i class=\"fas fa-calendar-alt calendar1\"></i>
\t\t\t\t\t\t<div class=\"error_dateInicio\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"tipDefinido\" class=\"col-sm-6 col-xs-12 form-group date-input\" style=\"display:none\">
\t\t\t\t\t\t<label for=\"\">Fecha finalización del contrato</label>
\t\t\t\t\t\t<input type=\"text\" class=\"datepicker\" name=\"dateFin\" id=\"dateFin\">
\t\t\t\t\t\t<i class=\"fas fa-calendar-alt\"></i>
\t\t\t\t\t\t<div class=\"error_dateFin\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"tipLiquid\" class=\"col-sm-6 col-xs-12 form-group date-input\" style=\"display: none\">
\t\t\t\t\t\t<label for=\"\">Último día de trabajo</label>
\t\t\t\t\t\t<input type=\"text\" class=\"datepicker\" name=\"dateLiquidacion\" id=\"dateLiquidacion\">
\t\t\t\t\t\t<i class=\"fas fa-calendar-alt\"></i>
\t\t\t\t\t\t<div class=\"error_dateLiquidacion\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"salariosMensual\" class=\"col-sm-6 col-xs-12 form-group\" style=\"display: none\">
\t\t\t\t\t\t<label for=\"\">Salario Mensual</label>
\t\t\t\t\t\t<input type=\"number\" name=\"salario\" id=\"salario\" >
\t\t\t\t\t\t<button type=\"button\" onclick=\"smlv('')\">Salario mínimo</button>
\t\t\t\t\t\t<div class=\"error_salario\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"salarioDiaDiv\" class=\"col-sm-6 col-xs-12 form-group\" style=\"display: none\">
\t\t\t\t\t\t<label for=\"\">Salario Diario</label>
\t\t\t\t\t\t<input type=\"number\" name=\"salarioXd\" id=\"salarioXd\">
\t\t\t\t\t\t<button type=\"button\" onclick=\"smlv('xd')\">Salario mínimo diario</button>
\t\t\t\t\t\t<div class=\"error_salarioXd\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div id=\"diasLaborados\" class=\"col-sm-6 col-xs-12 form-group\" style=\"display: none\">
\t\t\t\t\t\t<p>¿Cuáles días a la semana trabajó?</p>
\t\t\t\t\t\t<div id=\"diasLaborados1\" class=\"semanario\">
\t\t\t\t\t\t\t<label class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"diasLaborados\" value=\"1\">
\t\t\t\t\t\t\t\t<span class=\"checkbox-look\">Lunes</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"diasLaborados\" value=\"2\">
\t\t\t\t\t\t\t\t<span class=\"checkbox-look\">Martes</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"diasLaborados\" value=\"3\">
\t\t\t\t\t\t\t\t<span class=\"checkbox-look\">Miércoles</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"diasLaborados\" value=\"4\">
\t\t\t\t\t\t\t\t<span class=\"checkbox-look\">Jueves</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"diasLaborados\" value=\"5\">
\t\t\t\t\t\t\t\t<span class=\"checkbox-look\">Viernes</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"diasLaborados\" value=\"6\">
\t\t\t\t\t\t\t\t<span class=\"checkbox-look\">Sábado</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t<label class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"diasLaborados\" value=\"0\">
\t\t\t\t\t\t\t\t<span class=\"checkbox-look\">Domingo</span>
\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"error_diasLaborados\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group\">
\t\t\t\t\t\t<label for=\"\">¿Cuántos días de vacaciones tomó?</label>
\t\t\t\t\t\t<input type=\"number\" name=\"vacacionesDais\" id=\"vacacionesDais\" value=\"0\">
\t\t\t\t\t\t<div class=\"error_vacacionesDias\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group\" id=\"isAux\">
\t\t\t\t\t\t<p>¿Reside a menos de 1km del trabajo?</p>
\t\t\t\t\t\t<div class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t<input class=\"form-check-input aux\" type=\"radio\" name=\"aux\" value=\"1\">
\t\t\t\t\t\t\t<label class=\"form-check-label\" for=\"internaSi\">Si</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t<input class=\"form-check-input aux\" type=\"radio\" name=\"aux\" value=\"0\">
\t\t\t\t\t\t\t<label class=\"form-check-label\" for=\"internaNo\">No</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"error_aux\"></div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group\" id=\"pagoPrima\" style=\"display: none\">
\t\t\t\t\t\t<p></p>
\t\t        <div class=\"form-check form-check-inline\">
\t\t            <input class=\"form-check-input pgPrima\" type=\"radio\" name=\"pgPrima\" value=\"Si\">
\t\t            <label class=\"form-check-label\" for=\"primaSi\">Si</label>
\t\t        </div>
\t\t        <div class=\"form-check form-check-inline\">
\t\t            <input class=\"form-check-input pgPrima\" type=\"radio\" name=\"pgPrima\" value=\"No\">
\t\t            <label class=\"form-check-label\" for=\"primaNo\">No</label>
\t\t        </div>
\t\t        <div class=\"error_pgPrima\"></div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group\" id=\"cesantiasPregunta\" style=\"display: none\">
\t\t\t\t\t\t<p></p>
\t\t        <div class=\"form-check form-check-inline\">
\t\t            <input class=\"form-check-input pgCesantias\" type=\"radio\" name=\"pgCesantias\" value=\"Si\">
\t\t            <label class=\"form-check-label\" for=\"cesantiasSi\">Si</label>
\t\t        </div>
\t\t        <div class=\"form-check form-check-inline\">
\t\t            <input class=\"form-check-input pgCesantias\" type=\"radio\" name=\"pgCesantias\" value=\"No\">
\t\t            <label class=\"form-check-label\" for=\"cesantiasNo\">No</label>
\t\t        </div>
\t\t        <div class=\"error_pgCesantias\"></div>
\t\t\t\t\t</div>

\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group\" id=\"cambioSalarioCheck\">
\t\t\t\t\t\t<p>¿A cambiado el salario en el último año?</p>
\t\t\t\t\t\t<div class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t<input class=\"form-check-input aux\" type=\"radio\" name=\"cambioSalario\" value=\"1\">
\t\t\t\t\t\t\t<label class=\"form-check-label\" for=\"cambioSalarioSi\">Si</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t<input class=\"form-check-input aux\" type=\"radio\" name=\"cambioSalario\" value=\"0\">
\t\t\t\t\t\t\t<label class=\"form-check-label\" for=\"cambioSalarioNo\">No</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"error_cambioSalario\"></div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-6 col-xs-12 form-group cambioSalario\" style=\"display: none;\">
\t\t\t\t\t\t<dl>
\t\t\t\t\t\t\t<p>¿Cuando realizó el último cambio de salario?</p>
\t\t\t\t\t\t\t<input type=\"date\" name=\"dateSalari\" id=\"dateSalari\">
\t\t\t\t\t\t\t<p>Anterior salario</p>
\t\t\t\t\t\t\t<div class=\"form-check form-check-inline\">
\t\t\t\t\t\t\t\t<input type=\"number\" id=\"salaryPrevious\" name=\"salaryPrevious\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"error_salaryPrevius\"></div>
\t\t\t\t\t\t</dl>
\t\t\t\t\t</div>

\t\t\t\t</div>
\t\t\t\t<div class=\"form-row\">
\t\t\t\t\t<button type=\"submit\" id=\"btnGuardar\" data-id=\"\">Calcular liquidación</button>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<!-- ===================================================== -->
\t\t\t<!-- FIN Datos Contrato -->
\t\t\t<!-- ===================================================== -->
\t\t</form>

\t</section>

\t<div class=\"modal\" tabindex=\"-1\" role=\"dialog\" id=\"checkout\" style=\"display: none\">
      <div class=\"modal-dialog\" role=\"document\">
        <div class=\"modal-content\">
          <div class=\"modal-header\">
            <h5 class=\"modal-title\">Revisa los datos antes de continuar</h5>
            <button type=\"button\" class=\"close hideElement\" data-dismiss=\"modal\" aria-label=\"Close\">
              <span aria-hidden=\"true\">&times;</span>
            </button>
          </div>
          <div class=\"modal-body\">

            <div class=\"row\">
                <div class=\"col-sm-12 col-xs-12\">
                </div>
                <div class=\"col-sm-12 col-xs-12\">
                    <h3>TUS DATOS (Datos empleador)</h3>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Nombre Completo o Nombre empresa</dt>
                        <dd id=\"Dname\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Tipo de documento</dt>
                        <dd id=\"DtypeDocument\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Número de documento</dt>
                        <dd id=\"DnumberEm\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Celular</dt>
                        <dd id=\"DphonEm\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Email</dt>
\t\t\t\t\t\t<dd id=\"DemailEm\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-12 col-xs-12\">
                    <h3>DATOS EMPLEADO A LIQUIDAR</h3>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Nombre Completo</dt>
\t\t\t\t\t\t<dd id=\"DnameEmplo\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Tipo de documento</dt>
\t\t\t\t\t\t<dd id=\"DtypeDocEmplo\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Número de documento</dt>
\t\t\t\t\t\t<dd id=\"DnumberEmplo\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Email (opcional)</dt>
\t\t\t\t\t\t<dd id=\"DemailEmplo\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-12 col-xs-12\">
                    <h3>DATOS CONTRACTUALES</h3>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Cargo</dt>
\t\t\t\t\t\t<dd id=\"Dcargo\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Tipo de contrato</dt>
\t\t\t\t\t\t<dd id=\"DtypeContract\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Causa terminación del contrato</dt>
\t\t\t\t\t\t<dd id=\"DcausaTer\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>¿Cuál es la jornada de trabajo?</dt>
\t\t\t\t\t\t<dd id=\"Djornada\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Fecha de inicio del contrato</dt>
\t\t\t\t\t\t<dd id=\"DdateIni\"></dd>
                    </dl>
                </div>
                <div id=\"tc2\" class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Fecha finalización del contrato</dt>
\t\t\t\t\t\t<dd id=\"DdateEnd\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Último día de trabajo</dt>
\t\t\t\t\t\t<dd id=\"DdateLiqui\"></dd>
                    </dl>
                </div>
                <div id=\"xd\" class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Salario Mensual</dt>
\t\t\t\t\t\t<dd id=\"Dsalary\"></dd>
                    </dl>
                </div>
                <div id=\"tc\" class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>Salario Diario</dt>
\t\t\t\t\t\t<dd id=\"DdSalary\"></dd>
                    </dl>
                </div>
                <div id=\"tc1\" class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>¿Cuáles días a la semana trabajó?</dt>
\t\t\t\t\t\t<dd id=\"DdaysXD\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>¿Reside a menos de 1km del trabajo?</dt>
\t\t\t\t\t\t<dd id=\"Daux\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>¿Cuántos días de vacaciones tomó?</dt>
\t\t\t\t\t\t<dd id=\"Dvacaciones\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>¿Pagaste la prima del último periodo (30 junio 2018)?</dt>
\t\t\t\t\t\t<dd id=\"Dprima\"></dd>
                    </dl>
                </div>
                <div class=\"col-sm-6 col-xs-12\">
                    <dl>
                        <dt>¿Pagaste las cesantías del periodo 2017 en febrero de 2018?</dt>
\t\t\t\t\t\t<dd id=\"Dces\"></dd>
                    </dl>
                </div>
            </div>
          </div>
          <div class=\"modal-footer\">
            <button type=\"button\" class=\"btn btn-secondary hideElement\" data-dismiss=\"modal\">Corregir</button>
\t\t\t  <div class=\"btn btn-payU\">
\t\t\t\t  <form id=\"formPayU\" method=\"post\" action=\"https://checkout.payulatam.com/ppp-web-gateway-payu/\" accept-charset=\"UTF-8\">
\t\t\t\t\t  <input type=\"image\" border=\"0\" alt=\"\" src=\"http://www.payulatam.com/img-secure-2015/boton_pagar_mediano.png\" onClick=\"this.form.urlOrigen.value = window.location.href;\"/>
\t\t\t\t\t  <input name=\"merchantId\" type=\"hidden\" value=\"{{ merchantId }}\"/>
\t\t\t\t\t  <input name=\"accountId\" type=\"hidden\" value=\"556965\"/>
\t\t\t\t\t  <input name=\"referenceCode\" type=\"hidden\" value=\"{{ reference }}\"/>
\t\t\t\t\t  <input name=\"description\" type=\"hidden\" value=\"Pago cálculos de liquidación\"/>
\t\t\t\t\t  <input name=\"amount\" type=\"hidden\" value=\"{{ amount }}\"/>
\t\t\t\t\t      <input name=\"tax\" type=\"hidden\" value=\"{{ tax }}\"/>
\t\t\t\t\t      <input name=\"taxReturnBase\" type=\"hidden\" value=\"{{ valWithoutTax }}\"/>
\t\t\t\t\t  <input name=\"shipmentValue\" value=\"0\" type=\"hidden\"/>
\t\t\t\t\t  <input name=\"currency\" type=\"hidden\" value=\"{{ currency }}\"/>
\t\t\t\t\t  <input name=\"lng\" type=\"hidden\" value=\"es\"/>
\t\t\t\t\t  <input name=\"test\" type=\"hidden\" value=\"0\"/>
\t\t\t\t\t  <input name=\"buyerEmail\" id=\"buyerEmail\" type=\"hidden\">
\t\t\t\t\t  <input name=\"extra1\" id=\"extra1\" type=\"hidden\"/>
\t\t\t\t\t  <input name=\"approvedResponseUrl\" type=\"hidden\" value=\"https://www.afiliese.co/aprobado\"/>
\t\t\t\t\t  <input name=\"declinedResponseUrl\" type=\"hidden\" value=\"https://www.afiliese.co/no-aprobado\"/>
\t\t\t\t\t  <input name=\"pendingResponseUrl\" type=\"hidden\" value=\"https://www.afiliese.co/espera\"/>
                      <input name=\"confirmationUrl\"    type=\"hidden\"  value=\"https://www.afiliese.co/payu/confirmacion\" >
\t\t\t\t\t      <input name=\"displayShippingInformation\" type=\"hidden\" value=\"NO\"/>
\t\t\t\t\t    <input name=\"sourceUrl\" id=\"urlOrigen\" value=\"\" type=\"hidden\"/>
\t\t\t\t\t    <input name=\"signature\" value=\"{{ signature }}\" type=\"hidden\"/>
\t\t\t\t  </form>
\t\t\t  </div>
          </div>
        </div>
      </div>
    </div>
{% endblock %}

{% block javascripts %}
\t<script src=\"{{ asset('js/bootstrap-datepicker.es.min.js') }}\"></script>
\t<script type=\"text/javascript\" src=\"{{ asset('js/scripts/formulario.js') }}\"></script>
\t<script type=\"text/javascript\" src=\"{{ asset('bootstrap/accounting.min.js') }}\"></script>
{% endblock %}
", "dashboard/form.html.twig", "C:\\xampp\\htdocs\\PHP\\liquidacionesclienteexterno\\app\\Resources\\views\\dashboard\\form.html.twig");
    }
}
