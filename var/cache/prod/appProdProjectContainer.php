<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerXqiaj7o\appProdProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerXqiaj7o/appProdProjectContainer.php') {
    touch(__DIR__.'/ContainerXqiaj7o.legacy');

    return;
}

if (!\class_exists(appProdProjectContainer::class, false)) {
    \class_alias(\ContainerXqiaj7o\appProdProjectContainer::class, appProdProjectContainer::class, false);
}

return new \ContainerXqiaj7o\appProdProjectContainer(array(
    'container.build_hash' => 'Xqiaj7o',
    'container.build_id' => 'eb9ec84b',
    'container.build_time' => 1551053718,
), __DIR__.\DIRECTORY_SEPARATOR.'ContainerXqiaj7o');
